<?php

namespace App\Exports;

use App\Models\Bill;
use App\Models\Income;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Models\Project;
use App\Models\ProjectTask;
use App\Models\Vender;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;


class IncomesExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

     protected $project_id; 

     public function __construct($project_id) {
         $this->project_id = $project_id;
     }
    public function collection()
    {
        $data_income = collect();
        $projectID = $this->project_id;

        $incomes = Income::where('project_id', '=', $projectID)->get();
        
    foreach($incomes as $income) {
        $status = Invoice::where('income_id', '=', $income->id)->pluck('status')->first();
        if($status == 4){
            $status = "Plătită";
        }else if($status == 3) {
            $status = "Partial plătită";
        }else if($status == 2) {
            $status = "Neplătită";
        }else if($status == 1){
            $status = "Trimisă";
        }else if($status == 0) {
            $status = "Schiță";
        }

        // dd($income);
     $data_income->push([
         
         "Numele facturii" => $income->name,
         "Client" => Invoice::where('income_id', '=', $income->id)->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')->pluck('name')->first(),
         "Data emiterii facturii" => Invoice::where('income_id', '=', $income->id)->pluck('issue_date')->first(),
         "Data platii facturii" => $income->date,
         "Suma" => number_format($income->amount, 2, ',', '.') . 'RON',
         "Proiect" => Project::where('id', '=', $income->project_id)->pluck('project_name')->first(),
         "Sarcina" => ProjectTask::where('id', '=', $income->task_id)->pluck('name')->first(),
         "Numarul contractului" => Invoice::where('income_id', '=', $income->id)->pluck('ref_number')->first(),
         "Descrierea facturii" => $income->description,
         "Stare" => $status
     ]);


 }


         return $data_income;
    }

    public function headings(): array
    {
        return [
            "Numarul facturii",
            "Vanzator",
            "Data facturii",
            "Data platii facturii",
            "Suma",
            "Proiect",
            "Sarcina",
            "Numarul contractului",
            "Descrierea facturii",
            "Stare",
        ];
    }
}
