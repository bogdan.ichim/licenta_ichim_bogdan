<?php

namespace App\Exports;

use App\Models\NewWorkAttendance;
use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements FromCollection, WithHeadings
{
    protected $id;
    protected $start_date;
    protected $end_date;
    
    function __construct($id, $start_date, $end_date) {
        $this->id = $id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data = NewWorkAttendance::where('employee_id', '=', $this->id)
                ->whereDate('work_date', '>=', $this->start_date)
                ->whereDate('work_date', '<=', $this->end_date)
                ->get();

        foreach($data as $k => $clocking)
        {
            unset($clocking->overtime_balance, $clocking->updated_at, $clocking->created_at);
            $data[$k]["employee_id"] = $this->id;
            $data[$k]["start_date_test"] = $this->start_date;
            $data[$k]["end_date_test"] = $this->end_date;
            $data[$k]["employee_name"] = Employee::where('employee_id', '=', $this->id)->pluck('name')->first();
            if($data[$k]['validated_by_admin'] == 0) {
                $data[$k]['validated_by_admin'] = "Se asteaptă aprobarea";
            }else {
                $data[$k]['validated_by_admin'] = "Aprobată";
            }

            if($data[$k]['hours'] == 0) {
                $data[$k]['hours'] = "-";
            } else if ($data[$k]['non_billable_hours'] == 0) {
                $data[$k]['non_billable_hours'] = "-";
            }

            $data[$k] = [
                "Numele angajatului" => $data[$k]["employee_name"],
                "Cod proiect" => $data[$k]["id"],
                "Denumirea proiectului" => $data[$k]["project_name"],
                "Ore" => $data[$k]['hours'],
                "Ore nefacturabile" => $data[$k]['non_billable_hours'],
                "Data si ora" => $data[$k]['work_date'],
                "Faza proiectului" => $data[$k]['milestone'],
                "Activitate" => $data[$k]['phase'],
                "Descrierea sarcinii" => $data[$k]['task_description'],
                "Status" => $data[$k]['validated_by_admin']
            ];
        }

        return collect($data);
    }

    public function headings(): array
    {
        return [
            "Numele angajatului",
            "Cod proiect",
            "Denumirea proiectului",
            "Ore",
            "Ore nefacturabile",
            "Data si ora",
            "Faza proiectului",
            "Activitate",
            "Descrierea sarcinii",
            "Status"
        ];
    }
}
