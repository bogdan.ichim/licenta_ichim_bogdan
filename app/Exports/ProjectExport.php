<?php
namespace App\Exports;

use App\Models\Employee;
use App\Models\Milestone;
use App\Models\NewWorkAttendance;
use App\Models\Project;
use App\Models\ProjectTask;
use App\Models\ProjectUser;
use App\Models\User;
use App\Models\Utility;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProjectExport implements FromCollection, WithHeadings
{
    protected $id;
    
    function __construct($id) {
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data_project = collect();

            $project_details = Project::find($this->id);
            $expenses = 0;
            foreach($project_details->expense as $expense)
            {
                $expenses += $expense->amount;
            }

            $incomes=0;
            foreach($project_details->income as $income)
            {
                $incomes += $income->amount;
            }
            $milestones = Milestone::where('project_id', $project_details->id)->pluck('title')->toArray();
            $milestones_list = implode(", ", $milestones);   
            $phases = ProjectTask::where('project_id', $project_details->id)->pluck('name')->toArray();
            $phases_list = implode(", ", $phases);
            $members_of_project = ProjectUser::where('project_id', $project_details->id)->pluck('user_id');
            $users_list = [];
            $users_list_subcontractors = [];
            
            foreach ($members_of_project as $user_id) {
                $user = User::where('id', $user_id)->where('type', '!=', 'subcontractor')->first();
                $user_subcontractor = User::where('id', $user_id)->where('type', '=', 'subcontractor')->first();
                if ($user) {
                    $users_list[] = $user->name;
                }
                if ($user_subcontractor) {
                    $users_list_subcontractors[] = $user_subcontractor->name;
                }
            }
            $users_string = implode(', ', $users_list);
            $users_subcontractors_string = implode(', ', $users_list_subcontractors);

                $data_project->push([
                    "ID-ul proiectului" => $project_details->id,
                    "Grupul proiectului" => $project_details->project_id_group,
                    "Numele proiectului" => $project_details->project_name,
                    "Numele creatorului proiectului" => User::where('id', $project_details->created_by)->pluck('name')[0],
                    "Descrierea proiectului" => $project_details->description,
                    "Numarul de zile atribuite proiectului" => Carbon::parse($project_details->start_date)->diffInDays(Carbon::parse($project_details->end_date)),
                    "Numarul de zile ramase" => Carbon::parse($project_details->start_date)->diffInDays(now()),
                    "Fazele totale ale proiectului" => $project_details->milestones()->count(),
                    "Fazele completate ale proiectului" => $project_details->milestones()->where('status', 'LIKE', 'complete')->count(),
                    "Activitatile totale ale proiectului" => ProjectTask::where('project_id',$project_details->id)->count(),
                    "Activitatile completate ale proiectului" => ProjectTask::where('project_id',$project_details->id)->where('is_complete',1)->count(),
                    "Orele totale alocate proiectului" => Project::projectHrs($project_details->id)['allocated'],
                    "Orele totale lucrate la proiect" => NewWorkAttendance::where('project_group', $project_details->project_id_group)->where('validated_by_admin', '1')->sum('hours'),
                    "Orele ramase dupa orele lucrate" => number_format(Project::projectHrs($project_details->id)['allocated'] - NewWorkAttendance::where('project_group', $project_details->project_id_group)->where('validated_by_admin', '1')->sum('hours')),
                    "Bugetul alocat initial" => "{$project_details->budget}"." RON",
                    "Cheltuieli" => "{$expenses}"." RON",
                    "Venituri" => "{$incomes}"." RON",
                    "Fazele proiectului" => $milestones_list,
                    "Activitatile proiectului" => $phases_list,
                    "Membrii proiectului" => $users_string,
                    "Subcontractorii proiectului" => $users_subcontractors_string,
                    "Data de inceput al proiectului" =>  Utility::getDateFormated($project_details->start_date),
                    "Data de sfarsit al proiectului" =>  Utility::getDateFormated($project_details->end_date),
                    "Efectuat" => $project_details->project_progress()['percentage']

                ]);

        return $data_project;
    }

    public function headings(): array
    {
        return [
            "ID-ul proiectului",
            "Grupul proiectului",
            "Numele proiectului",
            "Numele creatorului proiectului",
            "Descrierea proiectului",
            "Numarul de zile atribuite proiectului",
            "Numarul de zile ramase",
            "Fazele totale ale proiectului",
            "Fazele completate ale proiectului",
            "Activitatile totale ale proiectului",
            "Activitatile completate ale proiectului",
            "Orele totale alocate proiectului",
            "Orele totale lucrate la proiect",
            "Orele ramase dupa orele lucrate",
            "Bugetul alocat initial",
            "Cheltuieli",
            "Venituri",
            "Fazele proiectului",
            "Activitatile proiectului",
            "Membrii proiectului",
            "Subcontractorii proiectului",
            "Data de inceput al proiectului",
            "Data de sfarsit al proiectului",
            "Efectuat"
        ];
    }
}
