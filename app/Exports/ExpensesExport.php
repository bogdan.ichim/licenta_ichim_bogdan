<?php

namespace App\Exports;

use App\Models\Bill;
use App\Models\BillProduct;
use App\Models\Expense;
use App\Models\Project;
use App\Models\ProjectTask;
use App\Models\Vender;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;


class ExpensesExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

     protected $project_id; 

     public function __construct($project_id) {
         $this->project_id = $project_id;
     }
    public function collection()
    {
        $data_expense = collect();
        $projectID = $this->project_id;

        $expenses = Expense::where('project_id', '=', $projectID)->get();
        
    foreach($expenses as $expense) {
        $status = Bill::where('expense_id', '=', $expense->id)->pluck('status')->first();
        if($status == 4){
            $status = "Plătită";
        }else if($status == 3) {
            $status = "Partial plătită";
        }else if($status == 2) {
            $status = "Neplătită";
        }else if($status == 1){
            $status = "Trimisă";
        }else if($status == 0) {
            $status = "Schiță";
        }
     $data_expense->push([
         
         "Numele facturii" => $expense->name,
         "Vanzator" => Bill::where('expense_id', '=', $expense->id)->join('venders', 'bills.vender_id', '=', 'venders.id')->pluck('name')->first(),
         "Data facturii" => Bill::where('expense_id', '=', $expense->id)->pluck('bill_date')->first(), 
         "Data platii facturii" => $expense->date,
         "Suma" => number_format($expense->amount, 2, ',', '.') . 'RON',
         "Proiect" => Project::where('id', '=', $expense->project_id)->pluck('project_name')->first(),
         "Sarcina" => ProjectTask::where('id', '=', $expense->task_id)->pluck('name')->first(),
         "Descrierea facturii" => $expense->description,
         "Stare" => $status
     ]);


 }
        return $data_expense;
    }

    public function headings(): array
    {
        return [
            "Numarul facturii",
            "Vanzator",
            "Data facturii",
            "Data platii facturii",
            "Suma",
            "Proiect",
            "Sarcina",
            "Descrierea facturii",
            "Stare",
        ];
    }
}
