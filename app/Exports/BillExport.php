<?php

namespace App\Exports;

use App\Models\Bill;
use App\Models\BillProduct;
use App\Models\Expense;
use App\Models\ProductServiceCategory;
use App\Models\Project;
use App\Models\Vender;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BillExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data_bill = collect();
        $bill_details = Bill::get();


        
    foreach($bill_details as $bill_detail) {
        if($bill_detail->status == 4) {    
            $bill_detail->status = "Plătită";
        }else if ($bill_detail->status == 3) {
            $bill_detail->status = "Partial plătită";
        }else if($bill_detail->status == 2) {
            $bill_detail->status = "Neplătită";
        }else if($bill_detail->status == 1) {
            $bill_detail->status = "Trimisă";
        }else if($bill_detail->status == 0) {
            $bill_detail->status = "Schiță";
        }

     $data_bill->push([
         
         "Numarul facturii" => $bill_detail->id,
         "Vanzator" => Vender::where('vender_id', $bill_detail->vender_id)->pluck('name')->first(),
         "Data facturii" =>$bill_detail->bill_date,
         "Data platii facturii" =>$bill_detail->paid_date,
         "Suma" =>Expense::where('id', $bill_detail->expense_id)->pluck('amount')->first()." RON",
         "Proiect" =>Project::where('id', $bill_detail->project_id)->pluck('project_name')->first(),
         "Sarcina" =>BillProduct::where('bill_id', $bill_detail->id)->pluck('description')->first(),
         "Stare" =>$bill_detail->status,
     ]);

 }

        return $data_bill;
    }

    public function headings(): array
    {
        return [
            "Numarul facturii",
            "Vanzator",
            "Data facturii",
            "Data platii facturii",
            "Suma",
            "Proiect",
            "Sarcina",
            "Stare",
        ];
    }
}
