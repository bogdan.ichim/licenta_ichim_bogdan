<?php

namespace App\Imports;

use App\Models\Bill;
use App\Models\ProductService;
use App\Models\BillProduct;
use App\Models\Expense;
use App\Models\Project;
use App\Models\Vender;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

function billNumber()
{
    $latest = Bill::where('created_by', '=', \Auth::user()->creatorId())->latest()->first();
    if(!$latest)
    {
        return 1;
    }

    return $latest->bill_id + 1;
}

function billId(){
    $latest = Bill::where('created_by', '=', \Auth::user()->creatorId())->latest()->first();
    if(!$latest)
    {
        return 1;
    }

    return $latest->id + 1;
}




function venderID($venderNameString) {
    $venderId = Vender::where('name', '=', $venderNameString)->pluck('id')->first();

    return $venderId;
}

function projectID($projectNameString) {
    $projectId = Project::where('project_name', '=', $projectNameString)->pluck('id')->first();

    return $projectId;
}

function productID($productNameString) {
    $productId = ProductService::where('name', '=', $productNameString)->pluck('id')->first();

    return $productId;
}

function expenseID($expenseNameString) {
    $expenseId = Expense::where('name', '=', $expenseNameString)->pluck('id')->first();

    return $expenseId;
}

function transformIntToDouble($value)
{
    return sprintf("%.10f", $value);
}

function transformIntToDoubleV2($value) {
    return number_format($value, 2, '.', '');
  }




class BillsImport implements ToModel, WithHeadingRow
{


    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $bill = new Bill([
            "id" => billId(),
            "bill_id" => billNumber(),
            "vender_id" => venderID($row['vender_name']),
            "bill_date" => Carbon::createFromFormat('Y-m-d', '1900-01-01')->addDays($row['bill_date'] - 2)->format('Y-m-d'),
            "due_date" => Carbon::createFromFormat('Y-m-d', '1900-01-01')->addDays($row['due_date'] - 2)->format('Y-m-d'),
            "order_number" => 0,
            "status" => 0,
            "shipping_display" => 1,
            "send_date" => Carbon::createFromFormat('Y-m-d', '1900-01-01')->addDays($row['send_date'] - 2)->format('Y-m-d'),
            "discount_apply" => 1,
            "category_id" => null,
            "created_by" => 2,
            "created_at" => Carbon::now()->toDateTimeString(),
            "updated_at" => Carbon::now()->toDateTimeString(),
            "project_id" => projectID($row['project_name']),
            "expense_id" => expenseID($row['expense_id']),
            "paid_date" => Carbon::createFromFormat('Y-m-d', '1900-01-01')->addDays($row['paid_date'] - 2)->format('Y-m-d')
        ]);

        for ($i = 1; $i <= $row['product_count']; $i++) {
            $productService = new ProductService([
                "name" => $row['product_'.$i.'_name'],
                "sku" => Carbon::now()->toDateTimeString(),
                "sale_price" => transformIntToDouble($row['product_'.$i.'_sale_price']),
                "purchase_price" => transformIntToDoubleV2($row['product_'.$i.'_sale_price']),
                "quantity" => 0,
                "tax_id" => null,
                "category_id" => 2, 
                "unit_id" => 3,
                "type" => 1,
                "created_by" => 2,
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
            ]);

            $bill->ProductService()->save($productService);
        
        }
        
        for ($i = 1; $i <= $row['product_count']; $i++) {
            $billProduct = new BillProduct([
                "bill_id" => billId(),
                "product_id" => productID($row['product_'.$i.'_name']),
                "quantity" => $row['product_'.$i.'_quantity'],
                "tax" => $row['product_'.$i.'_tax'],
                "discount" =>  transformIntToDoubleV2($row['product_'.$i.'_tax']),
                "price" => transformIntToDoubleV2($row['product_'.$i.'_sale_price']),
                "description" => $row['product_'.$i.'_description'],
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString(),
            ]);

            $bill->BillProductService()->save($billProduct);
        }

        // echo $bill;
        // // echo $productService;
        // echo $billProduct;
        // dd();


        return $bill;
    }
}






