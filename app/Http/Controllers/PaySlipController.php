<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\ProjectUser;
use App\Models\Allowance;
use App\Models\Commission;
use App\Models\Employee;
use App\Models\Loan;
use App\Models\Mail\InvoiceSend;
use App\Models\Mail\PayslipSend;
use App\Models\OtherPayment;
use App\Models\WorkTaskAttendance;
use App\Models\Overtime;
use App\Models\PaySlip;
use App\Models\SaturationDeduction;
use App\Models\Utility;
use App\Models\Expense;
use App\Models\AdminCost;
use App\Models\Project;
use App\Models\ProjectTask;
use App\Models\ProductService;
use App\Models\Bill;
use App\Models\BillProduct;
use App\Models\ProductServiceUnit;
use App\Models\ProductServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DB;

class PaySlipController extends Controller
{

    public function index()
    {
        if(\Auth::user()->can('manage pay slip') || \Auth::user()->type != 'client' || \Auth::user()->type != 'company')
        {
            $employees = Employee::where(
                [
                    'created_by' => \Auth::user()->creatorId(),
                ]
            )->first();

            $month = [
                '01' => 'JAN',
                '02' => 'FEB',
                '03' => 'MAR',
                '04' => 'APR',
                '05' => 'MAY',
                '06' => 'JUN',
                '07' => 'JUL',
                '08' => 'AUG',
                '09' => 'SEP',
                '10' => 'OCT',
                '11' => 'NOV',
                '12' => 'DEC',
            ];

            $year = [
                '2020' => '2020',
                '2021' => '2021',
                '2022' => '2022',
                '2023' => '2023',
                '2024' => '2024',
                '2025' => '2025',
                '2026' => '2026',
                '2027' => '2027',
                '2028' => '2028',
                '2029' => '2029',
                '2030' => '2030',
            ];

            return view('payslip.index', compact('employees', 'month', 'year'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $validator = \Validator::make(
            $request->all(), [
                               'month' => 'required',
                               'year' => 'required',
                           ]
        );

        if($validator->fails())
        {
            $messages = $validator->getMessageBag();

            return redirect()->back()->with('error', $messages->first());
        }

        $month = $request->month;
        $year  = $request->year;



        $formate_month_year = $year . '-' . $month;
        $validatePaysilp    = PaySlip::where('salary_month', '=', $formate_month_year)->where('created_by', \Auth::user()->creatorId())->pluck('employee_id');
        $payslip_employee   = Employee::where('created_by', \Auth::user()->creatorId())->where('company_doj', '<=', date($year . '-' . $month . '-t'))->count();
        if($payslip_employee > count($validatePaysilp))
        {
            $employees = Employee::where('created_by', \Auth::user()->creatorId())->where('company_doj', '<=', date($year . '-' . $month . '-t'))->whereNotIn('employee_id', $validatePaysilp)->get();

            $employeesSalary = Employee::where('created_by', \Auth::user()->creatorId())->where('salary', '<=', 0)->first();

            if(!empty($employeesSalary))
            {
                return redirect()->route('payslip.index')->with('error', __('Please set employee salary.'));
            }

            $total_payslip_expense = 0;

            $projects_expenses = [];

            foreach($employees as $employee)
            {

                $payslipEmployee                       = new PaySlip();
                $payslipEmployee->employee_id          = $employee->id;
                $payslipEmployee->net_payble           = $employee->get_net_salary();
                $payslipEmployee->salary_month         = $formate_month_year;
                $payslipEmployee->status               = 0;
                $payslipEmployee->basic_salary         = !empty($employee->salary) ? $employee->salary : 0;
                $payslipEmployee->allowance            = Employee::allowance($employee->id);
                $payslipEmployee->commission           = Employee::commission($employee->id);
                $payslipEmployee->loan                 = Employee::loan($employee->id);
                $payslipEmployee->saturation_deduction = Employee::saturation_deduction($employee->id);
                $payslipEmployee->other_payment        = Employee::other_payment($employee->id);
                $payslipEmployee->overtime             = Employee::overtime($employee->id);
                $payslipEmployee->created_by           = \Auth::user()->creatorId();

                $payslipEmployee->save();

                $total_payslip_expense += $payslipEmployee->net_payble;

                $actual_user = User::where('id', $employee->user_id)->first();

                $project_participating = ProjectUser::where('user_id', $actual_user->id)->get();

                $projects_expenses_local = [];

                foreach($project_participating as $project_user){
                  array_push($projects_expenses_local, $project_user->project_id);
                }

                foreach($projects_expenses_local as $project_id){
                  if(array_key_exists($project_id, $projects_expenses)){
                    $projects_expenses[$project_id] = [];
                  }
                  $users_working = ProjectUser::where('project_id', $project_id)->get();

                  $project_tasks = [];

                  $project_tasks_raw = ProjectTask::where('project_id', $project_id)->get();

                  if(count($project_tasks_raw)<1){
                    continue;
                  }

                  foreach($project_tasks_raw as $project_task){
                    array_push($project_tasks, $project_task->id);
                  }

                  $worked_hours_project = WorkTaskAttendance::where('employee_id', $employee->id)
                  ->whereIn('task_id', $project_tasks)->get([
                    DB::raw('SUM(hours) as worked_hours'),
                  ]);

                  if(count($worked_hours_project)<1){
                    continue;
                  }

                  $price_per_hour = AdminCostsController::calculate_hourly_salary($employee);

                  $projects_expenses[$project_id][$actual_user->id] = $worked_hours_project[0]->worked_hours*$price_per_hour;
                }

                //Slack Notification
                $setting  = Utility::settings(\Auth::user()->creatorId());
                if(isset($setting['payslip_notification']) && $setting['payslip_notification'] ==1){
                    $msg = __("Payslip generated of").' '.$formate_month_year.'.';
                    Utility::send_slack_msg($msg);
                }

                //Telegram Notification
                $setting  = Utility::settings(\Auth::user()->creatorId());
                if(isset($setting['telegram_payslip_notification']) && $setting['telegram_payslip_notification'] ==1){
                    $msg = __("Payslip generated of").' '.$formate_month_year.'.';
                    Utility::send_telegram_msg($msg);
                }
            }

            foreach($projects_expenses as $project_id => $project_array){
              foreach($project_array as $user_id => $value){
                $current_project = Project::find($project_id);

                if($current_project==null){
                  continue;
                }

                if($value<=0){
                  continue;
                }

                $actual_user = User::find($user_id);

                $expense = new Expense;
                $expense->name = 'Cheltuieli '.$actual_user->name.' '.$year . '-' . $month.' Proiect: '.$current_project->project_name;
                $expense->date = Carbon::now();
                $expense->description = 'Cheltuieli salarii, luna '.$month.' anul '.$year.'. Proiect: '.$current_project->project_name;
                $expense->amount = $value;
                $expense->created_by = \Auth::user()->creatorId();
                $expense->type = 'Salary Expense/Project';
                $expense->project_id = $current_project->id;
                $expense->save();

                $bill = new Bill;

                $last_bill = Bill::where('created_by', \Auth::user()->creatorId())->orderBy('id', 'DESC')->first();
                $last_bill_id = 1;
                if($last_bill){
                    $last_bill_id = $last_bill->bill_id+1;
                }

                $bill->bill_id = $last_bill_id;
                $bill->bill_date = Carbon::now();
                $bill->due_date = Carbon::now();
                $bill->order_number = 0;
                $bill->status = 4;
                $bill->shipping_display = 1;
                $bill->send_date = Carbon::now();
                $bill->discount_apply = 1;
                $bill->created_by = \Auth::user()->creatorId();
                $bill->project_id = $current_project->id;
                $bill->paid_date = Carbon::now();
                $bill->expense_id = $expense->id;
                $bill->save();

                $unit = ProductServiceUnit::where('name', 'Buc.')->first();
                if(!$unit){
                  $unit = new ProductServiceUnit;
                  $unit->name = 'Buc.';
                  $unit->created_by = \Auth::user()->creatorId();
                  $unit->save();
                }

                $product_category = ProductServiceCategory::where('type', 0)->first();
                if(!$product_category){
                  $product_category = new ProductServiceCategory;
                  $product_category->name = 'Salarii';
                  $product_category->type = 0;
                  $product_category->color = 'FFFFFF';
                  $product_category->created_by = \Auth::user()->creatorId();
                  $product_category->save();
                }

                $product = new ProductService;
                $product->name = 'Cheltuieli '.$actual_user->name.' '.$year . '-' . $month.' Proiect: '.$current_project->project_name;
                $product->sku = Carbon::now();
                $product->sale_price = $value;
                $product->purchase_price = $value;
                $product->quantity = 1;
                $product->unit_id = $unit->id;
                $product->category_id = $product_category->id;
                $product->description = 'Cheltuieli '.$actual_user->name.' '.$year . '-' . $month.' Proiect: '.$current_project->project_name;
                $product->created_by = \Auth::user()->creatorId();
                $product->save();

                $bill_product = new BillProduct;
                $bill_product->bill_id = $bill->id;
                $bill_product->product_id = $product->id;
                $bill_product->quantity = 1;
                $bill_product->discount = 0;
                $bill_product->price = $value;
                $bill_product->description = 'Cheltuieli '.$actual_user->name.' '.$year . '-' . $month.' Proiect: '.$current_project->project_name;
                $bill_product->save();
              }
            }

            /*
            $expense = new Expense;
            $expense->name = 'Cheltuiala salarii '.$year . '-' . $month;
            $expense->date = Carbon::now();
            $expense->description = 'Cheltuiala salarii nete, luna '.$month.' anul '.$year;
            $expense->amount = $total_payslip_expense;
            $expense->created_by = \Auth::user()->creatorId();
            $expense->type = 'Salary Expense';
            $expense->save();
            */

            $admin_costs = AdminCost::where('created_by', \Auth::user()->creatorId())->get();

            $total_admin_costs_expense = 0;

            foreach($admin_costs as $admin_cost)
            {
              if($admin_cost->period == 'monthly'){
                $total_admin_costs_expense += $admin_cost->cost;
              }else{
                $total_admin_costs_expense += $admin_cost->cost/12;
              }
            }

            /*
            $expense = new Expense;
            $expense->name = 'Cheltuieli administrative '.$year . '-' . $month;
            $expense->date = Carbon::now();
            $expense->description = 'Cheltuieli administrative, luna '.$month.' anul '.$year;
            $expense->amount = $total_admin_costs_expense;
            $expense->created_by = \Auth::user()->creatorId();
            $expense->type = 'Admin Expense';
            $expense->save();
            */

            $projects = Project::where('created_by', \Auth::user()->creatorId())->get();

            foreach($projects as $project){
              $expense = new Expense;
              $expense->name = 'Cheltuieli administrative '.$year . '-' . $month.' Proiect: '.$project->project_name;
              $expense->date = Carbon::now();
              $expense->description = 'Cheltuieli administrative, luna '.$month.' anul '.$year.'. Proiect: '.$project->project_name;
              $expense->amount = $total_admin_costs_expense / count($projects);
              $expense->created_by = \Auth::user()->creatorId();
              $expense->type = 'Administrative Expense';
              $expense->project_id = $project->id;
              $expense->save();

              $bill = new Bill;

              $last_bill = Bill::where('created_by', \Auth::user()->creatorId())->orderBy('id', 'DESC')->first();
              $last_bill_id = 1;
              if($last_bill){
                  $last_bill_id = $last_bill->bill_id+1;
              }

              $bill->bill_id = $last_bill_id;
              $bill->bill_date = Carbon::now();
              $bill->due_date = Carbon::now();
              $bill->order_number = 0;
              $bill->status = 4;
              $bill->shipping_display = 1;
              $bill->send_date = Carbon::now();
              $bill->discount_apply = 1;
              $bill->created_by = \Auth::user()->creatorId();
              $bill->project_id = $project->id;
              $bill->paid_date = Carbon::now();
              $bill->expense_id = $expense->id;
              $bill->save();

              $unit = ProductServiceUnit::where('name', 'Buc.')->first();
              if(!$unit){
                $unit = new ProductServiceUnit;
                $unit->name = 'Buc.';
                $unit->created_by = \Auth::user()->creatorId();
                $unit->save();
              }

              $product_category = ProductServiceCategory::where('type', 0)->first();
              if(!$product_category){
                $product_category = new ProductServiceCategory;
                $product_category->name = 'Cheltuieli administrative';
                $product_category->type = 0;
                $product_category->color = 'FFFFFF';
                $product_category->created_by = \Auth::user()->creatorId();
                $product_category->save();
              }

              $product = new ProductService;
              $product->name = 'Cheltuiala administrativa luna '.$month.' anul '.$year.'. Proiect: '.$project->project_name;
              $product->sku = Carbon::now();
              $product->sale_price = $total_admin_costs_expense / count($projects);
              $product->purchase_price = $total_admin_costs_expense / count($projects);
              $product->quantity = 1;
              $product->unit_id = $unit->id;
              $product->category_id = $product_category->id;
              $product->description = 'Cheltuiala administrativa luna '.$month.' anul '.$year.'. Proiect: '.$project->project_name;
              $product->created_by = \Auth::user()->creatorId();
              $product->save();

              $bill_product = new BillProduct;
              $bill_product->bill_id = $bill->id;
              $bill_product->product_id = $product->id;
              $bill_product->quantity = 1;
              $bill_product->discount = 0;
              $bill_product->price = $total_admin_costs_expense / count($projects);
              $bill_product->description = 'Cheltuiala administrativa luna '.$month.' anul '.$year.'. Proiect: '.$project->project_name;
              $bill_product->save();

            }

            return redirect()->route('payslip.index')->with('success', __('Payslip successfully created.'));
        }
        else
        {
            return redirect()->route('payslip.index')->with('error', __('Payslip Already created.'));
        }

    }

    public function destroy($id)
    {
        $payslip = PaySlip::find($id);
        $payslip->delete();

        return true;
    }

    public function showemployee($paySlip)
    {
        $payslip = PaySlip::find($paySlip);

        return view('payslip.show', compact('payslip'));
    }


    public function search_json(Request $request)
    {

        $formate_month_year = $request->datePicker;
        $validatePaysilp    = PaySlip::where('salary_month', '=', $formate_month_year)->where('created_by', \Auth::user()->creatorId())->get()->toarray();


        if(empty($validatePaysilp))
        {
            return;
        }
        else
        {
            $paylip_employee = PaySlip::select(
                [
                    'employees.id',
                    'employees.employee_id',
                    'employees.name',
                    'payslip_types.name as payroll_type',
                    'pay_slips.basic_salary',
                    'pay_slips.net_payble',
                    'pay_slips.id as pay_slip_id',
                    'pay_slips.status',
                    'employees.user_id',
                ]
            )->leftjoin(
                'employees', function ($join) use ($formate_month_year){
                $join->on('employees.id', '=', 'pay_slips.employee_id');
                $join->on('pay_slips.salary_month', '=', \DB::raw("'" . $formate_month_year . "'"));
                $join->leftjoin('payslip_types', 'payslip_types.id', '=', 'employees.salary_type');
            }
            )->where('employees.created_by', \Auth::user()->creatorId())->get();


            foreach($paylip_employee as $employee)
            {

                if(Auth::user()->type == 'employee')
                {
                    if(Auth::user()->id == $employee->user_id)
                    {
                        $tmp   = [];
                        $tmp[] = $employee->id;
                        $tmp[] = $employee->name;
                        $tmp[] = $employee->payroll_type;
                        $tmp[] = $employee->pay_slip_id;
                        $tmp[] = !empty($employee->basic_salary) ? \Auth::user()->priceFormat($employee->basic_salary) : '-';
                        $tmp[] = !empty($employee->net_payble) ? \Auth::user()->priceFormat($employee->net_payble) : '-';
                        if($employee->status == 1)
                        {
                            $tmp[] = 'paid';
                        }
                        else
                        {
                            $tmp[] = 'unpaid';
                        }
                        $tmp[]  = !empty($employee->pay_slip_id) ? $employee->pay_slip_id : 0;
                        $data[] = $tmp;
                    }
                }
                else
                {

                    $tmp   = [];
                    $tmp[] = $employee->id;
                    $tmp[] = \Auth::user()->employeeIdFormat($employee->employee_id);
                    $tmp[] = $employee->name;
                    $tmp[] = $employee->payroll_type;
                    $tmp[] = !empty($employee->basic_salary) ? \Auth::user()->priceFormat($employee->basic_salary) : '-';
                    $tmp[] = !empty($employee->net_payble) ? \Auth::user()->priceFormat($employee->net_payble) : '-';
                    if($employee->status == 1)
                    {
                        $tmp[] = 'Paid';
                    }
                    else
                    {
                        $tmp[] = 'UnPaid';
                    }
                    $tmp[]  = !empty($employee->pay_slip_id) ? $employee->pay_slip_id : 0;
                    $data[] = $tmp;
                }

            }

            return $data;
        }
    }

    public function paysalary($id, $date)
    {
        $employeePayslip = PaySlip::where('employee_id', '=', $id)->where('created_by', \Auth::user()->creatorId())->where('salary_month', '=', $date)->first();
        if(!empty($employeePayslip))
        {
            $employeePayslip->status = 1;
            $employeePayslip->save();

            return redirect()->route('payslip.index')->with('success', __('Payslip Payment successfully.'));
        }
        else
        {
            return redirect()->route('payslip.index')->with('error', __('Payslip Payment failed.'));
        }

    }

    public function bulk_pay_create($date)
    {
        $Employees       = PaySlip::where('salary_month', $date)->where('created_by', \Auth::user()->creatorId())->get();
        $unpaidEmployees = PaySlip::where('salary_month', $date)->where('created_by', \Auth::user()->creatorId())->where('status', '=', 0)->get();

        return view('payslip.bulkcreate', compact('Employees', 'unpaidEmployees', 'date'));
    }

    public function bulkpayment(Request $request, $date)
    {
        $unpaidEmployees = PaySlip::where('salary_month', $date)->where('created_by', \Auth::user()->creatorId())->where('status', '=', 0)->get();

        foreach($unpaidEmployees as $employee)
        {
            $employee->status = 1;
            $employee->save();
        }

        return redirect()->route('payslip.index')->with('success', __('Payslip Bulk Payment successfully.'));
    }

    public function employeepayslip()
    {
        $employees = Employee::where(
            [
                'user_id' => \Auth::user()->id,
            ]
        )->first();

        $payslip = PaySlip::where('employee_id', '=', $employees->id)->get();

        return view('payslip.employeepayslip', compact('payslip'));

    }

    public function pdf($id, $month)
    {

        $payslip  = PaySlip::where('employee_id', $id)->where('salary_month', $month)->where('created_by', \Auth::user()->creatorId())->first();
        $employee = Employee::find($payslip->employee_id);

        $payslipDetail = Utility::employeePayslipDetail($id);

        return view('payslip.pdf', compact('payslip', 'employee', 'payslipDetail'));
    }

    public function send($id, $month)
    {
        $payslip  = PaySlip::where('employee_id', $id)->where('salary_month', $month)->where('created_by', \Auth::user()->creatorId())->first();
        $employee = Employee::find($payslip->employee_id);

        $payslip->name  = $employee->name;
        $payslip->email = $employee->email;

        $payslipId    = Crypt::encrypt($payslip->id);
        $payslip->url = route('payslip.payslipPdf', $payslipId);

        $setings = Utility::settings();
        if($setings['payroll_create'] == 1)
        {
            try
            {
                Mail::to($payslip->email)->send(new PayslipSend($payslip));
            }
            catch(\Exception $e)
            {
                $smtp_error = __('E-Mail has been not sent due to SMTP configuration');
            }

            return redirect()->back()->with('success', __('Payslip successfully sent.') . (isset($smtp_error) ? $smtp_error : ''));
        }

        return redirect()->back()->with('success', __('Payslip successfully sent.'));

    }

    public function payslipPdf($id)
    {
        $payslipId = Crypt::decrypt($id);

        $payslip  = PaySlip::where('id', $payslipId)->where('created_by', \Auth::user()->creatorId())->first();
        $employee = Employee::find($payslip->employee_id);

        $payslipDetail = Utility::employeePayslipDetail($payslip->employee_id);

        return view('payslip.payslipPdf', compact('payslip', 'employee', 'payslipDetail'));
    }

    public function editEmployee($paySlip)
    {
        $payslip = PaySlip::find($paySlip);

        return view('payslip.salaryEdit', compact('payslip'));
    }

    public function updateEmployee(Request $request, $id)
    {


        if(isset($request->allowance) && !empty($request->allowance))
        {
            $allowances   = $request->allowance;
            $allowanceIds = $request->allowance_id;
            foreach($allowances as $k => $allownace)
            {
                $allowanceData         = Allowance::find($allowanceIds[$k]);
                $allowanceData->amount = $allownace;
                $allowanceData->save();
            }
        }


        if(isset($request->commission) && !empty($request->commission))
        {
            $commissions   = $request->commission;
            $commissionIds = $request->commission_id;
            foreach($commissions as $k => $commission)
            {
                $commissionData         = Commission::find($commissionIds[$k]);
                $commissionData->amount = $commission;
                $commissionData->save();
            }
        }

        if(isset($request->loan) && !empty($request->loan))
        {
            $loans   = $request->loan;
            $loanIds = $request->loan_id;
            foreach($loans as $k => $loan)
            {
                $loanData         = Loan::find($loanIds[$k]);
                $loanData->amount = $loan;
                $loanData->save();
            }
        }


        if(isset($request->saturation_deductions) && !empty($request->saturation_deductions))
        {
            $saturation_deductionss   = $request->saturation_deductions;
            $saturation_deductionsIds = $request->saturation_deductions_id;
            foreach($saturation_deductionss as $k => $saturation_deductions)
            {

                $saturation_deductionsData         = SaturationDeduction::find($saturation_deductionsIds[$k]);
                $saturation_deductionsData->amount = $saturation_deductions;
                $saturation_deductionsData->save();
            }
        }


        if(isset($request->other_payment) && !empty($request->other_payment))
        {
            $other_payments   = $request->other_payment;
            $other_paymentIds = $request->other_payment_id;
            foreach($other_payments as $k => $other_payment)
            {
                $other_paymentData         = OtherPayment::find($other_paymentIds[$k]);
                $other_paymentData->amount = $other_payment;
                $other_paymentData->save();
            }
        }


        if(isset($request->rate) && !empty($request->rate))
        {
            $rates   = $request->rate;
            $rateIds = $request->rate_id;
            $hourses = $request->hours;

            foreach($rates as $k => $rate)
            {
                $overtime        = Overtime::find($rateIds[$k]);
                $overtime->rate  = $rate;
                $overtime->hours = $hourses[$k];
                $overtime->save();
            }
        }


        $payslipEmployee                       = PaySlip::find($request->payslip_id);
        $payslipEmployee->allowance            = Employee::allowance($payslipEmployee->employee_id);
        $payslipEmployee->commission           = Employee::commission($payslipEmployee->employee_id);
        $payslipEmployee->loan                 = Employee::loan($payslipEmployee->employee_id);
        $payslipEmployee->saturation_deduction = Employee::saturation_deduction($payslipEmployee->employee_id);
        $payslipEmployee->other_payment        = Employee::other_payment($payslipEmployee->employee_id);
        $payslipEmployee->overtime             = Employee::overtime($payslipEmployee->employee_id);
        $payslipEmployee->net_payble           = Employee::find($payslipEmployee->employee_id)->get_net_salary();
        $payslipEmployee->save();

        return redirect()->route('payslip.index')->with('success', __('Employee payroll successfully updated.'));
    }
}
