<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use Carbon\Carbon;
use App\Models\AdminCost;
use App\Models\Employee;

class AdminCostsController extends Controller
{
    //
    public static $holidays = [
    ];

    public function __construct(){
      AdminCostsController::$holidays = [
          Carbon::create(Carbon::now()->year, 12, 25),
          Carbon::create(Carbon::now()->year, 12, 26),
          Carbon::create(Carbon::now()->year, 12, 27),
      ];
    }

    function index(Request $request){
      if(Gate::check('manage employee')){

        $costs = AdminCost::get();

        return view('admin-costs.index', [
          'costs'=> $costs,
        ]);
      }else{
        abort(403);
      }
    }

    function create(Request $request){
      if(Gate::check('manage employee')){
        return view('admin-costs.create');
      }
    }

    function store(Request $request){
      if(!Gate::check('manage employee')){
        abort(403);
      }


      $validator = \Validator::make(
          $request->all(), [
                             'name' => 'required',
                             'cost' => 'required|numeric|min:0',
                             'period' => 'required',
                         ]
      );
      if($validator->fails())
      {
          $messages = $validator->getMessageBag();

          return redirect()->back()->with('error', $messages->first());
      }

      $name = $request->name;
      $cost = $request->cost;
      $period = $request->period;

      $remote = false;
      if($request->has('remote') && $request->remote == 'on'){
        $remote = true;
      }

      $adminCost = new AdminCost;
      $adminCost->name = $name;
      $adminCost->cost = $cost;
      $adminCost->period = $period;
      $adminCost->remote = $remote;
      $adminCost->created_by = \Auth::user()->creatorId();
      $adminCost->save();

      return back()->with('success', 'Cost created successfully');
    }

    function destroy($cost_id){
      if(!Gate::check('manage employee')){
        abort(403);
      }

      $adminCost = AdminCost::find($cost_id);
      $adminCost->delete();

      return back()->with('success', 'Cost deleted successfully');
    }

    function edit($cost_id){
      if(!Gate::check('manage employee')){
        abort(403);
      }

      $adminCost = AdminCost::find($cost_id);

      return view('admin-costs.edit', [
        'cost'=>$adminCost,
      ]);
    }

    function update(Request $request){
      if(!Gate::check('manage employee')){
        abort(403);
      }

      $validator = \Validator::make(
          $request->all(), [
                             'name' => 'required',
                             'cost' => 'required|numeric|min:0',
                             'period' => 'required',
                         ]
      );
      if($validator->fails())
      {
          $messages = $validator->getMessageBag();

          return redirect()->back()->with('error', $messages->first());
      }

      $cost_id = $request->input('cost-id');

      $name = $request->name;
      $cost = $request->cost;
      $period = $request->period;

      $remote = false;
      if($request->has('remote') && $request->remote == 'on'){
        $remote = true;
      }

      $adminCost = AdminCost::find($cost_id);

      $adminCost->name = $name;
      $adminCost->cost = $cost;
      $adminCost->period = $period;
      $adminCost->remote = $remote;
      $adminCost->save();

      return back()->with('success', 'Cost edited successfully');
    }

    public static function get_current_working_days(){

      $start = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month, 1);
      $end = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month,
                                    Carbon::now()->endOfMonth()->day);

      $holidays = [
          Carbon::create(Carbon::now()->year, 12, 25),
          Carbon::create(Carbon::now()->year, 12, 26),
          Carbon::create(Carbon::now()->year, 12, 27),
      ];

      $days = $start->diffInDaysFiltered(function (Carbon $date) use ($holidays) {

          return $date->isWeekday() && !in_array($date, $holidays);

      }, $end);
      return $days;
    }

    public static function calculate_administrative_cost(AdminCost $cost){
      $employees = Employee::where('created_by', \Auth::user()->creatorId());
      if(!$cost->remote){
        $employees = $employees->where('is_remote', false);
      }
      $employees = $employees->get();

      $start = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month, 1);
      $end = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month,
                                    Carbon::now()->endOfMonth()->day);

      if($cost->period == 'yearly'){
        $start = Carbon::now()->setDate(Carbon::now()->year, 1, 1);
        $end = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month,
                                      31);
      }

      $holidays = AdminCostsController::$holidays;

      $days = $start->diffInDaysFiltered(function (Carbon $date) use ($holidays) {

          return $date->isWeekday() && !in_array($date, $holidays);

      }, $end);

      $hours_worked = 0;
      foreach ($employees as $employee) {
        $hours_worked += $employee->working_hours;
      }

      $hourly_cost = $cost->cost/$days/$hours_worked;

      return [floatval(number_format((float)$hourly_cost, 2, '.', '')), $cost->remote];
    }

    public static function calculate_all_administrative_costs(){
      $costs = AdminCost::where('created_by', \Auth::user()->creatorId())->get();

      $all_costs = [];

      foreach ($costs as $cost) {
        $all_costs[$cost->name] = AdminCostsController::calculate_administrative_cost($cost);
      }

      return $all_costs;
    }

    public static function calculate_hourly_salary($employee){
      if(!is_a($employee, 'App\Models\Employee')){
        $employee = Employee::find($employee);
      }

      $start = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month, 1);
      $end = Carbon::now()->setDate(Carbon::now()->year, Carbon::now()->month,
                                    Carbon::now()->endOfMonth()->day);
       

      $holidays = AdminCostsController::$holidays;

      $days = $start->diffInDaysFiltered(function (Carbon $date) use ($holidays) {
          return $date->isWeekday() && !in_array($date, $holidays);
      }, $end);

      print_r($days);

      $hourly_cost = $employee->get_net_salary()/$days/$employee->working_hours;

      return floatval(number_format((float)$hourly_cost, 2, '.', ''));
    }
}
