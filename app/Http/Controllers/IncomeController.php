<?php

namespace App\Http\Controllers;

use App\Exports\IncomesExport;
use App\Models\Income;
use App\Models\Project;
use App\Models\Utility;
use App\Models\ActivityLog;
use App\Models\ProjectTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class IncomeController extends Controller
{
    public function index($project_id)
    {
        if(\Auth::user()->can('manage expense'))
        {
            $project     = Project::find($project_id);
            $amount      = $project->income->sum('amount');
            $income_cnt  = Utility::projectCurrencyFormat($project_id, $amount) . '/' . Utility::projectCurrencyFormat($project_id, $project->budget);

            $incomes = $project->incomes;
            $invoices = [];
            for ($i=0; $i < count($incomes); $i++) {
              array_push($invoices, $incomes[$i]->invoice[0]);
            }

            return view('incomes.index', compact('project', 'invoices', 'income_cnt'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function create($project_id)
    {
        if(\Auth::user()->can('create expense'))
        {
            $project = Project::find($project_id);

            return view('incomes.create', compact('project'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function store(Request $request, $project_id)
    {
        if(\Auth::user()->can('create expense'))
        {
            $usr       = \Auth::user();
            $validator = Validator::make(
                $request->all(), [
                                'name' => 'required|max:120',
                                'amount' => 'required|numeric|min:0',
                            ]
            );

            if($validator->fails())
            {
                return redirect()->back()->with('error', Utility::errorFormat($validator->getMessageBag()));
            }

            $post               = $request->all();
            $post['project_id'] = $project_id;
            $post['date']       = date("Y-m-d H:i:s", strtotime($request->date));
            $post['created_by'] = $usr->id;

            if($request->hasFile('attachment'))
            {
                $fileNameToStore    = time() . '.' . $request->attachment->getClientOriginalExtension();
                $path               = $request->file('attachment')->storeAs('expense', $fileNameToStore);
                $post['attachment'] = $path;
            }

            $income = Income::create($post);

            // Make entry in activity log
            ActivityLog::create(
                [
                    'user_id' => $usr->id,
                    'project_id' => $project_id,
                    'log_type' => 'Create Income',
                    'remark' => json_encode(['title' => $income->name]),
                ]
            );

            return redirect()->back()->with('success', __('Income added successfully.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function edit($project_id, $income_id)
    {
        if(\Auth::user()->can('edit expense'))
        {
            $project = Project::find($project_id);
            $income = Income::find($income_id);

            return view('incomes.edit', compact('project', 'income'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function update(Request $request, $project_id, $income_id)
    {
        if(\Auth::user()->can('edit expense'))
        {
            $validator = Validator::make(
                $request->all(), [
                                'name' => 'required|max:120',
                                'amount' => 'required|numeric|min:0',
                            ]
            );

            if($validator->fails())
            {
                return redirect()->back()->with('error', Utility::errorFormat($validator->getMessageBag()));
            }

            $income = Income::find($income_id);
            $income->name = $request->name;
            $income->date = date("Y-m-d H:i:s", strtotime($request->date));
            $income->amount =$request->amount;
            $income->task_id = $request->task_id;
            $income->description = $request->description;

            if($request->hasFile('attachment'))
            {
                Utility::checkFileExistsnDelete([$income->attachment]);

                $fileNameToStore    = time() . '.' . $request->attachment->extension();
                $path =  $request->file('attachment')->storeAs('income', $fileNameToStore);
                $income->attachment = $path;
            }

            $income->save();

            return redirect()->back()->with('success', __('Income Updated successfully.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function destroy($income_id)
    {
        if(\Auth::user()->can('delete expense'))
        {
            $income = Income::find($income_id);

            $invoice = $income->invoice;
            if(count($invoice)>0)
              $invoice[0]->delete();

            Utility::checkFileExistsnDelete([$income->attachment]);
            $income->delete();

            return redirect()->back()->with('success', __('Income Deleted successfully.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function export($project_id)
    {
        
        $name = 'incomes_'. $project_id . '_' . date('Y-m-d i:h:s');
        $data = Excel::download(new IncomesExport($project_id), $name . '.xlsx'); ob_end_clean();

        return $data;
    }


}
