<?php

namespace App\Http\Controllers;

use App\Models\AttendanceEmployee;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Employee;
use App\Models\IpRestrict;
use App\Models\User;
use App\Models\WorkTaskAttendance;
use App\Models\Utility;
use App\Models\Breaks;
use App\Models\Postimage;
use App\Http\Controllers\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class AttendanceEmployeeController extends Controller
{
    public function index(Request $request)
    {

        if(\Auth::user()->can('manage attendance'))
        {
            $branch = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $branch->prepend('All', '');

            $department = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $department->prepend('All', '');

            $breaks = Breaks::where('employee_id', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $breaks->prepend('All', '');

            if(\Auth::user()->type != 'client' && \Auth::user()->type != 'company' && !\Auth::user()->can('show all users wta'))
            {
                $emp = !empty(\Auth::user()->employee) ? \Auth::user()->employee->id : 0;

                $attendanceEmployee = AttendanceEmployee::where('employee_id', $emp);

                if($request->type == 'monthly' && !empty($request->month))
                {
                    $month = date('m', strtotime($request->month));
                    $year  = date('Y', strtotime($request->month));

                    $start_date = date($year . '-' . $month . '-01');
                    $end_date   = date($year . '-' . $month . '-t');

                    $attendanceEmployee->whereBetween(
                        'date', [
                                  $start_date,
                                  $end_date,
                              ]
                    );
                }
                elseif($request->type == 'daily' && !empty($request->date))
                {
                    $attendanceEmployee->where('date', $request->date);
                }
                else
                {
                    $month      = date('m');
                    $year       = date('Y');
                    $start_date = date($year . '-' . $month . '-01');
                    $end_date   = date($year . '-' . $month . '-t');

                    $attendanceEmployee->whereBetween(
                        'date', [
                                  $start_date,
                                  $end_date,
                              ]
                    );
                }
                $attendanceEmployee = $attendanceEmployee->get();

            }
            else
            {
                $employee = Employee::select('id')->where('created_by', \Auth::user()->creatorId());

                if(!empty($request->branch))
                {
                    $employee->where('branch_id', $request->branch);
                }

                if(!empty($request->department))
                {
                    $employee->where('department_id', $request->department);
                }

                $employee = $employee->get()->pluck('id');

                $attendanceEmployee = AttendanceEmployee::whereIn('employee_id', $employee);


                if($request->type == 'monthly' && !empty($request->month))
                {
                    $month = date('m', strtotime($request->month));
                    $year  = date('Y', strtotime($request->month));

                    $start_date = date($year . '-' . $month . '-01');
                    $end_date   = date($year . '-' . $month . '-t');

                    $attendanceEmployee->whereBetween(
                        'date', [
                            $start_date,
                            $end_date,
                            ]
                        );
                    }
                    elseif($request->type == 'daily' && !empty($request->date))
                    {
                        $attendanceEmployee->where('date', $request->date);
                    }
                    else
                    {
                        $month      = date('m');
                        $year       = date('Y');
                        $start_date = date($year . '-' . $month . '-01');
                        $end_date   = date($year . '-' . $month . '-t');

                        $attendanceEmployee->whereBetween(
                            'date', [
                                $start_date,
                                $end_date,
                                ]
                            );
                        }


                        $attendanceEmployee = $attendanceEmployee->get();


                    }
                    $emp = !empty(\Auth::user()->employee) ? \Auth::user()->employee->id : 0;
                    $idx = 0;
                    foreach ($attendanceEmployee as $employee) {
                      $breaks = Breaks::where('employee_id', $employee->employee_id)
                                      ->whereBetween('date_break', [
                                          $start_date,
                                          $end_date,
                                          ]);

                      $breaks = $breaks->get();

                      $attendanceEmployee[$idx]->breaks = $breaks;
                      $idx++;
                    }



            return view('attendance.index', compact('attendanceEmployee', 'branch', 'department', 'breaks'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function create()
    {
        if(\Auth::user()->can('create attendance'))
        {
            $employees = User::where('created_by', '=', Auth::user()->creatorId())->where('type', '=', "employee")->get()->pluck('name', 'id');

            return view('attendance.create', compact('employees'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }


    }

    public function store(Request $request)
    {
        if(\Auth::user()->can('create attendance'))
        {
            $validator = \Validator::make(
                $request->all(), [
                                   'employee_id' => 'required',
                                   'date' => 'required',
                                   'clock_in' => 'required',
                                   'clock_out' => 'required',
                               ]
            );
            if($validator->fails())
            {
                $messages = $validator->getMessageBag();

                return redirect()->back()->with('error', $messages->first());
            }

            $startTime  = Utility::getValByName('company_start_time');
            $endTime    = Utility::getValByName('company_end_time');
            $attendance = AttendanceEmployee::where('employee_id', '=', $request->employee_id)->where('date', '=', $request->date)->where('clock_out', '=', '00:00:00')->get()->toArray();
            if($attendance)
            {
                return redirect()->route('attendanceemployee.index')->with('error', __('Employee Attendance Already Created.'));
            }
            else
            {
                $date = date("Y-m-d");

                $totalLateSeconds = strtotime($request->clock_in) - strtotime($date . $startTime);

                $hours = floor($totalLateSeconds / 3600);
                $mins  = floor($totalLateSeconds / 60 % 60);
                $secs  = floor($totalLateSeconds % 60);
                $late  = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

                //early Leaving
                $totalEarlyLeavingSeconds = strtotime($date . $endTime) - strtotime($request->clock_out);
                $hours                    = floor($totalEarlyLeavingSeconds / 3600);
                $mins                     = floor($totalEarlyLeavingSeconds / 60 % 60);
                $secs                     = floor($totalEarlyLeavingSeconds % 60);
                $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);


                if(strtotime($request->clock_out) > strtotime($date . $endTime))
                {
                    //Overtime
                    $totalOvertimeSeconds = strtotime($request->clock_out) - strtotime($date . $endTime);
                    $hours                = floor($totalOvertimeSeconds / 3600);
                    $mins                 = floor($totalOvertimeSeconds / 60 % 60);
                    $secs                 = floor($totalOvertimeSeconds % 60);
                    $overtime             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                }
                else
                {
                    $overtime = '00:00:00';
                }

                $employeeAttendance                = new AttendanceEmployee();
                $employeeAttendance->employee_id   = $request->employee_id;
                $employeeAttendance->date          = $request->date;
                $employeeAttendance->status        = 'Present';
                $employeeAttendance->clock_in      = $request->clock_in . ':00';
                $employeeAttendance->clock_out     = $request->clock_out . ':00';
                $employeeAttendance->late          = $late;
                $employeeAttendance->early_leaving = $earlyLeaving;
                $employeeAttendance->overtime      = $overtime;
                $employeeAttendance->total_rest    = '00:00:00';
                $employeeAttendance->created_by    = \Auth::user()->creatorId();
                $employeeAttendance->save();

                return redirect()->route('attendanceemployee.index')->with('success', __('Employee attendance successfully created.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function edit($id)
    {
        if(\Auth::user()->can('edit attendance'))
        {
            $attendanceEmployee = AttendanceEmployee::where('id', $id)->first();
            $employees          = Employee::where('created_by', '=', \Auth::user()->creatorId())->get()->pluck('name', 'id');

            return view('attendance.edit', compact('attendanceEmployee', 'employees'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }



    public function update(Request $request, $id)
    {
        $employeeId      = !empty(\Auth::user()->employee) ? \Auth::user()->employee->id : 0;
       /* $todayAttendance = AttendanceEmployee::where('employee_id', '=', $employeeId)->where('date', date('Y-m-d'))->first();
        if(!empty($todayAttendance) && $todayAttendance->clock_out == '00:00:00')
        {*/

          $attendanceEmployee = null;
          $break = null;
          $tasks = [];
          $all_requests = $request->all();



          $validator = \Validator::make(
            $request->all(), [
                 'absence' => 'image|file|max:2048'
            ]
        );
        // if($validator->fails())
        //     {
        //         $messages = $validator->getMessageBag();

        //         return redirect()->back()->with('error', $messages->first());
        //     }




          foreach ($all_requests as $key => $value) {
              $comps = explode('-', $key);
              if(count($comps)==2 && $comps[0] == 'task' && !empty($value)){

                  $task_id = $comps[1];
                  $type='Project';

                  if($request->has('type-'.$task_id)){
                      $type=$request->input('type-'.$task_id);
                    }

                    $description  = strval($request->input('description-'.$task_id));
                    $hours = intval($value);


                    $tasks[$task_id] = [$hours, $description, $type];
                }
            }





            $startTime = Utility::getValByName('company_start_time');
            $endTime   = Utility::getValByName('company_end_time');
            if($employeeId!=0)
            {

                $date_break = $request->input('date-break');
                $time_break_1 = $request->input('time-break-1');
                $time_break_2 = $request->input('time-break-2');
                $time_break_message = $request->input('reason-for-the-absence');
                $date = date("Y-m-d");
                $time = date("H:i:s");

                if($date_break !=null ) {
                    $date = explode("-", $date);
                    $date_break = explode("-", $date_break);
                    if(intval($date[2]) > intval($date_break[2]) + 5 || intval($date[2]) < intval($date_break[2]) - 5) {
                        $date_break[2] = $date[2];
                    }
                    $date = implode("-", $date);
                    $date_break = implode("-", $date_break);
                }

                //time-breaker

                $break = new Breaks;

                if($time_break_1 != null && $time_break_2 != null) {
                    $time_break_1 = explode(":", $time_break_1);
                    $time_break_2 = explode(":", $time_break_2);
                    $time_break_2[0] = intval($time_break_2[0]);
                    $time_break_2[1] = intval($time_break_2[1]);
                    $time_break_1[0] = intval($time_break_1[0]);
                    $time_break_1[1] = intval($time_break_1[1]);

                    /*
                    $calculus_time_break = array (
                        $time_break_2[0] - $time_break_1[0],
                        $time_break_2[1] - $time_break_1[1]
                    );

                    if($calculus_time_break[1] < 0) {
                        $calculus_time_break[1] = 60 - (-$calculus_time_break[1]);
                        $calculus_time_break[0] = $calculus_time_break[0] - 1;
                    }

                    if($calculus_time_break[1] < 9){
                        $calculus_time_break[1] = "0".$calculus_time_break[1];
                    }*/

                    $now = \Carbon\Carbon::now();

                    $first_time = clone $now;
                    $first_time->hour = $time_break_1[0];
                    $first_time->minute = $time_break_1[1];

                    $second_time = clone $now;
                    $second_time->hour = $time_break_2[0];
                    $second_time->minute = $time_break_2[1];

                    $calculus_time_break[0] = intval($second_time->diffInMinutes($first_time)/60);
                    $calculus_time_break[1] = intval($second_time->diffInMinutes($first_time)%60);

                    $fileNameToStore = NULL;
                    if(!empty($request->absence) && $request->hasFile('absence'))
                    {
                        $filenameWithExt = $request->file('absence')->getClientOriginalName();
                        $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                        $extension       = $request->file('absence')->getClientOriginalExtension();
                        $fileNameToStore = time() . '.' . $extension;


                        $dir        = public_path('uploads/breaks/');
                        $image_path = $dir . $filenameWithExt;

                        if(!file_exists($dir))
                        {
                            mkdir($dir, 0777, true);
                        }

                        $path = $request->file('absence')->move($dir, $fileNameToStore);

                      }



                      $break->employee_id = $employeeId;
                      $break->time_break_message = $time_break_message;
                      $break->date_break = $date_break;
                      $break->total_break_hour = $calculus_time_break[0];
                      $break->total_break_minute = $calculus_time_break[1];
                      $break->start_break_hour = $time_break_1[0];
                      $break->start_break_minute = $time_break_1[1];
                      $break->end_break_hour = $time_break_2[0];
                      $break->end_break_minute = $time_break_2[1];
                      //$break->created_today_date = $date;
                      $break->document_title = $fileNameToStore;
                      $break->save();
                      $break = Breaks::where('employee_id', $break->employee_id )->first();
                    }




                //early Leaving
                $totalEarlyLeavingSeconds = strtotime($date . $endTime) - time();
                $hours                    = floor($totalEarlyLeavingSeconds / 3600);
                $mins                     = floor($totalEarlyLeavingSeconds / 60 % 60);
                $secs                     = floor($totalEarlyLeavingSeconds % 60);
                if($time_break_1 != null && $time_break_2 !=null) {
                    $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours - $calculus_time_break[0], $mins - $calculus_time_break[1], $secs);
                }else {
                    $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                }
                $earlyLeaving = sprintf('%02d:%02d:%02d', 0, 0, 0);

                if(time() > strtotime($date . $endTime))
                {
                    //Overtime
                    $totalOvertimeSeconds = time() - strtotime($date . $endTime);
                    $hours                = floor($totalOvertimeSeconds / 3600);
                    $mins                 = floor($totalOvertimeSeconds / 60 % 60);
                    $secs                 = floor($totalOvertimeSeconds % 60);
                    if($time_break_1 != null && $time_break_2 !=null) {
                    $overtime             = sprintf('%02d:%02d:%02d', $hours - $calculus_time_break[0], $mins - $calculus_time_break[1], $secs);
                    }else {
                        $overtime             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                    }
                }
                else
                {
                    $overtime = '00:00:00';
                }

                $attendanceEmployee                = AttendanceEmployee::find($id);
                $attendanceEmployee->clock_out     = $time;
                $attendanceEmployee->early_leaving = $earlyLeaving;
                $attendanceEmployee->overtime      = $overtime;
                $attendanceEmployee->save();

            }
            else
            {
                $date = date("Y-m-d");
                //late
                $totalLateSeconds = strtotime($request->clock_in) - strtotime($date . $startTime);

                $hours = floor($totalLateSeconds / 3600);
                $mins  = floor($totalLateSeconds / 60 % 60);
                $secs  = floor($totalLateSeconds % 60);
                $late  = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);



                //early Leaving
                $totalEarlyLeavingSeconds = strtotime($date . $endTime) - strtotime($request->clock_out);
                $hours                    = floor($totalEarlyLeavingSeconds / 3600);
                $mins                     = floor($totalEarlyLeavingSeconds / 60 % 60);
                $secs                     = floor($totalEarlyLeavingSeconds % 60);
                if($time_break_1 != null && $time_break_2 !=null) {
                $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours - $calculus_time_break[0], $mins - $calculus_time_break[1], $secs);
                }else {
                    $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                }


                if(strtotime($request->clock_out) > strtotime($date . $endTime))
                {
                    //Overtime
                    $totalOvertimeSeconds = strtotime($request->clock_out) - strtotime($date . $endTime);
                    $hours                = floor($totalOvertimeSeconds / 3600);
                    $mins                 = floor($totalOvertimeSeconds / 60 % 60);
                    $secs                 = floor($totalOvertimeSeconds % 60);
                    if($time_break_1 != null && $time_break_2 !=null) {
                    $overtime             = sprintf('%02d:%02d:%02d', $hours - $calculus_time_break[0], $mins - $calculus_time_break[1], $secs);
                    }else {
                        $overtime             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                    }
                }
                else
                {
                    $overtime = '00:00:00';
                }

                $attendanceEmployee                = AttendanceEmployee::find($id);
                $attendanceEmployee->employee_id   = $request->employee_id;
                $attendanceEmployee->date          = $request->date;
                $attendanceEmployee->clock_in      = $request->clock_in;
                $attendanceEmployee->clock_out     = $request->clock_out;
                $attendanceEmployee->late          = $late;
                $attendanceEmployee->early_leaving = $earlyLeaving;
                $attendanceEmployee->overtime      = $overtime;
                $attendanceEmployee->total_rest    = '00:00:00';

                $attendanceEmployee->save();

            }


            foreach ($tasks as $task_id => $value) {
              $wta = WorkTaskAttendance::where('attendance_employee_id', $attendanceEmployee->id)->
                                         where('task_id', $task_id)->first();

              if($wta){
                $wta->hours = $value[0];
              }else{
                $wta = new WorkTaskAttendance;
                $wta->attendance_employee_id = $attendanceEmployee->id;
                $wta->employee_id = $attendanceEmployee->employee_id;
                $wta->task_id = $task_id;
                $wta->hours = $value[0];
                $wta->description = $value[1];
                $wta->type=$value[2];
              }
              $wta->save();
            }

            if(Auth::user()->type == 'employee'){
              return redirect()->route('home')->with('success', __('Employee successfully clock Out.'));
            }else{
              return redirect()->route('attendanceemployee.index')->with('success', __('Employee attendance successfully updated.'));
            }
       /* }
        else
        {
            return redirect()->back()->with('error', __('Employee are not allow multiple time clock in & clock for every day.'));
        }*/
    }

    public function destroy($id)
    {
        if(\Auth::user()->can('delete attendance'))
        {
            $attendance = AttendanceEmployee::where('id', $id)->first();

            $attendance->delete();

            return redirect()->route('attendanceemployee.index')->with('success', __('Attendance successfully deleted.'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function attendance(Request $request)
    {
        $settings = Utility::settings();

        $employeeId      = !empty(\Auth::user()->employee) ? \Auth::user()->employee->id : 0;
        $todayAttendance = AttendanceEmployee::where('employee_id', '=', $employeeId)->where('date', date('Y-m-d'))->first();
        if(empty($todayAttendance))
        {

            $startTime = Utility::getValByName('company_start_time');
            $endTime   = Utility::getValByName('company_end_time');

            $attendance = AttendanceEmployee::orderBy('id', 'desc')->where('employee_id', '=', $employeeId)->where('clock_out', '=', '00:00:00')->first();

            if($attendance != null)
            {
                $attendance            = AttendanceEmployee::find($attendance->id);
                $attendance->clock_out = $endTime;
                $attendance->save();
            }

            $date = date("Y-m-d");
            $time = date("H:i:s");

            //late
            $totalLateSeconds = time() - strtotime($date . $startTime);
            $hours            = floor($totalLateSeconds / 3600);
            $mins             = floor($totalLateSeconds / 60 % 60);
            $secs             = floor($totalLateSeconds % 60);
            $late             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
            $late             = sprintf('%02d:%02d:%02d', 0, 0, 0);

            $checkDb = AttendanceEmployee::where('employee_id', '=', \Auth::user()->id)->get()->toArray();


            if(empty($checkDb))
            {
                $employeeAttendance                = new AttendanceEmployee();
                $employeeAttendance->employee_id   = $employeeId;
                $employeeAttendance->date          = $date;
                $employeeAttendance->status        = 'Present';
                $employeeAttendance->clock_in      = $time;
                $employeeAttendance->clock_out     = '00:00:00';
                $employeeAttendance->late          = $late;
                $employeeAttendance->early_leaving = '00:00:00';
                $employeeAttendance->overtime      = '00:00:00';
                $employeeAttendance->total_rest    = '00:00:00';
                $employeeAttendance->created_by    = \Auth::user()->id;

                $employeeAttendance->save();

                return redirect()->route('dashboard')->with('success', __('Employee Successfully Clock In.'));
            }
            foreach($checkDb as $check)
            {


                $employeeAttendance                = new AttendanceEmployee();
                $employeeAttendance->employee_id   = $employeeId;
                $employeeAttendance->date          = $date;
                $employeeAttendance->status        = 'Present';
                $employeeAttendance->clock_in      = $time;
                $employeeAttendance->clock_out     = '00:00:00';
                $employeeAttendance->late          = $late;
                $employeeAttendance->early_leaving = '00:00:00';
                $employeeAttendance->overtime      = '00:00:00';
                $employeeAttendance->total_rest    = '00:00:00';
                $employeeAttendance->created_by    = \Auth::user()->id;

                $employeeAttendance->save();

                return redirect()->route('dashboard')->with('success', __('Employee Successfully Clock In.'));

            }
        }
        else
        {
            return redirect()->back()->with('error', __('Employee are not allow multiple time clock in & clock for every day.'));
        }
    }

    public function bulkAttendance(Request $request)
    {
        if(\Auth::user()->can('create attendance'))
        {

            $branch = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $branch->prepend('Select Branch', '');

            $department = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $department->prepend('Select Department', '');

            $employees = [];
            if(!empty($request->branch) && !empty($request->department))
            {
                $employees = Employee::where('created_by', \Auth::user()->creatorId())->where('branch_id', $request->branch)->where('department_id', $request->department)->get();


            }


            return view('attendance.bulk', compact('employees', 'branch', 'department'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function bulkAttendanceData(Request $request)
    {

        if(\Auth::user()->can('create attendance'))
        {
            if(!empty($request->branch) && !empty($request->department))
            {
                $startTime = Utility::getValByName('company_start_time');
                $endTime   = Utility::getValByName('company_end_time');
                $date      = $request->date;

                $employees = $request->employee_id;
                $atte      = [];
                foreach($employees as $employee)
                {
                    $present = 'present-' . $employee;
                    $in      = 'in-' . $employee;
                    $out     = 'out-' . $employee;
                    $atte[]  = $present;
                    if($request->$present == 'on')
                    {

                        $in  = date("H:i:s", strtotime($request->$in));
                        $out = date("H:i:s", strtotime($request->$out));

                        $totalLateSeconds = strtotime($in) - strtotime($startTime);

                        $hours = floor($totalLateSeconds / 3600);
                        $mins  = floor($totalLateSeconds / 60 % 60);
                        $secs  = floor($totalLateSeconds % 60);
                        $late  = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

                        //early Leaving
                        $totalEarlyLeavingSeconds = strtotime($endTime) - strtotime($out);
                        $hours                    = floor($totalEarlyLeavingSeconds / 3600);
                        $mins                     = floor($totalEarlyLeavingSeconds / 60 % 60);
                        $secs                     = floor($totalEarlyLeavingSeconds % 60);
                        $earlyLeaving             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);


                        if(strtotime($out) > strtotime($endTime))
                        {
                            //Overtime
                            $totalOvertimeSeconds = strtotime($out) - strtotime($endTime);
                            $hours                = floor($totalOvertimeSeconds / 3600);
                            $mins                 = floor($totalOvertimeSeconds / 60 % 60);
                            $secs                 = floor($totalOvertimeSeconds % 60);
                            $overtime             = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        }
                        else
                        {
                            $overtime = '00:00:00';
                        }


                        $attendance = AttendanceEmployee::where('employee_id', '=', $employee)->where('date', '=', $request->date)->first();

                        if(!empty($attendance))
                        {
                            $employeeAttendance = $attendance;
                        }
                        else
                        {
                            $employeeAttendance              = new AttendanceEmployee();
                            $employeeAttendance->employee_id = $employee;
                            $employeeAttendance->created_by  = \Auth::user()->creatorId();
                        }


                        $employeeAttendance->date          = $request->date;
                        $employeeAttendance->status        = 'Present';
                        $employeeAttendance->clock_in      = $in;
                        $employeeAttendance->clock_out     = $out;
                        $employeeAttendance->late          = $late;
                        $employeeAttendance->early_leaving = ($earlyLeaving > 0) ? $earlyLeaving : '00:00:00';
                        $employeeAttendance->overtime      = $overtime;
                        $employeeAttendance->total_rest    = '00:00:00';
                        $employeeAttendance->save();

                    }
                    else
                    {
                        $attendance = AttendanceEmployee::where('employee_id', '=', $employee)->where('date', '=', $request->date)->first();

                        if(!empty($attendance))
                        {
                            $employeeAttendance = $attendance;
                        }
                        else
                        {
                            $employeeAttendance              = new AttendanceEmployee();
                            $employeeAttendance->employee_id = $employee;
                            $employeeAttendance->created_by  = \Auth::user()->creatorId();
                        }

                        $employeeAttendance->status        = 'Leave';
                        $employeeAttendance->date          = $request->date;
                        $employeeAttendance->clock_in      = '00:00:00';
                        $employeeAttendance->clock_out     = '00:00:00';
                        $employeeAttendance->late          = '00:00:00';
                        $employeeAttendance->early_leaving = '00:00:00';
                        $employeeAttendance->overtime      = '00:00:00';
                        $employeeAttendance->total_rest    = '00:00:00';
                        $employeeAttendance->save();
                    }
                }

                return redirect()->back()->with('success', __('Employee attendance successfully created.'));
            }
            else
            {
                return redirect()->back()->with('error', __('Branch & department field required.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function is_canceled(Request $request){


        $employeeId      = !empty(\Auth::user()->employee) ? \Auth::user()->employee->id : 0;
        $cancel_break = Breaks::where('employee_id', $employeeId)->get();
        $cancel_break->is_canceled = intval($request->is_canceled);
        $cancel_break->id = $request->break_id;
        $cancel_break->employee_id = $request->employee_id;
        Breaks::where('id', $cancel_break->id)->update(['is_canceled'=> $cancel_break->is_canceled]);




        return redirect()->back()->with('success', __('Break successfully canceled.'));
    }

}
