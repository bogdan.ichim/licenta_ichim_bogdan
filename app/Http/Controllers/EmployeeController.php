<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Document;
use App\Models\Employee;
use App\Models\Milestone;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\NewWorkAttendance;
use App\Models\EmployeeDocument;
use App\Models\Mail\UserCreate;
use App\Models\Plan;
use App\Models\ProjectTask;
use App\Models\User;
use App\Models\Utility;
use App\Models\WorkTaskAttendance;
use File;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

//use Faker\Provider\File;

class EmployeeController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->can('manage employee'))
        {
            if(Auth::user()->type == 'employee')
            {
                $employees = Employee::where('user_id', '=', Auth::user()->id)->get();
            }
            else
            {
                $employees = Employee::where('created_by', \Auth::user()->creatorId())->get();
            }

            return view('employee.index', compact('employees'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function clocking(Request $request){
      $employees = Employee::where('created_by', '=', \Auth::user()->creatorId())->get();
      return view('employee.clocking', [
        'employees'=>$employees,
      ]);
    }


    public function get_max_worked_hours($id) {
        $currentDate = Carbon::today()->toDateString(); // get current date as Y-m-d string
        $hoursWorkedToday = NewWorkAttendance::where('employee_id', '=', $id)
        ->whereDate(DB::raw('DATE(work_date)'), $currentDate)
        ->sum('hours');

        return response()->json(['hours_worked_today' => $hoursWorkedToday]);
        
    }

    public function clocking_sorting_hours ($id) {
        $sorting_hours = ProjectGroup::where('id_number', '=', $id)
        ->select('billable', 'vacation', 'unpaid_leave', 'sick_leave', 'non_billable')
        ->get();
        
        return response()->json(['sorted_hours' => $sorting_hours]);
    }

    public function clocking_submit(Request $request){
      $project_group = $request->input('project-group');

      $sorting_hours = $this->clocking_sorting_hours($project_group);  
      $sorting_hours_data = $sorting_hours->getData();

      $billable = $sorting_hours_data->sorted_hours[0]->billable;
      $nonBillable = $sorting_hours_data->sorted_hours[0]->non_billable;
      $vacation = $sorting_hours_data->sorted_hours[0]->vacation;
      $unpaid_leave = $sorting_hours_data->sorted_hours[0]->unpaid_leave;
      $sick_leave = $sorting_hours_data->sorted_hours[0]->sick_leave;
      $employee_id = $request->input('employee-id');

      $max_worked_hours = $this->get_max_worked_hours($employee_id);
      $max_worked_hours_data = $max_worked_hours->getData();

      $project_name = $request->input('project-name');
      $hours = $request->input('hours');
      if ($hours === '' || $hours < 0) {
          $hours = 0;
      }else if ($hours === '' || $hours > 8) {
          $hours = 8;
      } 
      
      $work_date = Carbon::createFromFormat('Y-m-d', $request->input('work-date'));
      $project_milestone = $request->input('project-milestone');
      $project_phase = $request->input('project-phase');
      $task_description = $request->input('task-description');
      $overtime_balance = $request->input('overtime-balance');

      $nwta = new NewWorkAttendance;
      $nwta->employee_id=$employee_id;
      $nwta->project_group=$project_group;
      $nwta->project_name=$project_name;

      if($billable == 1 && $nonBillable == 0 && $vacation == 0 && $unpaid_leave == 0 && $sick_leave == 0) {
          if((intVal($max_worked_hours_data->hours_worked_today) + intVal($hours)) > 40) {
              $nwta->non_billable_hours=$hours;
              $nwta->hours=0;
          } else {
              $nwta->hours=$hours;
              $nwta->non_billable_hours = 0;
          }
      }else if($billable == 0 && $nonBillable == 1 || $vacation == 1 || $unpaid_leave == 1 || $sick_leave == 1) {
          $nwta->non_billable_hours = $hours;
          $nwta->hours = 0;
      }
      
      $nwta->milestone=$project_milestone;
      $nwta->phase = $project_phase;
      $nwta->task_description=$task_description;
      $nwta->work_date = $work_date;
      $nwta->overtime_balance = $overtime_balance;
      $nwta->save();

      return back()->with('success', 'Submitted');
    }

    public function clocking_submit_edit(Request $request) {
        $project_id = $request->input('project-code-editable-fictional');
        $hours = $request->input('workedHours-editable');
        $work_date = date('Y-m-d', strtotime($request->input('work-date-editable')));
        $project_milestone = $request->input('project-milestone-editable');
        $project_phase = $request->input('project-phase-editable');
        $task_description = $request->input('task-description-editable');

        $nwta = NewWorkAttendance::where('id', $project_id)->first();
        if ($nwta) {
            $nwta->hours = $hours;
            $nwta->work_date = $work_date;
            $nwta->milestone = $project_milestone;
            $nwta->phase = $project_phase;
            $nwta->task_description = $task_description;
            $nwta->save();

            return redirect()->back()->with('success', __('The attendance has been updated'));
        } else {
            return redirect()->back()->with('error', __('The attendance has been not updated'));

        }
    }
    public function clocking_projects(Request $request){
      $group_nr = $request->group_id;

      $project = Project::where('created_by', '=', \Auth::user()->creatorId())
                               ->where('project_id_group', $group_nr)
                               ->first();

      return response()->json(['project' => $project]);
    }

    public function verify_clocking_hours($employee_id, $selected_date)
    {
        $clockingHours = NewWorkAttendance::where('employee_id', '=', $employee_id)
            ->whereDate('work_date', '=', $selected_date)
            ->groupBy(DB::raw('DATE(work_date)'))
            ->select(DB::raw('DATE(work_date) as date'), DB::raw('SUM(hours) as total_hours'))
            ->get();
    
        return $clockingHours;
    }
    public function get_new_attendance_employee_id(Request $request){
      $employee_id = $request->employee_id;

      $attendance = NewWorkAttendance::where('new_work_attendances.employee_id', $employee_id)
      ->leftJoin('project_groups', 'project_groups.id_number', '=', 'new_work_attendances.project_group')
      ->get(['new_work_attendances.*', 'project_groups.billable', 'project_groups.non_billable', 'project_groups.vacation', 'project_groups.unpaid_leave', 'project_groups.sick_leave']);
      $employee = Employee::where('employee_id', $employee_id)->first();


      return response()->json(['attendance' => $attendance,
                               'employee' => $employee]);
    }

    public function get_new_attendace_employee_id_project_code($attendace_id) {
        $attendace = NewWorkAttendance::where('id', '=', $attendace_id)->first();
        $project_id = Project::where('project_id_group', '=', $attendace['project_group'])->pluck('id');
        $milestones = Milestone::where('project_id', $project_id[0])->pluck('title');
        $milestone_id= Milestone::where('project_id', $project_id[0])->pluck('id');
        $project_tasks = ProjectTask::where('project_id', $project_id[0])
                                    ->where('milestone_id', $milestone_id)->pluck('name');
        return response()->json([
            'attendace' => $attendace,
            'milestones_available' => $milestones,
            'project_tasks_available' => $project_tasks
    ]);
    }
    public function approve_attendance_employee(Request $request, $id, $work_date) {
        $attendance = NewWorkAttendance::find($id);
        $approvedWorkDate = Carbon::createFromFormat('d.m.Y', $work_date)->format('Y-m-d');
        if ($attendance) {
            $attendance->validated_by_admin = 1;
            $attendance->work_date = $approvedWorkDate;
            $attendance->save(); 
    
    
            return redirect('/clocking?selected_id=' . $request->selected_id . '&start_date=' . $request->start_date . '&end_date=' . $request->end_date . '&week_selected=' . $request->week_selected);
        } else {
            return redirect()->back()->with('error', __('The request was not found'));
        }
    }
    public function delete_attendace_employee(Request $request, $id) {
        $attendance = NewWorkAttendance::find($id);

        if ($attendance) {
            $attendance->delete();

            if ($request->selected_id != null && $request->start_date != null && $request->end_date != null && $request->week_selected != null) {
                return redirect('/clocking?selected_id=' . $request->selected_id . '&start_date=' . $request->start_date . '&end_date=' . $request->end_date . '&week_selected=' . $request->week_selected);
            }else {
                return redirect('/clocking');
            }

        } else {
            return redirect()->back()->with('error',  __('The attendance was not found'));
        }
}
    public function reverse_approve_attendance_employee(Request $request, $id, $work_date) {
        $attendance = NewWorkAttendance::find($id);
        $approvedWorkDate = Carbon::createFromFormat('d.m.Y', $work_date)->format('Y-m-d');
        if ($attendance) {
            $attendance->validated_by_admin = 0;
            $attendance->work_date = $approvedWorkDate;
            $attendance->save();

            return redirect('/clocking?selected_id=' . $request->selected_id . '&start_date=' . $request->start_date . '&end_date=' . $request->end_date . '&week_selected=' . $request->week_selected);
        } else {
            return redirect()->back()->with('error',  __('The request was not found'));
        }
}

public function get_real_employee_id($employee_id) {
    $real_employee_id = Employee::where('id', '=', $employee_id)->pluck('employee_id')->first();
    return $real_employee_id;
}
    public function create()
    {
        if(\Auth::user()->can('create employee'))
        {
            $company_settings = Utility::settings();
            $documents        = Document::where('created_by', \Auth::user()->creatorId())->get();
            $branches         = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $departments      = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $designations     = Designation::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $employees        = User::where('created_by', \Auth::user()->creatorId())->get();
            $employeesId      = \Auth::user()->employeeIdFormat($this->employeeNumber());

            return view('employee.create', compact('employees', 'employeesId', 'departments', 'designations', 'documents', 'branches', 'company_settings'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function store(Request $request)
    {
        if(\Auth::user()->can('create employee'))
        {
            $validator = \Validator::make(
                $request->all(), [
                                   'name' => 'required',
                                   'dob' => 'required',
                                   // 'gender' => 'required',
                                   'phone' => 'required',
                                   'address' => 'required',
                                   'email' => 'required|unique:users',
                                   'password' => 'required',
                                   'department_id' => 'required',
                                   'designation_id' => 'required',
                                   'document.*' => 'mimes:jpeg,png,jpg,gif,svg,pdf,doc,zip|max:20480',
                               ]
            );
            if($validator->fails())
            {
                $messages = $validator->getMessageBag();

                return redirect()->back()->withInput()->with('error', $messages->first());
            }

            $objUser        = User::find(\Auth::user()->creatorId());
            $total_employee = $objUser->countEmployees();
            $plan           = Plan::find($objUser->plan);

            if($total_employee < $plan->max_employees || $plan->max_employees == -1)
            {
                $user = User::create(
                    [
                        'name' => $request['name'],
                        'email' => $request['email'],
                        // 'gender'=>$request['gender'],
                        'password' => Hash::make($request['password']),
                        'type' => 'employee',
                        'lang' => 'en',
                        'created_by' => \Auth::user()->creatorId(),
                    ]
                );
                $user->save();
                $user->assignRole('Employee');
            }
            else
            {
                return redirect()->back()->with('error', __('Your employee limit is over, Please upgrade plan.'));
            }


            if(!empty($request->document) && !is_null($request->document))
            {
                $document_implode = implode(',', array_keys($request->document));
            }
            else
            {
                $document_implode = null;
            }


            $employee = Employee::create(
                [
                    'user_id' => $user->id,
                    'name' => $request['name'],
                    'dob' => $request['dob'],
                    'gender' => $request['gender'],
                    'phone' => $request['phone'],
                    'address' => $request['address'],
                    'email' => $request['email'],
                    'password' => Hash::make($request['password']),
                    'employee_id' => $this->employeeNumber(),
                    'branch_id' => $request['branch_id'],
                    'department_id' => $request['department_id'],
                    'designation_id' => $request['designation_id'],
                    'company_doj' => $request['company_doj'],
                    'documents' => $document_implode,
                    'account_holder_name' => $request['account_holder_name'],
                    'account_number' => $request['account_number'],
                    'bank_name' => $request['bank_name'],
                    'bank_identifier_code' => $request['bank_identifier_code'],
                    'branch_location' => $request['branch_location'],
                    'tax_payer_id' => $request['tax_payer_id'],
                    'created_by' => \Auth::user()->creatorId(),
                ]
            );

            if($request->hasFile('document'))
            {
                foreach($request->document as $key => $document)
                {

                    $filenameWithExt = $request->file('document')[$key]->getClientOriginalName();
                    $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    $extension       = $request->file('document')[$key]->getClientOriginalExtension();
                    $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                    $dir             = storage_path('uploads/document/');
                    $image_path      = $dir . $filenameWithExt;

                    if(File::exists($image_path))
                    {
                        File::delete($image_path);
                    }

                    if(!file_exists($dir))
                    {
                        mkdir($dir, 0777, true);
                    }
                    $path              = $request->file('document')[$key]->storeAs('uploads/document/', $fileNameToStore);
                    $employee_document = EmployeeDocument::create(
                        [
                            'employee_id' => $employee['employee_id'],
                            'document_id' => $key,
                            'document_value' => $fileNameToStore,
                            'created_by' => \Auth::user()->creatorId(),
                        ]
                    );
                    $employee_document->save();

                }

            }

            $setings = Utility::settings();
            if($setings['employee_create'] == 1)
            {
                $user->type     = 'Employee';
                $user->password = $request['password'];
                try
                {
                    Mail::to($user->email)->send(new UserCreate($user));
                }
                catch(\Exception $e)
                {
                    $smtp_error = __('E-Mail has been not sent due to SMTP configuration');
                }

                return redirect()->route('employee.index')->with('success', __('Employee successfully created.') . (isset($smtp_error) ? $smtp_error : ''));

            }

            return redirect()->route('employee.index')->with('success', __('Employee  successfully created.'));

        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        if(\Auth::user()->can('edit employee'))
        {
            $documents    = Document::where('created_by', \Auth::user()->creatorId())->get();
            $branches     = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $departments  = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $designations = Designation::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $employee     = Employee::find($id);
            $employeesId  = \Auth::user()->employeeIdFormat($employee->employee_id);

            return view('employee.edit', compact('employee', 'employeesId', 'branches', 'departments', 'designations', 'documents'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function update(Request $request, $id)
    {

        if(\Auth::user()->can('edit employee'))
        {
            $validator = \Validator::make(
                $request->all(), [
                                   'name' => 'required',
                                   'dob' => 'required',
                                   'gender' => 'required',
                                   'phone' => 'required|numeric',
                                   'address' => 'required',
                                   'document.*' => 'mimes:jpeg,png,jpg,gif,svg,pdf,doc,zip|max:20480',
                               ]
            );
            if($validator->fails())
            {
                $messages = $validator->getMessageBag();

                return redirect()->back()->with('error', $messages->first());
            }

            $employee = Employee::findOrFail($id);

            if($request->document)
            {

                foreach($request->document as $key => $document)
                {
                    if(!empty($document))
                    {
                        $filenameWithExt = $request->file('document')[$key]->getClientOriginalName();
                        $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                        $extension       = $request->file('document')[$key]->getClientOriginalExtension();
                        $fileNameToStore = $filename . '_' . time() . '.' . $extension;

                        $dir        = storage_path('uploads/document/');
                        $image_path = $dir . $filenameWithExt;

                        if(File::exists($image_path))
                        {
                            File::delete($image_path);
                        }
                        if(!file_exists($dir))
                        {
                            mkdir($dir, 0777, true);
                        }
                        $path = $request->file('document')[$key]->storeAs('uploads/document/', $fileNameToStore);

                        $employee_document = EmployeeDocument::where('employee_id', $employee->employee_id)->where('document_id', $key)->first();

                        if(!empty($employee_document))
                        {
                            $employee_document->document_value = $fileNameToStore;
                            $employee_document->save();
                        }
                        else
                        {
                            $employee_document                 = new EmployeeDocument();
                            $employee_document->employee_id    = $employee->employee_id;
                            $employee_document->document_id    = $key;
                            $employee_document->document_value = $fileNameToStore;
                            $employee_document->save();
                        }

                    }
                }
            }
            $employee = Employee::findOrFail($id);
            $input    = $request->all();
            $employee->fill($input)->save();
            $employee = Employee::find($id);
            $user = User::where('id',$employee->user_id)->first();
            $user->name = $employee->name;
            $user->email = $employee->email;
            $user->save();



            $is_remote = boolval($request->is_remote=='true');
            $working_hours = intval($request->working_hours);

            $employee->is_remote     = $is_remote;
            $employee->working_hours = $working_hours;
            $employee->save();

            if($request->salary)
            {
                return redirect()->route('setsalary.index')->with('success', 'Employee successfully updated.');
            }

            if(\Auth::user()->type != 'employee')
            {
                return redirect()->route('employee.index')->with('success', 'Employee successfully updated.');
            }
            else
            {
                return redirect()->route('employee.show', \Illuminate\Support\Facades\Crypt::encrypt($employee->id))->with('success', 'Employee successfully updated.');
            }

        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function destroy($id)
    {

        if(Auth::user()->can('delete employee'))
        {
            $employee      = Employee::findOrFail($id);
            $user          = User::where('id', '=', $employee->user_id)->first();
            $emp_documents = EmployeeDocument::where('employee_id', $employee->employee_id)->get();
            $employee->delete();
            $user->delete();
            $dir = storage_path('uploads/document/');
            foreach($emp_documents as $emp_document)
            {
                $emp_document->delete();
                if(!empty($emp_document->document_value))
                {
                    unlink($dir . $emp_document->document_value);
                }

            }

            return redirect()->route('employee.index')->with('success', 'Employee successfully deleted.');
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }

    }

    public function show($id)
    {
        if(\Auth::user()->can('view employee'))
        {
            $empId        = Crypt::decrypt($id);
            $documents    = Document::where('created_by', \Auth::user()->creatorId())->get();
            $branches     = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $departments  = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $designations = Designation::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $employee     = Employee::find($empId);

            $employeesId  = \Auth::user()->employeeIdFormat($employee->employee_id);

            return view('employee.show', compact('employee', 'employeesId', 'branches', 'departments', 'designations', 'documents'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function json(Request $request)
    {
        $designations = Designation::where('department_id', $request->department_id)->get()->pluck('name', 'id')->toArray();

        return response()->json($designations);
    }

    function employeeNumber()
    {
        $latest = Employee::where('created_by', '=', \Auth::user()->creatorId())->latest()->first();
        if(!$latest)
        {
            return 1;
        }

        return $latest->employee_id + 1;
    }

    public function profile(Request $request)
    {
        if(\Auth::user()->can('manage employee profile'))
        {
            $employees = Employee::where('created_by', \Auth::user()->creatorId());
            if(!empty($request->branch))
            {
                $employees->where('branch_id', $request->branch);
            }
            if(!empty($request->department))
            {
                $employees->where('department_id', $request->department);
            }
            if(!empty($request->designation))
            {
                $employees->where('designation_id', $request->designation);
            }
            $employees = $employees->get();

            $brances = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $brances->prepend('All', '');

            $departments = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $departments->prepend('All', '');

            $designations = Designation::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $designations->prepend('All', '');

            return view('employee.profile', compact('employees', 'departments', 'designations', 'brances'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function activity(Request $request, $employee_id, $project_id=null){
      $employee_id = Crypt::decrypt($employee_id);

      if($project_id!=null){
        $project_id = Crypt::decrypt($project_id);
      }

      $employee = Employee::find($employee_id);

      $daily_activity = WorkTaskAttendance::where('work_task_attendances.created_at', '>=', Carbon::now()->subMonth())
                        ->where('work_task_attendances.employee_id', $employee->id)
                        ->join('project_tasks', 'work_task_attendances.task_id', '=', 'project_tasks.id')
                        ->join('projects', 'project_tasks.project_id', '=', 'projects.id');

      if($project_id != null){
        $daily_activity = $daily_activity->where('projects.id', $project_id);
      }

      $daily_activity = $daily_activity->groupBy('date', 'project_tasks.project_id')
                        ->orderBy('date', 'DESC')
                        ->get(array(
                            DB::raw('Date(work_task_attendances.created_at) as "date"'),
                            DB::raw('SUM(work_task_attendances.hours) as "hours_worked"'),
                            DB::raw('projects.project_name as "project_name"')
                        ));

      $monthly_activity = WorkTaskAttendance::where('work_task_attendances.created_at', '>=', Carbon::now()->subMonth())
                        ->where('work_task_attendances.employee_id', $employee->id)
                        ->join('project_tasks', 'work_task_attendances.task_id', '=', 'project_tasks.id')
                        ->join('projects', 'project_tasks.project_id', '=', 'projects.id');

      if($project_id != null){
        $monthly_activity = $monthly_activity->where('projects.id', $project_id);
      }

      $monthly_activity = $monthly_activity->groupBy('date', 'project_tasks.project_id')
                        ->orderBy('date', 'DESC')
                        ->get(array(
                            DB::raw("DATE_FORMAT(work_task_attendances.created_at, '%m-%Y') as date"),
                            DB::raw('SUM(work_task_attendances.hours) as "hours_worked"'),
                            DB::raw('projects.project_name as "project_name"')
                        ));

      $yearly_activity = WorkTaskAttendance::where('work_task_attendances.created_at', '>=', Carbon::now()->subMonth())
                        ->where('work_task_attendances.employee_id', $employee->id)
                        ->join('project_tasks', 'work_task_attendances.task_id', '=', 'project_tasks.id')
                        ->join('projects', 'project_tasks.project_id', '=', 'projects.id');

      if($project_id != null){
        $yearly_activity = $yearly_activity->where('projects.id', $project_id);
      }

      $yearly_activity = $yearly_activity->groupBy('date', 'project_tasks.project_id')
                        ->orderBy('date', 'DESC')
                        ->get(array(
                            DB::raw("DATE_FORMAT(work_task_attendances.created_at, '%Y') as date"),
                            DB::raw('SUM(work_task_attendances.hours) as "hours_worked"'),
                            DB::raw('projects.project_name as "project_name"')
                        ));

      return view('employee.activity', [
        'employee'=>$employee,
        'daily_activity'=>$daily_activity,
        'monthly_activity'=>$monthly_activity,
        'yearly_activity'=>$yearly_activity,
      ]);
    }

    public function profileShow($id)
    {
        if(\Auth::user()->can('show employee profile'))
        {
            $empId        = Crypt::decrypt($id);
            $documents    = Document::where('created_by', \Auth::user()->creatorId())->get();
            $branches     = Branch::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $departments  = Department::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $designations = Designation::where('created_by', \Auth::user()->creatorId())->get()->pluck('name', 'id');
            $employee     = Employee::find($empId);
            $employeesId  = \Auth::user()->employeeIdFormat($employee->employee_id);

            return view('employee.show', compact('employee', 'employeesId', 'branches', 'departments', 'designations', 'documents'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission denied.'));
        }
    }

    public function lastLogin()
    {
        $users = User::where('created_by', \Auth::user()->creatorId())->get();

        return view('employee.lastLogin', compact('users'));
    }

    public function employeeJson(Request $request)
    {
        $employees = Employee::where('branch_id', $request->branch)->get()->pluck('name', 'id')->toArray();

        return response()->json($employees);
    }

    public function export(Request $request)
    {
        $name = Employee::where('employee_id', "=", $request->id)->pluck('name')->first();
        $title = $name . '_clocking_' . date('Y-m-d i:h:s');
        $data = Excel::download(new EmployeeExport($request->id, $request->start_date, $request->end_date), $title . '.xlsx'); ob_end_clean();

        return $data;
    }

}
