<?php

namespace App\Http\Controllers;

use App\Models\SubcontractorDeal;
use App\Models\SubcontractorPermission;
use App\Models\Mail\UserCreate;
use App\Models\Contract;
use App\Models\CustomField;
use App\Models\Estimation;
use App\Models\Invoice;
use App\Models\Vender;
use App\Models\Plan;
use App\Models\User;
use App\Models\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;

class SubcontractorController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            [
                'auth',
                'XSS',
            ]
        );
    }

    public function index()
    {
        if(\Auth::user()->can('manage client'))
        {
            $user    = \Auth::user();
            $subcontractors = User::where('created_by', '=', $user->creatorId())->where('type', '=', 'subcontractor')->get();

            return view('subcontractors.index', compact('subcontractors'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function create(Request $request)
    {

        if(\Auth::user()->can('create client'))
        {
            if($request->ajax)
            {
                return view('subcontractors.createAjax');
            }
            else
            {
                $customFields = CustomField::where('module', '=', 'subcontractor')->get();

                return view('subcontractors.create', compact('customFields'));
            }
        }
        else
        {
            return response()->json(['error' => __('Permission Denied.')], 401);
        }
    }

    public function store(Request $request)
    {
                if(\Auth::user()->can('create client'))
                {
                    $user      = \Auth::user();
                    $validator = \Validator::make(
                        $request->all(), [
                                           'name' => 'required',
                                           'contact' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
                                           'email' => 'required|email|unique:customers',
                                       ]
                    );

                    if($validator->fails())
                    {
                        $messages = $validator->getMessageBag();
                        if($request->ajax)
                        {
                            return response()->json(['error' => $messages->first()], 401);
                        }
                        else
                        {
                            return redirect()->back()->with('error', $messages->first());
                        }
                    }
                    $objCustomer    = \Auth::user();
                    $creator        = User::find($objCustomer->creatorId());
                    $total_client = User::where('type','subcontractor')->count();
                    // dd($total_client);
                    $plan           = Plan::find($creator->plan);
                    if($total_client < $plan->max_clients || $plan->max_clients == -1)
                    {
                        $role = Role::findByName('subcontractor');

                        $same_email_user = User::where('email', $request->email)->get();
                        if(count($same_email_user)>0){
                          return redirect()->route('subcontractors.index')->with('success', 'User with the same email already exists. <br> <span class="text-danger"></span>');
                        }

                        $subcontractor = User::create(
                            [
                                'name' => $request->name,
                                'email' => $request->email,
                                'job_title' => $request->job_title,
                                'password' => Hash::make('12345678'),
                                'type' => 'subcontractor',
                                'lang' => Utility::getValByName('default_language'),
                                'created_by' => $user->creatorId(),
                            ]
                        );
                        $role_r = Role::findByName('subcontractor');
                        $subcontractor->assignRole($role_r);

                        $subcontractor->password = '12345678';
                        try
                        {
                            Mail::to($subcontractor->email)->send(new UserCreate($subcontractor));
                        }
                        catch(\Exception $e)
                        {

                            $smtp_error = __('E-Mail has been not sent due to SMTP configuration');
                        }

                        $vender = Vender::where('email', $request->email)->first();
                        if($vender){
                          $vender->subcontractor_id = $subcontractor->id;
                          $vender->save();
                        }else{
                          $vender                   = new Vender();
                          $vender->vender_id        = VenderController::venderNumberStatic();
                          $vender->name             = $request->name;
                          $vender->subcontractor_id = $subcontractor->id;
                          $vender->contact          = $request->contact;
                          $vender->email            = $request->email;
                          $vender->password         = Hash::make('12345678');
                          $vender->created_by       = \Auth::user()->creatorId();
                          $vender->billing_name     = $request->billing_name;
                          $vender->billing_country  = $request->billing_country;
                          $vender->billing_state    = $request->billing_state;
                          $vender->billing_city     = $request->billing_city;
                          $vender->billing_phone    = $request->billing_phone;
                          $vender->billing_zip      = $request->billing_zip;
                          $vender->billing_address  = $request->billing_address;
                          $vender->shipping_name    = $request->shipping_name;
                          $vender->shipping_country = $request->shipping_country;
                          $vender->shipping_state   = $request->shipping_state;
                          $vender->shipping_city    = $request->shipping_city;
                          $vender->shipping_phone   = $request->shipping_phone;
                          $vender->shipping_zip     = $request->shipping_zip;
                          $vender->shipping_address = $request->shipping_address;
                          $vender->lang             = !empty($default_language) ? $default_language->value : '';
                          $vender->save();
                        }
                        return redirect()->route('subcontractors.index')->with('success', __('Subcontractor successfully added.') . ((isset($smtp_error)) ? '<br> <span class="text-danger">' . $smtp_error . '</span>' : ''));

                    }

                       /* $subcontractor->assignRole($role);
                        $uArr = [
                            'email' => $subcontractor->email,
                            'password' => $request->password,
                        ];
                        // Send Email
                        $resp = Utility::sendEmailTemplate('New User', [$subcontractor->id => $subcontractor->email], $uArr);


                        if($request->customField)
                        {
                            CustomField::saveData($subcontractor, $request->customField);
                        }
                        if($request->ajax)
                        {
                            return response()->json(
                                [
                                    'success' => __('Subcontractor created Successfully!'),
                                    'record' => $subcontractor,
                                    'target' => '#client_id',
                                ], 200
                            );
                        }
                        else
                        {
                            return redirect()->back()->with('success', __('Subcontractor created Successfully!') . (($resp['is_success'] == false && !empty($resp['error'])) ? '<br> <span class="text-danger">' . $resp['error'] . '</span>' : ''));
                        }*/

                    else
                    {
                        return redirect()->back()->with('error', __('Your user limit is over, Please upgrade plan.'));
                    }

                }
                else
                {
                    if($request->ajax)
                    {
                        return response()->json(['error' => __('Permission Denied.')], 401);
                    }
                    else
                    {
                        return redirect()->back()->with('error', __('Permission Denied.'));
                    }
                }
    }

    public function show(User $subcontractor)
    {
        $usr = Auth::user();
        if(!empty($subcontractor) && $usr->id == $subcontractor->creatorId() && $subcontractor->id != $usr->id && $subcontractor->type == 'subcontractor')
        {
            // For Estimations
            $estimations = $subcontractor->clientEstimations()->orderByDesc('id')->get();
            $curr_month  = $subcontractor->clientEstimations()->whereMonth('issue_date', '=', date('m'))->get();
            $curr_week   = $subcontractor->clientEstimations()->whereBetween(
                'issue_date', [
                                \Carbon\Carbon::now()->startOfWeek(),
                                \Carbon\Carbon::now()->endOfWeek(),
                            ]
            )->get();
            $last_30days = $subcontractor->clientEstimations()->whereDate('issue_date', '>', \Carbon\Carbon::now()->subDays(30))->get();
            // Estimation Summary
            $cnt_estimation                = [];
            $cnt_estimation['total']       = Estimation::getEstimationSummary($estimations);
            $cnt_estimation['this_month']  = Estimation::getEstimationSummary($curr_month);
            $cnt_estimation['this_week']   = Estimation::getEstimationSummary($curr_week);
            $cnt_estimation['last_30days'] = Estimation::getEstimationSummary($last_30days);

            $cnt_estimation['cnt_total']       = $estimations->count();
            $cnt_estimation['cnt_this_month']  = $curr_month->count();
            $cnt_estimation['cnt_this_week']   = $curr_week->count();
            $cnt_estimation['cnt_last_30days'] = $last_30days->count();

            // For Contracts
            $contracts   = $subcontractor->clientContracts()->orderByDesc('id')->get();
            $curr_month  = $subcontractor->clientContracts()->whereMonth('start_date', '=', date('m'))->get();
            $curr_week   = $subcontractor->clientContracts()->whereBetween(
                'start_date', [
                                \Carbon\Carbon::now()->startOfWeek(),
                                \Carbon\Carbon::now()->endOfWeek(),
                            ]
            )->get();
            $last_30days = $subcontractor->clientContracts()->whereDate('start_date', '>', \Carbon\Carbon::now()->subDays(30))->get();

            // Contracts Summary
            $cnt_contract                = [];
            $cnt_contract['total']       = Contract::getContractSummary($contracts);
            $cnt_contract['this_month']  = Contract::getContractSummary($curr_month);
            $cnt_contract['this_week']   = Contract::getContractSummary($curr_week);
            $cnt_contract['last_30days'] = Contract::getContractSummary($last_30days);

            $cnt_contract['cnt_total']       = $contracts->count();
            $cnt_contract['cnt_this_month']  = $curr_month->count();
            $cnt_contract['cnt_this_week']   = $curr_week->count();
            $cnt_contract['cnt_last_30days'] = $last_30days->count();

            return view('subcontractors.show', compact('subcontractor', 'estimations', 'cnt_estimation', 'contracts', 'cnt_contract'));
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function edit(User $subcontractor)
    {
        if(\Auth::user()->can('edit client'))
        {
            $user = \Auth::user();
            if($subcontractor->created_by == $user->creatorId())
            {
                $subcontractor->customField = CustomField::getData($subcontractor, 'subcontractor');
                $customFields        = CustomField::where('module', '=', 'subcontractor')->get();

                return view('subcontractors.edit', compact('subcontractor', 'customFields'));
            }
            else
            {
                return response()->json(['error' => __('Invalid Subcontractor.')], 401);
            }
        }
        else
        {
            return response()->json(['error' => __('Permission Denied.')], 401);
        }
    }

    public function update(User $subcontractor, Request $request)
    {
        if(\Auth::user()->can('edit client'))
        {
            $user = \Auth::user();
            if($subcontractor->created_by == $user->creatorId())
            {
                $validation = [
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $subcontractor->id,
                ];

                $post         = [];
                $post['name'] = $request->name;
                if(!empty($request->password))
                {
                    $validation['password'] = 'required';
                    $post['password']       = Hash::make($request->password);
                }

                $validator = \Validator::make($request->all(), $validation);
                if($validator->fails())
                {
                    $messages = $validator->getMessageBag();

                    return redirect()->back()->with('error', $messages->first());
                }
                $post['email'] = $request->email;

                $subcontractor->update($post);

                CustomField::saveData($subcontractor, $request->customField);

                return redirect()->back()->with('success', __('Subcontractor Updated Successfully!'));
            }
            else
            {
                return redirect()->back()->with('error', __('Invalid Subcontractor.'));
            }
        }
        else
        {
            return redirect()->back()->with('error', __('Permission Denied.'));
        }
    }

    public function destroy(User $subcontractor)
    {
        $user = \Auth::user();
            if($subcontractor->created_by == $user->creatorId())
            {
                $estimation = Estimation::where('client_id', '=', $subcontractor->id)->first();
                if(empty($estimation))
                {

                  $vender = $subcontractor->vender;
                  if(count($vender)>0)
                    $vender[0]->delete();

                  /*  SubcontractorDeal::where('client_id', '=', $subcontractor->id)->delete();
                    SubcontractorPermission::where('client_id', '=', $subcontractor->id)->delete();*/
                    $subcontractor->delete();
                    return redirect()->back()->with('success', __('Subcontractor Deleted Successfully!'));
                }
                else
                {
                    return redirect()->back()->with('error', __('This client has assigned some estimation.'));
                }
            }
            else
            {
                return redirect()->back()->with('error', __('Invalid Subcontractor.'));
            }
        }


}
