<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeActivity extends Model
{
  public function user()
  {
      return $this->hasOne('App\Models\User', 'id', 'user_id');
  }

  public function userdetail()
  {
      return $this->hasOne('App\Models\UserDetail', 'user_id', 'user_id');
  }

  public function checklist()
  {
      return $this->hasOne('App\Models\TaskChecklist', 'id', 'checklist_id');
  }

  public function logIcon($type = '')
  {
      return 'fa-pen';
  }

}
