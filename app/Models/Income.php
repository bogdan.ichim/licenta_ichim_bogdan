<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable = [
        'name',
        'date',
        'description',
        'amount',
        'attachment',
        'project_id',
        'task_id',
        'created_by',
    ];

    // Get Expense based task
    public function tasks()
    {
      $task_ids = explode(',', $this->task_id);
      for ($i=0; $i < count($task_ids); $i++) {
        $task_ids[$i] = trim($task_ids[$i]);
      }
      return ProjectTask::select('project_tasks.*')->whereIn('id', $task_ids)->get();
    }

    public function task(){
      $this->tasks()[0];
    }

    // Get Expense based project
    public function project()
    {
        return $this->hasOne('App\Models\Project', 'id', 'project_id');
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice', 'income_id', 'id');
    }

}
