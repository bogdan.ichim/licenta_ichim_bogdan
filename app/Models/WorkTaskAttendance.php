<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkTaskAttendance extends Model
{
    use HasFactory;

    public function task()
    {
        return $this->hasOne('App\Models\ProjectTask', 'id', 'task_id');
    }

    public function deal(){
      return $this->hasOne('App\Models\Deal', 'id', 'task_id');
    }
}
