<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyInvoiceExplan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
          $table->integer('project_id')->unsigned();
          $table->integer('income_id')->unsigned();

          $table->date('paid_date')->nullable();

          $table->integer('category_id')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
          $table->dropColumn('project_id');
          $table->dropColumn('income_id');
          $table->dropColumn('paid_date');

          $table->integer('category_id')->change();

        });
    }
}
