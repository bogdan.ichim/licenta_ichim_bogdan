<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

class AddWtaViewPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $permission = new Permission;
      $permission->name = 'show all users wta';
      $permission->guard_name = 'web';
      $permission->save();
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $permission = Permission::where('name', 'show all users wta')
                               ->where('guard_name', 'web')
                               ->first();
      if($permission)
        $permission->delete();
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
