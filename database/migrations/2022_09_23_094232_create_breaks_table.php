<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breaks', function (Blueprint $table) {
            $table->id();
            $table->string("time_break_message");
            $table->string("document_title")->nullable();
            $table->string("employee_id");
            $table->string("date_break");
            $table->integer("total_break_hour");
            $table->integer("total_break_minute");
            $table->integer("start_break_hour");
            $table->integer("start_break_minute");
            $table->integer("end_break_hour");
            $table->integer("end_break_minute");
            $table->boolean('is_canceled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breaks');
    }
}
