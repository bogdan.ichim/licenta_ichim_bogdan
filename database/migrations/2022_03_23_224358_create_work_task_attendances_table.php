<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkTaskAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_task_attendances', function (Blueprint $table) {
            $table->id();
            $table->integer('attendance_employee_id')->unsigned();
            $table->integer('employee_id')->unsigned()->default(0);
            $table->integer('task_id')->unsigned();
            $table->integer('hours')->unsigned();
            $table->longText('description')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_task_attendances');
    }
}
