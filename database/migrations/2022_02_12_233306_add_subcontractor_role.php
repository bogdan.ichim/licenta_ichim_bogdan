<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddSubcontractorRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //
        $subcontractor_role = new Role;
        $subcontractor_role->name = 'subcontractor';
        $subcontractor_role->guard_name = 'web';
        $subcontractor_role->created_by = 0;
        $subcontractor_role->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $subcontractor_role = Role::where('name', 'subcontractor')->first();
        $subcontractor_role->delete();
    }
}
