<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewWorkAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_work_attendances', function (Blueprint $table) {
            $table->id();
            $table->integer('employee_id')->unsigned()->default(0);
            $table->integer('project_group')->unsigned();
            $table->string('project_name')->nullable();
            $table->integer('hours');
            $table->string('milestone')->nullable();
            $table->longText('task_description');
            $table->timestamp('work_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_work_attendances');
    }
}
