<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Studenti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studenti', function (Blueprint $table) {
            $table->id(); // id column with auto-increment
            $table->string('nume', 14);
            $table->string('prenume', 14);
            $table->integer('grupa');
            $table->integer('nota1');
            $table->integer('nota2');
            $table->double('media', 8, 2); // 'media' column with double data type and 2 decimal places
            $table->timestamps(); // created_at and updated_at columns for timestamps
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studenti');
    }
}
