<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProjectGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_groups', function (Blueprint $table) {
            //
            $table->boolean('non_billable')->after('vacation');
            $table->boolean('unpaid_leave')->after('non_billable');
            $table->boolean('sick_leave')->after('unpaid_leave');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_groups', function (Blueprint $table) {
            //
            $table->dropColumn('non_billable');
            $table->dropColumn('unpaid_leave');
            $table->dropColumn('sick_leave');
        });
    }
}
