<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhaseToNewWorkAttendace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_work_attendances', function (Blueprint $table) {
            $table->string('phase')->after('milestone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_work_attendances', function (Blueprint $table) {
            $table->dropColumn('milestone');
        });
    }
}
