@extends('layouts.admin')
@section('page-title')
    {{__('Manage Invoices')}}
@endsection
@push('script-page')
    <script>
        $('.copy_link').click(function (e) {
            e.preventDefault();
            var copyText = $(this).attr('href');


            document.addEventListener('copy', function (e) {
                e.clipboardData.setData('text/plain', copyText);
                e.preventDefault();
            }, true);

            document.execCommand('copy');
            show_toastr('Success', 'Url copied to clipboard', 'success');
        });
    </script>
@endpush
@section('action-button')
    @can('create invoice')
        <div class="col-0" >
            @if(!\Auth::guard('customer')->check())
                {{ Form::open(array('route' => array('invoice.index'),'method' => 'GET','id'=>'customer_submit')) }}
            @else
                {{ Form::open(array('route' => array('customer.invoice'),'method' => 'GET','id'=>'customer_submit')) }}
            @endif
        </div>
        <div class="row d-flex justify-content-end">
          <div class="col-2">
            <div class="all-select-box">
                <div class="btn-box">
                    {{ Form::label('issue_date', __('Date'),['class'=>'text-type']) }}
                    {{ Form::text('issue_date', isset($_GET['issue_date'])?$_GET['issue_date']:null, array('class' => 'form-control month-btn datepicker-range')) }}
                </div>
            </div>
          </div>

          @if(!\Auth::guard('customer')->check())
              <div class="col-auto">
                  <div class="all-select-box">
                      <div class="btn-box">
                          {{ Form::label('customer', __('Customer'),['class'=>'text-type']) }}
                          {{ Form::select('customer',$customer,isset($_GET['customer'])?$_GET['customer']:'', array('class' => 'form-control select2')) }}
                      </div>
                  </div>
              </div>
          @endif
          <div class="col-2">
              <div class="all-select-box">
                  <div class="btn-box">
                      {{ Form::label('status', __('Status'),['class'=>'text-type']) }}
                      {{ Form::select('status', [''=>'All']+$status,isset($_GET['status'])?$_GET['status']:'', array('class' => 'form-control select2')) }}
                  </div>
              </div>
          </div>
          <div class="col-auto my-custom">
              <a href="#" class="apply-btn" onclick="document.getElementById('customer_submit').submit(); return false;" data-toggle="tooltip" data-original-title="{{__('apply')}}">
                  <span class="btn-inner--icon"><i class="fas fa-search"></i></span>
              </a>
              @if(!\Auth::guard('customer')->check())
                  <a href="{{route('invoice.index')}}" class="reset-btn" data-toggle="tooltip" data-original-title="{{__('Reset')}}">
                      <span class="btn-inner--icon"><i class="fas fa-trash-restore-alt"></i></span>
                  </a>
              @else
                  <a href="{{route('customer.index')}}" class="reset-btn" data-toggle="tooltip" data-original-title="{{__('Reset')}}">
                      <span class="btn-inner--icon"><i class="fas fa-trash-restore-alt"></i></span>
                  </a>
              @endif
          </div>
          {{ Form::close() }}
          <div class="col-2 my-custom-btn">
              <div class="all-button-box">
                  <a href="{{ route('invoice.create',0) }}" class="btn btn-xs btn-white btn-icon-only width-auto">
                      <i class="fas fa-plus"></i> {{__('Create')}}
                  </a>
              </div>
          </div>
      </div>
    @endcan
@endsection


@section('content')
    <div class="">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body py-0 mt-2">
                  @include('invoice.components.invoices-view')
                </div>
            </div>
        </div>
    </div>
@endsection
