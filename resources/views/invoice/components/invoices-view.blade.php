<div class="table-responsive">
    <table class="table table-striped mb-0 dataTable">
        <thead>
        <tr>
            <th> {{__('Invoice')}}</th>
            @if(!\Auth::guard('customer')->check())
                <th>{{__('Customer')}}</th>
            @endif
            <th>{{__('Issue Date')}}</th>
            <th>{{__('Paid Date')}}</th>
            <th>{{__('Amount')}}</th>
            <th>{{__('Project')}}</th>
            <th>{{__('Task')}}</th>
            <th>{{__('Contract Number')}}</th>
            <th>{{__('Status')}}</th>
            @if(Gate::check('edit invoice') || Gate::check('delete invoice') || Gate::check('show invoice'))
                <th>{{__('Action')}}</th>
            @endif
        </tr>
        </thead>

        <tbody>
        @foreach ($invoices as $invoice)
            <tr>
                <td class="Id">
                    @if(\Auth::guard('customer')->check())
                        <a href="{{ route('customer.invoice.show',\Crypt::encrypt($invoice->id)) }}">{{ AUth::user()->invoiceNumberFormat($invoice->invoice_id) }}</a>
                    @else
                        <a href="{{ route('invoice.show',\Crypt::encrypt($invoice->id)) }}">{{ AUth::user()->invoiceNumberFormat($invoice->invoice_id) }}</a>
                    @endif
                </td>
                @if(!\Auth::guard('customer')->check())
                    <td> {{!empty($invoice->customer)? $invoice->customer->name:'' }} </td>
                @endif
                <td data-sort="{{ Carbon\Carbon::createFromFormat('Y-m-d', strval($invoice->issue_date))->timestamp }}">{{ Auth::user()->dateFormat($invoice->issue_date) }}</td>
                <td data-sort="{{ Carbon\Carbon::createFromFormat('Y-m-d', strval($invoice->paid_date))->timestamp }}">{{ Auth::user()->dateFormat($invoice->paid_date) }}</td>
                <td data-sort="{{ $invoice->getTotal() }}">{{ \Auth::user()->priceFormat($invoice->getTotal())}}</td>
                <td>@if($invoice->project)
                  <a href="{{ route('projects.show', ['project'=>$invoice->project]) }}">{{ $invoice->project->project_name }}</a>
                @else
                  -
                @endif</td>

                <td>@if($invoice->income && count($invoice->income->tasks())>0)
                  @php
                    $i = 0;
                  @endphp
                  @foreach ($invoice->income->tasks() as $task)
                    <a href="{{ url('projects/'.$invoice->project->id.'/task') }}">{{ $task->name }}</a>
                    @if($i<count($invoice->income->tasks())-1)
                      ,
                    @endif
                    @php
                      $i++;
                    @endphp
                  @endforeach
                @else
                  -
                @endif</td>
                <td>{{ $invoice->ref_number }}</td>
                <td>
                    @if($invoice->status == 0)
                        <span class="badge badge-pill badge-primary">{{ __(\App\Models\Invoice::$statues[$invoice->status]) }}</span>
                    @elseif($invoice->status == 1)
                        <span class="badge badge-pill badge-warning">{{ __(\App\Models\Invoice::$statues[$invoice->status]) }}</span>
                    @elseif($invoice->status == 2)
                        <span class="badge badge-pill badge-danger">{{ __(\App\Models\Invoice::$statues[$invoice->status]) }}</span>
                    @elseif($invoice->status == 3)
                        <span class="badge badge-pill badge-info">{{ __(\App\Models\Invoice::$statues[$invoice->status]) }}</span>
                    @elseif($invoice->status == 4)
                        <span class="badge badge-pill badge-success">{{ __(\App\Models\Invoice::$statues[$invoice->status]) }}</span>
                    @endif
                </td>
                @if(Gate::check('edit invoice') || Gate::check('delete invoice') || Gate::check('show invoice'))
                    <td class="Action">
                        <span>@php $invoiceID= Crypt::encrypt($invoice->id); @endphp
                          @can('duplicate invoice')
                          <!--<a href="#" class="edit-icon bg-success" data-toggle="tooltip" data-original-title="{{__('Duplicate')}}" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="You want to confirm this action. Press Yes to continue or Cancel to go back" data-confirm-yes="document.getElementById('duplicate-form-{{$invoice->id}}').submit();">
                                <i class="fas fa-copy"></i>
                                {!! Form::open(['method' => 'get', 'route' => ['invoice.duplicate', $invoice->id],'id'=>'duplicate-form-'.$invoice->id]) !!}
                                    {!! Form::close() !!}
                            </a>-->
                            @endcan
                            @can('show invoice')
                                @if(\Auth::guard('customer')->check())

                                @else
                                    <a href="{{ route('invoice.show',\Crypt::encrypt($invoice->id)) }}" class="edit-icon bg-info" data-toggle="tooltip" data-original-title="{{__('Detail')}}">
                                    <i class="fas fa-eye"></i>
                                    </a>
                                @endif
                            @endcan
                            @can('edit invoice')
                            <a href="{{ route('invoice.edit',Crypt::encrypt($invoice->id)) }}" class="edit-icon" data-toggle="tooltip" data-original-title="{{__('Edit')}}">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            @endcan
                            @can('delete invoice')
                                <a href="#" class="delete-icon " data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$invoice->id}}').submit();">
                                <i class="fas fa-trash"></i>
                            </a>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['invoice.destroy', $invoice->id],'id'=>'delete-form-'.$invoice->id]) !!}
                                {!! Form::close() !!}
                            @endcan
                        </span>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
