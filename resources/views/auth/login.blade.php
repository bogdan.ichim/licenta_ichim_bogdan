@extends('layouts.auth')
@php
    $logo=asset(Storage::url('uploads/logo/'));
    $company_logo=Utility::getValByName('company_logo');
@endphp
@section('page-title')
    {{__('Login')}}
@endsection
@section('content')
    <div class="login-contain">
        <div class="login-inner-contain">
            <div class="login-form">
                <div class="page-title"><h5>{{__('Login')}}</h5></div>
                {{Form::open(array('route'=>'login','method'=>'post','id'=>'loginForm' ))}}
                @csrf
                <div class="form-group">
                    <label for="email" class="form-control-label">{{__('Email')}}</label>
                    <input class="form-control @error('email') is-invalid @enderror" id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <div class="invalid-feedback" role="alert">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group position-relative">
                    <label for="password" class="form-control-label">{{__('Password')}}</label>
                    <input class="form-control @error('password') is-invalid @enderror" id="password" type="password" name="password" required autocomplete="current-password">
                    <span class="position-absolute" style="right: 10px; top: 35px; cursor: pointer;" onclick="togglePasswordVisibility()">
                        <i class="fas fa-eye mt-2" id="togglePasswordIcon"></i>
                    </span>
                    @error('password')
                    <div class="invalid-feedback" role="alert">{{ $message }}</div>
                    @enderror
                </div>

                <button type="submit" class="btn-login">{{__('Login')}}</button>
                <div class="or-text">{{__('OR')}}</div>
                <small class="text-muted">{{__("Don't have an account?")}}</small>
                <a href="{{ route('register') }}" class="text-xs text-primary">{{__('Register')}}</a>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <script>
        function togglePasswordVisibility() {
            var passwordField = document.getElementById("password");
            var toggleIcon = document.getElementById("togglePasswordIcon");
            if (passwordField.type === "password") {
                passwordField.type = "text";
                toggleIcon.classList.remove('fa-eye');
                toggleIcon.classList.add('fa-eye-slash');
            } else {
                passwordField.type = "password";
                toggleIcon.classList.remove('fa-eye-slash');
                toggleIcon.classList.add('fa-eye');
            }
        }
    </script>
@endsection
