@extends('layouts.auth')
@section('page-title')
    {{__('Forgot Password')}}
@endsection
@php
    $logo=asset(Storage::url('uploads/logo/'));
$company_logo=Utility::getValByName('company_logo');
@endphp
@section('content')
    <div class="login-contain">
        <div class="login-inner-contain">
            <div class="login-form">
                <div class="page-title"><h5><span>{{__('Forgot')}}</span> {{__('Password')}}</h5></div>
                <small class="text-muted"></small>
                @if (session('status'))
                    <small class="text-muted">{{ session('status') }}</small>
                @endif
                <form method="POST" action="{{ route('customer.password.email') }}">
                    @csrf
                    <div class="form-group">
                        <label class="form-control-label" for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn-login">{{ __('Send Password Reset Link') }}</button>
                    <div class="or-text">{{__('OR')}}</div>
                    <a href="{{ route('customer.login') }}" class="text-xs text-primary">{{__('Login')}}</a>
                </form>
            </div>
        </div>
    </div>
@endsection
