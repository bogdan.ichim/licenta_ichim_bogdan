@extends('layouts.admin')
@section('page-title')
    {{__('Employee Set Salary')}}
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="card min-height-253">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <h6 class="mb-0">{{__('Employee Salary')}}</h6>
                                </div>
                                @can('create set salary')
                                    <div class="col text-right">
                                        <a href="#" data-url="{{ route('employee.basic.salary',$employee->id) }}" data-size="md" data-ajax-popup="true" data-title="{{__('Set Basic Sallary')}}" data-toggle="tooltip" data-original-title="{{__('Basic Salary')}}" class="apply-btn">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="project-info d-flex text-sm">
                                <div class="project-info-inner mr-3 col-6">
                                    <b class="m-0"> {{__('Payslip Type') }} </b>
                                    <div class="project-amnt pt-1">@if(!empty($employee->salary_type())){{ $employee->salary_type() }}@else -- @endif</div>
                                </div>
                                <div class="project-info-inner mr-3 col-6">
                                    <b class="m-0"> {{__('Salary') }} </b>
                                    <div class="project-amnt pt-1">@if(!empty($employee->salary)){{ $employee->salary }}@else -- @endif</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card  min-height-253">
                        <div class="card-header ">
                            <div class="row">
                                <div class="col">
                                    <h6 class="mb-0">{{__('Allowance')}}</h6>
                                </div>
                                @can('create allowance')
                                    <div class="col text-right">
                                        <a href="#" data-url="{{ route('allowances.create',$employee->id) }}" data-size="md" data-ajax-popup="true" data-title="{{__('Create Allowance')}}" data-toggle="tooltip" data-original-title="{{__('Create Allowance')}}" class="apply-btn">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="table-responsive">
                          @if(!$allowances->isEmpty())
                            <table class="table table-striped mb-0">
                                <thead>
                                <tr>
                                    <th>{{__('Employee Name')}}</th>
                                    <th>{{__('Allownace Option')}}</th>
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($allowances as $allowance)
                                    <tr>
                                        <td>{{ !empty($allowance->employee())?$allowance->employee()->name:'' }}</td>
                                        <td>{{ !empty($allowance->allowance_option())?$allowance->allowance_option()->name:'' }}</td>
                                        <td>{{ $allowance->title }}</td>
                                        <td>{{  \Auth::user()->priceFormat($allowance->amount) }}</td>
                                        <td>
                                            @can('edit allowance')
                                                <a href="#" data-url="{{ URL::to('allowance/'.$allowance->id.'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit Allowance')}}" class="edit-icon" data-toggle="tooltip" data-original-title="{{__('Edit')}}"><i class="fas fa-pencil-alt"></i></a>
                                            @endcan
                                            @can('delete allowance')
                                                <a href="#" class="delete-icon" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('allowance-delete-form-{{$allowance->id}}').submit();"><i class="fas fa-trash"></i></a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['allowance.destroy', $allowance->id],'id'=>'allowance-delete-form-'.$allowance->id]) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                              </table>
                              @else
                              <div class="mt-2 text-center">
                                Nu este inregistrată nicio idemnizație!
                              </div>
                              @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card  min-height-253">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <h6 class="mb-0">{{__('Taxes')}}</h6>
                                </div>
                                @can('create commission')
                                    <div class="col text-right">
                                        <a href="#" data-url="{{ route('commissions.create',$employee->id) }}" data-size="md" data-ajax-popup="true" data-title="{{__('Create Commission')}}" data-toggle="tooltip" data-original-title="{{__('Create Commission')}}" class="apply-btn">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="table-responsive">
                          @if(!$commissions->isEmpty())
                            <table class="table table-striped mb-0">
                                <thead>
                                <tr>
                                    <th>{{__('Employee Name')}}</th>
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($commissions as $commission)
                                    <tr>
                                        <td>{{ !empty($commission->employee())?$commission->employee()->name:'' }}</td>
                                        <td>{{ $commission->title }}</td>
                                        <td>{{ \Auth::user()->priceFormat( $commission->amount) }}</td>
                                        <td class="text-right">
                                            @can('edit commission')
                                                <a href="#" data-url="{{ URL::to('commission/'.$commission->id.'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit Commission')}}" class="edit-icon" data-toggle="tooltip" data-original-title="{{__('Edit')}}"><i class="fas fa-pencil-alt"></i></a>
                                            @endcan
                                            @can('delete commission')
                                                <a href="#" class="delete-icon" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('commission-delete-form-{{$commission->id}}').submit();"><i class="fas fa-trash"></i></a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['commission.destroy', $commission->id],'id'=>'commission-delete-form-'.$commission->id]) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="mt-2 text-center">
                            Nu este intregistrat niciun comision!
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <h6 class="mb-0">{{__('Overtime')}}</h6>
                                </div>
                                @can('create overtime')
                                    <div class="col text-right">
                                        <a href="#" data-url="{{ route('overtimes.create',$employee->id) }}" data-size="md" data-ajax-popup="true" data-title="{{__('Create Overtime')}}" data-toggle="tooltip" data-original-title="{{__('Create Overtime')}}" class="apply-btn">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                @endcan
                            </div>
                        </div>
                        <div class="table-responsive">
                          @if(!$overtimes->isEmpty())
                            <table class="table table-striped mb-0">
                                <thead>
                                <tr>
                                    <th>{{__('Employee Name')}}</th>
                                    <th>{{__('Overtime Title')}}</th>
                                    <th>{{__('Number of days')}}</th>
                                    <th>{{__('Hours')}}</th>
                                    <th>{{__('Rate')}}</th>
                                    <th>{{__('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($overtimes as $overtime)
                                    <tr>
                                        <td>{{ !empty($overtime->employee())?$overtime->employee()->name:'' }}</td>
                                        <td>{{ $overtime->title }}</td>
                                        <td>{{ $overtime->number_of_days }}</td>
                                        <td>{{ $overtime->hours }}</td>
                                        <td>{{  \Auth::user()->priceFormat($overtime->rate) }}</td>
                                        <td class="text-right">
                                            @can('edit overtime')
                                                <a href="#" data-url="{{ URL::to('overtime/'.$overtime->id.'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit OverTime')}}" class="edit-icon" data-toggle="tooltip" data-original-title="{{__('Edit')}}"><i class="fas fa-pencil-alt"></i></a>
                                            @endcan
                                            @can('delete overtime')
                                                <a href="#" class="delete-icon" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('overtime-delete-form-{{$overtime->id}}').submit();"><i class="fas fa-trash"></i></a>
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['overtime.destroy', $overtime->id],'id'=>'overtime-delete-form-'.$overtime->id]) !!}
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                          @else
                            <div class="mt-2 text-center">
                              Nu există ore peste program!
                            </div>
                          @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('script-page')
    <script type="text/javascript">

        $(document).ready(function () {
            var d_id = $('#department_id').val();
            var designation_id = '{{ $employee->designation_id }}';
            getDesignation(d_id);


            $("#allowance-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });

            $("#commission-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });

            $("#loan-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });

            $("#saturation-deduction-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });

            $("#other-payment-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });

            $("#overtime-dataTable").dataTable({
                "columnDefs": [
                    {"sortable": false, "targets": [1]}
                ]
            });
        });

        $(document).on('change', 'select[name=department_id]', function () {
            var department_id = $(this).val();
            getDesignation(department_id);
        });

        function getDesignation(did) {
            $.ajax({
                url: '{{route('employee.json')}}',
                type: 'POST',
                data: {
                    "department_id": did, "_token": "{{ csrf_token() }}",
                },
                success: function (data) {
                    $('#designation_id').empty();
                    $('#designation_id').append('<option value="">{{__('Select any Designation')}}</option>');
                    $.each(data, function (key, value) {
                        var select = '';
                        if (key == '{{ $employee->designation_id }}') {
                            select = 'selected';
                        }

                        $('#designation_id').append('<option value="' + key + '"  ' + select + '>' + value + '</option>');
                    });
                }
            });
        }

    </script>
@endpush
