<div class="card bg-none card-box">
    {{Form::open(array('url'=>'admin-costs','method'=>'post'))}}
    <div class="row">
        <div class="form-group col-md-6 col-lg-6">
            <label for="name">Nume</label>
            <input type="text" class="form-control" name="name"
             placeholder="Numele costului administrativ (ex.: chirie birou)" required value="">
        </div>
        <div class="form-group col-md-6 col-lg-6">
            <label for="cost">Suma necesară</label>
            <input type="number" class="form-control" name="cost" placeholder="Suma în RON"
             min="0" step="0.01" required value="">
        </div>
        <div class="form-group col-md-6 col-lg-6">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="period" id="monthly"
             style="height: 15px;" value="monthly" checked>
            <label class="form-check-label" for="monthly">
              Lunar
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" name="period" type="radio"
             style="height: 15px;" id="yearly" value="yearly">
            <label class="form-check-label" for="yearly">
              Anual
            </label>
          </div>

        </div>
        <div class="form-group col-md-6 col-lg-6">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="remote" id="remoteCheckbox" style="height: 15px;">
            <label class="form-check-label" for="remoteCheckbox">
              Se aplică angajațiilor din mediul remote?
            </label>
          </div>
        </div>
        <div class="col-12">
            <input type="submit" value="{{__('Create')}}" class="btn-create badge-blue">
            <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
        </div>
    </div>
    {{Form::close()}}
</div>
