<div class="card bg-none card-box">
    {{Form::model($cost,array('route' => array('admin-costs.update', $cost->id), 'method' => 'PUT')) }}
    <input type="hidden" name="cost-id" value="{{ $cost->id }}">
    <div class="row">
        <div class="form-group col-md-6 col-lg-6">
            <label for="name">Nume</label>
            <input type="text" class="form-control" name="name"
             placeholder="Numele costului administrativ (ex.: chirie birou)" required value="{{ $cost->name }}">
        </div>
        <div class="form-group col-md-6 col-lg-6">
            <label for="cost">Suma necesară</label>
            <input type="number" class="form-control" name="cost" placeholder="Suma în RON"
             min="0" step="0.01" required value="{{ $cost->cost }}">
        </div>
        <div class="form-group col-md-6 col-lg-6">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="period" id="monthly"
             style="height: 15px;" value="monthly"
             @if($cost->period == 'monthly')
               checked
             @endif
             >
            <label class="form-check-label" for="monthly">
              Lunar
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" name="period" type="radio"
             style="height: 15px;" id="yearly" value="yearly"
             @if($cost->period == 'yearly')
               checked
             @endif>
            <label class="form-check-label" for="yearly">
              Anual
            </label>
          </div>

        </div>
        <div class="form-group col-md-6 col-lg-6">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="remote" id="remoteCheckbox" style="height: 15px;"
            @if($cost->remote)
              checked
            @endif>
            <label class="form-check-label" for="remoteCheckbox">
            Se aplică angajațiilor din mediul remote?
            </label>
          </div>
        </div>
        <div class="col-12">
            <input type="submit" value="{{__('Edit')}}" class="btn-create badge-blue">
            <input type="button" value="{{__('Cancel')}}" class="btn-create bg-gray" data-dismiss="modal">
        </div>
    </div>
    {{Form::close()}}
</div>
