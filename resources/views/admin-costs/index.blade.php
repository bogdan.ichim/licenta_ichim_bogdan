@extends('layouts.admin')
@section('page-title')
    {{__('Manage Administrative Costs')}}
@endsection
@push('script-page')

@endpush

@section('action-button')
  <div class="all-button-box row d-flex justify-content-end">
    <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
    <a href="#" data-url="{{ route('admin-costs.create') }}" class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-title="{{__('Create New Administrative Cost')}}">
        <i class="fa fa-plus"></i> {{__('Create')}}
    </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="col-12 mt-3">
      <div class="card">
          <div class="table-responsive">
              <table class="table align-items-center">
                  <thead>
                  <tr>
                      <th scope="col">{{__('Name')}}</th>
                      <th scope="col">{{__('Cost')}}</th>
                      <th scope="col">{{__('Period')}}</th>
                      <th scope="col">{{__('Apply to remote?')}}</th>
                      <th scope="col">{{__('Actions')}}</th>
                  </tr>
                  </thead>
                  <tbody class="list">
                    @if(count($costs)>0)
                      @foreach ($costs as $cost)
                        <tr>
                          <td>{{ $cost->name }}</td>
                          <td>{{ 'RON'.$cost->cost }}</td>
                          <td><span class="badge
                            @if($cost->period=='monthly')
                              badge-primary
                            @else
                              badge-secondary
                            @endif
                            ">@if($cost->period=='monthly') 
                            LUNAR 
                            @else 
                            ANUAL 
                            @endif
                          </span></td>
                          <td>
                            @if($cost->remote)
                              DA
                            @else
                              NU
                            @endif
                          </td>
                          <td>
                            <a href="#" data-url="{{ route('cost.edit',$cost->id) }}" class="edit-icon" data-ajax-popup="true" data-title="{{__('Edit Administrative Cost')}}">
                                <i class="fas fa-pencil-alt"></i>
                            </a>

                            <a href="#" data-url="{{ route('admin-costs.create') }}" class="delete-icon " data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$cost->id}}').submit();">
                                <i class="fas fa-trash"></i>
                            </a>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['cost.destroy', $cost->id],'id'=>'delete-form-'.$cost->id]) !!}
                                {!! Form::close() !!}
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                        <td colspan="4" class="text-center">Nu există costuri administrative.</td>
                      </tr>
                    @endif
                  </tbody>
              </table>
          </div>
      </div>
  </div>


@endsection
