@extends('layouts.admin')

@section('page-title')
    {{ucwords($project->project_name)}}
@endsection
@section('action-button')
    <div class="col-md-12 d-flex align-items-center justify-content-between justify-content-md-end">
        @can('edit project')
        <a href="#" data-url="{{ route('projects.edit', $project->id) }}" data-size="lg" data-ajax-popup="true"
        data-title="{{__('Edit Project')}}" class="btn btn-xs btn-white btn-icon-only width-auto">
        <i class="fas fa-pencil-alt"></i> {{__('Edit')}}</a>
        @endcan
        @can('create project task')
            <a href="{{ route('projects.tasks.index',$project->id) }}" class="btn btn-xs btn-white btn-icon-only width-auto">
                <span class="btn-inner--icon"><i class="fas fa-list"></i>{{__('Activities')}}</span>
            </a>
        @endcan
        <a href="{{ route('projects.index') }}" class="btn btn-xs btn-white btn-icon-only width-auto">
            <span class="btn-inner--icon"><i class="fas fa-arrow-left"></i>{{__('Back')}}</span>
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-3 col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="mb-0">{{ $project_data['task_chart']['total'] }}</h6>
                                <span class="text-sm text-muted">{{__('Last 7 days activities done')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 pt-4 pb-5">
                        <div class="spark-chart" data-toggle="spark-chart" data-color="info" data-dataset="{{ json_encode($project_data['task_chart']['chart']) }}"></div>
                    </div>
                    <div class="progress-wrapper mb-3">
                        <small class="progress-label">{{ __('Day Left') }} <span class="text-muted">{{ $project_data['day_left']['day'] }}</span></small>
                        <div class="progress mt-0 height-3">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{ $project_data['day_left']['percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project_data['day_left']['percentage'] }}%;"></div>
                        </div>
                    </div>
                    <div class="progress-wrapper">
                        <small class="progress-label">{{__('Open Ativity')}} <span class="text-muted">{{ $project_data['open_task']['tasks'] }}</span></small>
                        <div class="progress mt-0 height-3">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{ $project_data['open_task']['percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project_data['open_task']['percentage'] }}%;"></div>
                        </div>
                    </div>
                    <div class="progress-wrapper">
                        <small class="progress-label">{{__('Completed Milestone')}} <span class="text-muted">{{ $project_data['milestone']['total'] }}</span></small>
                        <div class="progress mt-0 height-3">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{ $project_data['milestone']['percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project_data['milestone']['percentage'] }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h6 class="text-muted mb-1">{{ __('Project Hours') }}</h6>
                        </div>
                    </div>
                    <div class="progress-wrapper mb-3">
                        <small class="progress-label">Ore totale <span class="text-muted">{{ $project_data['task_allocated_hrs']['difference_hrs_worked_hours'] }}</span></small>
                        <div class="progress mt-0 height-3">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{ $project_data['task_allocated_hrs']['percentage_difference_hrs_worked_hours'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project_data['task_allocated_hrs']['percentage_difference_hrs_worked_hours'] }}%;"></div>
                        </div>
                    </div>
                    <div class="progress-wrapper mb-3">
                        <small class="progress-label">{{ __('Total hours remaining') }} <span class="text-muted">{{ $project_data['task_allocated_hrs']['remaining_hours'] }}</span></small>
                        <div class="progress mt-0 height-3">
                            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="{{$project_data['task_allocated_hrs']['percentage_remaining_hours'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project_data['task_allocated_hrs']['percentage_remaining_hours'] }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Project Overview--}}
        <div class="card col-sm-5  col-md-6  col-lg-6  col-xl-9">
            <div class="m-1 card-fluid">
                <div class="card-header">
                    <h6 class="mb-0">{{__('Project overview')}}</h6>
                </div>
                <div class="card-body py-3 flex-grow-1">
                    <div class="pb-3 mb-3 border-bottom">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <img {{ $project->img_image }} class="avatar rounded-circle">
                            </div>
                            <div class="col ml-n2">
                                <div class="progress-wrapper">
                                    <span class="progress-percentage" style="left: 0;">
                                      <small class="font-weight-bold">
                                        <b><h5>{{ $project->project_name }}</h5></b>
                                      </span>
                                    </span>
                                    <span class="progress-percentage"><small class="font-weight-bold">{{__('Completed:')}} </small>{{ $project->project_progress()['percentage'] }}</span>
                                    <div class="progress progress-xs mt-2">
                                        <div class="progress-bar bg-{{ $project->project_progress()['color'] }}" role="progressbar" aria-valuenow="{{ $project->project_progress()['percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $project->project_progress()['percentage'] }};"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-sm mb-0">
                        {{ $project->description }}
                    </p>
                </div>
                <div class="card-footer py-0 px-0">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <small>{{__('Start date')}}:</small>
                                    <div class="h6 mb-0">{{ Utility::getDateFormated($project->start_date) }}</div>
                                </div>
                                <div class="col-6">
                                    <small>{{__('End date')}}:</small>
                                    <div class="h6 mb-0 {{ (strtotime($project->end_date) < time()) ? 'text-danger' : '' }}">{{ Utility::getDateFormated($project->end_date) }}</div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
    <div class="col-xl-12 col-sm-6">
<div class="card card-stats">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <h6 class="text-muted mb-1">{{ __('Project Group') }}</h6>
            </div>
        </div>
        <div class="row">
          <div class="col-12">
            @can('create project')
              <form action="/project-groups/add-group/{{ $project->id }}" class="text-center" method="post">
                @csrf
                <div class="form-group">
                  <label for="project-group">Selectează grupul:</label>
                  <select class="form-control" name="project-group" id="project-group">
                    <option value="null">Null</option>
                    @foreach ($groups as $group)
                      <option value="{{ $group->id_number }}"
                        @if($group->id_number==$projectGroup)selected @endif>{{ $group->id_number }}</option>
                    @endforeach
                  </select>
                </div>
                <input type="submit" name="submit" class="btn btn-submit text-light" value="Modifică grupul">
              </form>
            @else
              <p>Project group ID: {{ $projectGroup }}</p>
            @endcan
          </div>
        </div>
    </div>
</div>
</div>
  </div>
</div>
    <div class="row">
        {{--Users--}}
        <div class="col-xl-6">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-0">{{__('Members')}}</h6>
                        </div>
                        <div class="col-auto">

                            <div class="actions">
                                @can('edit project')
                                <a href="#" class="btn btn-sm btn-white float-right add-small" data-url="{{ route('invite.project.member.view', $project->id) }}" data-ajax-popup="true" data-size="lg" data-title="{{__('Add Member')}}">
                                    <span class="btn-inner--icon">
                                        <i class="fas fa-plus"></i>
                                        {{__('Add')}}
                                    </span>
                                </a>
                                @endcan
                            </div>

                        </div>




                    </div>
                </div>
                <div class="table-responsive">
                    <div class="mh-350 min-h-350">
                        <table class="table align-items-center">
                            <tbody class="list" id="project_users">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>

        {{--Subcontractors--}}
        <div class="col-xl-6">
            <div class="card">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="mb-0">{{__('Subcontractors')}}</h6>
                        </div>
                        <div class="col-auto">

                            <div class="actions">
                                @can('edit project')
                                <a href="#" class="btn btn-sm btn-white float-right add-small" data-url="{{ route('invite.project.subcontractor.view', $project->id) }}" data-ajax-popup="true" data-size="lg" data-title="{{__('Add Subcontractors')}}">
                                    <span class="btn-inner--icon">
                                        <i class="fas fa-plus"></i>
                                        {{__('Add')}}
                                    </span>
                                </a>
                                @endcan
                            </div>

                        </div>




                    </div>
                </div>
                <div class="table-responsive">
                    <div class="mh-350 min-h-350">
                        <table class="table align-items-center">
                            <tbody class="list" id="project_subcontractors">

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="card col-xl-12">
            <div class="">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="mb-0">{{__('Milestones')}} ({{count($project->milestones)}})</h6>
                        </div>
                        @can('create milestone')
                            <div class="text-right">
                                <a href="#" data-url="{{ route('project.milestone',$project->id) }}" data-ajax-popup="true" data-title="{{__('Create New Milestone')}}" class="btn btn-sm btn-white float-right add-small">
                                    <span class="btn-inner--icon">
                                        <i class="fas fa-plus"></i>
                                        {{__('Add')}}
                                    </span>
                                </a>
                            </div>
                        @endcan
                    </div>
                </div>

                <div class="mh-350 min-h-350">
                    <div class="list-group list-group-flush">
                        @if($project->milestones->count() > 0)
                            @foreach($project->milestones as $milestone)
                                <span class="list-group-item list-group-item-action">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <h6 class="text-sm d-block text-limit mb-0">{{ $milestone->title }}
                                            <span class="badge badge-pill badge-{{\App\Models\Project::$status_color[$milestone->status]}}">{{ __(\App\Models\Project::$project_status[$milestone->status]) }}</span>
                                        </h6>
                                        <span class="d-block text-sm text-muted">{{ $milestone->tasks->count().' '. __('Activities') }}</span>
                                    </div>
                                    <div class="media-body text-right">
                                            <a href="#" class="action-item"
                                                data-url="{{ route('project.milestone.show',$milestone->id) }}" data-ajax-popup="true"
                                                data-title="{{ $milestone->title }}" data-toggle="tooltip"
                                                data-original-title="{{ __('View') }}" data-size='lg'>
                                                <span class="btn-inner--icon"><i class="fas fa-eye"></i></span>
                                            </a>
                                        @can('edit milestone')
                                            <a href="#" class="action-item"
                                                    data-url="{{ route('project.milestone.edit',$milestone->id) }}" data-ajax-popup="true"
                                                    data-title="{{ __('Edit Milestone') }}" data-toggle="tooltip"
                                                    data-original-title="{{ __('Edit') }}" data-size='md'>
                                                    <span class="btn-inner--icon"><i class="fas fa-pencil-alt"></i></span>
                                            </a>
                                        @endcan
                                        @can('delete milestone')

                                            <a href="#" class="action-item text-danger" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?')}}|{{__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$milestone->id}}').submit();">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['project.milestone.destroy', $milestone->id],'id'=>'delete-form-'.$milestone->id]) !!}
                                            {!! Form::close() !!}
                                        @endcan
                                    </div>
                                </div>
                            </span>
                            @endforeach
                        @else
                            <div class="py-5">
                                <h6 class="h6 text-center">{{__('No Milestone Found.')}}</h6>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script-page')
<script>
    $(document).ready(function () {
            loadProjectUser();
            loadProjectSubcontractor();
        $(document).on('click', '.invite_usr', function () {
            var project_id = $('#project_id').val();
            var user_id = $(this).attr('data-id');

            $.ajax({
                url: '{{ route('invite.project.user.member') }}',
                method: 'POST',
                dataType: 'json',
                data: {
                    'project_id': project_id,
                    'user_id': user_id,
                    "_token": "{{ csrf_token() }}"
                },
                success: function (data) {
                    if (data.code == '200') {
                        show_toastr(data.status, data.success, 'success')
                        setInterval('location.reload()', 5000);
                        loadProjectUser();
                        loadProjectSubcontractor();
                    } else if (data.code == '404') {
                        show_toastr(data.status, data.errors, 'error')
                    }

                }
            });
        });
    });

    function loadProjectUser() {
            var mainEle = $('#project_users');
            var project_id = '{{$project->id}}';

            $.ajax({
                url: '{{ route('project.user') }}',
                data: {project_id: project_id},
                beforeSend: function () {
                    $('#project_users').html('<tr><th colspan="2" class="h6 text-center pt-5">{{__('Loading...')}}</th></tr>');
                },
                success: function (data) {
                    mainEle.html(data.html);
                    $('[id^=fire-modal]').remove();
                    loadConfirm();
                }
            });
        }

      function loadProjectSubcontractor() {
              var mainEle = $('#project_subcontractors');
              var project_id = '{{$project->id}}';

              $.ajax({
                  url: '{{ route('project.subcontractor') }}',
                  data: {project_id: project_id},
                  beforeSend: function () {
                      $('#project_subcontractors').html('<tr><th colspan="2" class="h6 text-center pt-5">{{__('Loading...')}}</th></tr>');
                  },
                  success: function (data) {
                      mainEle.html(data.html);
                      $('[id^=fire-modal]').remove();
                      loadConfirm();
                  }
              });
          }
</script>
@endpush
