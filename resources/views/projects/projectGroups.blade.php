@extends('layouts.admin')
@section('content')
  <style media="screen">
    .card form input{
      height: 15px;
      line-height: 15px;
    }

    .scrollable-y{
      overflow-y: scroll;
      overflow-x: hidden;
      height: 200px;
    }

    .scrollable{
      overflow: scroll;
      height: 300px;
    }
  </style>

<script type="text/javascript">
  function uncheckPrevious(selected) {
    var radio_buttons = document.getElementsByClassName("group-radio");
    for (var i = 0; i < radio_buttons.length; i++) {
        if(radio_buttons[i]){
            radio_buttons[i].checked = false;
        }
    }
    selected.checked = true;
  }
</script>


<div class="row">
  <div class="col-12 col-md-6 p-2">
    <div class="card">
      <div class="card-header">
        <h3>{{__('Project groups')}}</h3>
      </div>
      <div class="card-body">
        <div class="scrollable-y">
          @foreach ($groups as $group)
            <div class="row border mb-1">
              <div class="col-1 pt-2">
                <h6 class="text-center">{{ $group->id_number }}</h6>
                </div>

              <div class="col-1 pt-2">
              </div>

              <div class="col-3 pt-2">
                <h6 class="text-center">{{__('Type:')}}</h6>
              </div>

              <div class="col-3 pt-2">
              @if ($group->billable == 1)
                  <h6 class="text-center">Facturabil</h6>
                @endif

                @if ($group->non_billable == 1)
                  <h6 class="text-center">Nefacturabil</h6>
                @endif
              </div>

              <div class="col-1 pt-2">
              </div>


              <div class="col-3" style="padding-bottom: 5px;">
                <a class="btn btn-danger" style="font-size: 12px;" href="/project-groups/delete/{{ $group->id }}">{{__('Delete')}}</a>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

  <div class="col-12 col-md-6 p-2">
    <div class="card">
      <div class="card-header">
        <h3>{{__('Add a group project')}}</h3>
      </div>
      <div class="card-body">
        <form action="/project-groups/add" method="post">
          @csrf
          <div class="form-group">
            <input type="text" class="form-control" name="id_number" value="" placeholder="{{__('Number ID')}}">
          </div>

          <div class="form-check">
            <input class="form-check-input group-radio" type="radio" name="billable" value="billable" onchange="uncheckPrevious(this)" checked>
            <label class="form-check-label" for="billable">{{__('Is it billable?')}}</label>
          </div>

          <div class="form-check">
            <input class="form-check-input group-radio" type="radio" name="non_billable" value="non_billable" onchange="uncheckPrevious(this)">
            <label class="form-check-label" for="non_billable">{{__('Is it non-billable?')}}</label>
          </div>

          <input type="submit" class="btn btn-primary h-100" name="submit" value="{{__('Create')}}">

        </form>
      </div>
    </div>
  </div>
</div>

@endsection
