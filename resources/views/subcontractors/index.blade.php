@extends('layouts.admin')

@section('page-title')
    {{ __('Manage Subcontractors') }}
@endsection

@push('script-page')
    <script>
        $(document).on('click', '#billing_data', function () {
            $("[name='shipping_name']").val($("[name='billing_name']").val());
            $("[name='shipping_country']").val($("[name='billing_country']").val());
            $("[name='shipping_state']").val($("[name='billing_state']").val());
            $("[name='shipping_city']").val($("[name='billing_city']").val());
            $("[name='shipping_phone']").val($("[name='billing_phone']").val());
            $("[name='shipping_zip']").val($("[name='billing_zip']").val());
            $("[name='shipping_address']").val($("[name='billing_address']").val());
        })
    </script>
@endpush

@section('action-button')
    <div class="all-button-box row d-flex justify-content-end">
        @can('create client')
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6">
                <a href="#" class="btn btn-xs btn-white btn-icon-only width-auto" data-ajax-popup="true" data-size="lg" data-title="{{__('Create Subcontractor')}}" data-url="{{route('subcontractors.create')}}"><i class="fas fa-plus"></i> {{__('Add')}} </a>
            </div>
        @endcan
    </div>
@endsection

@section('content')
    <div class="row mt-0">
        @foreach($subcontractors as $subcontractor)
            <div class="col-lg-3 col-sm-6 col-md-4">
                <div class="card profile-card">
                    <div class="edit-profile user-text">
                        <div class="dropdown action-item">
                            @if($subcontractor->is_active == 1)
                                <a href="#" class="action-item" role="button" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-ellipsis-h"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{route('subcontractors.show',$subcontractor->id)}}" class="dropdown-item text-sm">{{__('View')}}</a>
                                    @can('edit client')
                                        <a href="#" class="dropdown-item text-sm" data-url="{{route('subcontractors.edit',$subcontractor->id)}}" data-ajax-popup="true" data-title="{{__('Edit Client')}}">{{__('Edit')}}</a>
                                    @endcan
                                    @can('delete client')
                                        <a class="dropdown-item text-sm" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$subcontractor->id}}').submit();">{{__('Delete')}}</a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['subcontractors.destroy', $subcontractor->id],'id'=>'delete-form-'.$subcontractor->id]) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </div>
                            @else
                                <a href="#" class="action-item"><i class="fas fa-lock"></i></a>
                            @endif
                        </div>
                    </div>
                    <div class="avatar-parent-child">
                        <img @if($subcontractor->avatar) src="{{asset(Storage::url("uploads/avatar/".$subcontractor->avatar))}}" @else src="{{asset('assets/img/avatar/avatar-1.png')}}" @endif class="avatar rounded-circle avatar-xl">
                    </div>
                    <h4 class="h4 mb-0 my-2"><a href="{{route('subcontractors.show',$subcontractor->id)}}">{{$subcontractor->name}}</a></h4>
                    <div class="sal-right-card">
                        <span class="badge badge-pill badge-blue">{{$subcontractor->email}}</span>
                    </div>
                    <small class="my-2" data-toggle="tooltip" data-placement="bottom" data-original-title="{{__('Last Login')}}">{{ (!empty($subcontractor->last_login_at)) ? $subcontractor->last_login_at : '-' }}</small>
                    <div class="office-time mb-0 mt-3">
                        <div class="row">
                            <div class="col-6">
                                <div class="font-weight-bold text-sm">{{__('Deals')}}</div>
                                @if($subcontractor->clientDeals)
                                    {{$subcontractor->clientDeals->count()}}
                                @endif
                            </div>
                            <div class="col-6">
                                <div class="font-weight-bold text-sm">{{__('Projects')}}</div>
                                @if($subcontractor->clientProjects)
                                    {{ $subcontractor->clientProjects->count() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
