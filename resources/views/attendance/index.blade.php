@extends('layouts.admin')
@section('page-title')
{{__('Manage Attendance List')}}
@endsection
@push('script-page')
<script>
    $('input[name="type"]:radio').on('change', function(e) {
        var type = $(this).val();

        if (type == 'monthly') {
            $('.month').addClass('d-block');
            $('.month').removeClass('d-none');
            $('.date').addClass('d-none');
            $('.date').removeClass('d-block');
        } else {
            $('.date').addClass('d-block');
            $('.date').removeClass('d-none');
            $('.month').addClass('d-none');
            $('.month').removeClass('d-block');
        }
    });

    $('input[name="type"]:radio:checked').trigger('change');
</script>
@endpush
@section('action-button')
<div class="row d-flex justify-content-end">
    <div class="col-auto">
        {{ Form::open(array('route' => array('attendanceemployee.index'),'method'=>'get','id'=>'attendanceemployee_filter')) }}
    </div>
    <div class="col-3">
        <div class="all-select-box">
            <div class="btn-box">
                <label class="text-type">{{__('Type')}}</label> <br>
                <div class="d-flex radio-check">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="monthly" value="monthly" name="type" class="custom-control-input" {{isset($_GET['type']) && $_GET['type']=='monthly' ?'checked':'checked'}}>
                        <label class="custom-control-label" for="monthly">{{__('Monthly')}}</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="daily" value="daily" name="type" class="custom-control-input" {{isset($_GET['type']) && $_GET['type']=='daily' ?'checked':''}}>
                        <label class="custom-control-label" for="daily">{{__('Daily')}}</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-auto month">
        <div class="all-select-box">
            <div class="btn-box">
                {{Form::label('month',__('Month'),['class'=>'text-type'])}}
                {{Form::month('month',isset($_GET['month'])?$_GET['month']:date('Y-m'),array('class'=>'month-btn form-control month-btn'))}}
            </div>
        </div>
    </div>
    <div class="col-2 date">
        <div class="all-select-box">
            <div class="btn-box">
                {{ Form::label('date', __('Date'),['class'=>'text-type'])}}
                {{ Form::text('date',isset($_GET['date'])?$_GET['date']:'', array('class' => 'form-control datepicker month-btn')) }}
            </div>
        </div>
    </div>
    @if(\Auth::user()->type != 'employee')
        <div class="col-2">
            <div class="all-select-box">
                <div class="btn-box">
                    {{ Form::label('branch', __('Branch'),['class'=>'text-type'])}}
                    {{ Form::select('branch', $branch,isset($_GET['branch'])?$_GET['branch']:'', array('class' => 'form-control select2')) }}
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="all-select-box">
                <div class="btn-box">
                    {{ Form::label('department', __('Department'),['class'=>'text-type'])}}
                    {{ Form::select('department', $department,isset($_GET['department'])?$_GET['department']:'', array('class' => 'form-control select2')) }}
                </div>
            </div>
        </div>
        @endif
        <div class="col-auto  my-custom">
            <a href="#" class="apply-btn" onclick="document.getElementById('attendanceemployee_filter').submit(); return false;">
                <span class="btn-inner--icon"><i class="fas fa-search"></i></span>
            </a>
            <a href="{{route('attendanceemployee.index')}}" class="reset-btn">
                <span class="btn-inner--icon"><i class="fas fa-trash-restore-alt"></i></span>
            </a>
        </div>
        {{ Form::close() }}
</div>
@endsection
@section('content')
<div class="">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body py-0">

                <div class="table-responsive">
                    <table class="table table-striped mb-0 dataTable">
                        <thead>
                            <tr>
                                @if(\Auth::user()->type!='employee')
                                    <th>{{__('Employee')}}</th>
                                    @endif
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Clock In')}}</th>
                                    <th>{{__('Clock Out')}}</th>
                                    <th>{{__('Hours Spent')}}</th>
                                    <!--<th>{{__('Late')}}</th>
                                <th>{{__('Early Leaving')}}</th>
                                <th>{{__('Overtime')}}</th>-->
                                    @if(Gate::check('edit attendance') || Gate::check('delete attendance'))
                                    <th>{{__('Action')}}</th>
                                    @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($attendanceEmployee as $attendance)
                            <tr>
                                @if(\Auth::user()->type!='employee')
                                    <td>{{!empty($attendance->employee)?$attendance->employee->name:'' }}</td>
                                    @endif
                                    <td>{{ \Auth::user()->dateFormat($attendance->date) }}</td>
                                    <td>{{ $attendance->status }}</td>
                                    <td>{{ ($attendance->clock_in !='00:00:00') ?\Auth::user()->timeFormat( $attendance->clock_in):'00:00' }} </td>
                                    <td>{{ ($attendance->clock_out !='00:00:00') ?\Auth::user()->timeFormat( $attendance->clock_out):'00:00' }}</td>
                                    @php
                                    $hours = intval(Carbon\Carbon::parse($attendance->clock_out)->diffInSeconds($attendance->clock_in)/3600);
                                    $minutes = intval((Carbon\Carbon::parse($attendance->clock_out)->diffInSeconds($attendance->clock_in)/60)%60);
                                    @endphp
                                    <td>{{ $hours }} hours {{ $minutes }} minutes</td>
                                    <!--<td>{{ $attendance->late }}</td>
                                    <td>{{ $attendance->early_leaving }}</td>
                                    <td>{{ $attendance->overtime }}</td>-->
                                    @if(Gate::check('edit attendance') || Gate::check('delete attendance'))
                                    <td>
                                        <a href="#" class="edit-icon" data-toggle="modal" data-target="#attendance-{{ $attendance->id }}">
                                            <i class="fas fa-eye"></i>
                                        </a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="attendance-{{ $attendance->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="attendanceLabel{{ $attendance->id }}">Worked Tasks</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @php

                                                        $wtas = $attendance->work_attendance;

                                                        @endphp

                                                        @foreach ($wtas as $wta)
                                                        @if($wta->type=='Project')
                                                            <div class="row border">
                                                                <div class="col-12 col-md-6" style="white-space: normal;">
                                                                    <a href="{{ url('/projects/'.$wta->task->project_id) }}">{{ $wta->task->project->project_name }}</a>
                                                                </div>
                                                                <div class="col-12 col-md-6" style="white-space: normal;">
                                                                    <a href="{{ url('/projects/'.$wta->task->project_id.'/task') }}">{{ $wta->task->name }}</a>
                                                                </div>
                                                                <div class="col-12 col-md-6" style="white-space: normal;">
                                                                    <p>Worked hours: {{ $wta->hours }}</p>
                                                                </div>
                                                                <div class="col-12 col-md-6" style="white-space: normal;">
                                                                    <p>
                                                                        @if(!empty($wta->description))
                                                                            {{ $wta->description }}
                                                                            @else
                                                                            No other notes
                                                                            @endif
                                                                    </p>
                                                                </div>
                                                                <div class="col-12">
                                                                    Spent today on this activity: {{ App\Http\Controllers\AdminCostsController::calculate_hourly_salary($wta->employee_id)*$wta->hours }}

                                                                </div>
                                                            </div>


                                                            @elseif($wta->type=='Deal')
                                                                <div class="row border">
                                                                    <div class="col-12 col-md-6" style="white-space: normal;">
                                                                        <a href="{{ url('/deals/') }}">{{ $wta->deal->name }}</a>
                                                                    </div>
                                                                    <div class="col-12 col-md-6" style="white-space: normal;">
                                                                        <p>Worked hours: {{ $wta->hours }}</p>
                                                                    </div>
                                                                    <div class="col-12 col-md-6" style="white-space: normal;">
                                                                        <p>
                                                                            @if(!empty($wta->description))
                                                                                {{ $wta->description }}
                                                                                @else
                                                                                No other notes
                                                                                @endif
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        Spent today on this activity: {{ App\Http\Controllers\AdminCostsController::calculate_hourly_salary($wta->employee_id)*$wta->hours }}
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                @endforeach
                                                                @php
                                                                $break = $attendance->breaks;
                                                                @endphp
                                                                @if(count($breaks)>0)
                                                                    @foreach ($breaks as $break)
                                                                    @if($break->is_canceled == 1)


                                                                            <br>
                                                                            <del>Break time #{{$break->id}}: {{$break->end_break_hour}}:{{$break->end_break_minute}} ({{str_pad(strval($break->total_break_hour), 2, "0", STR_PAD_LEFT)}}:{{str_pad(strval($break->total_break_minute), 2, "0", STR_PAD_LEFT)}})</del>
                                                                            <br>
                                                                            <del>Break message #{{$break->id}}: {{$break->time_break_message}}</del>
                                                                            <br>

                                                                            @else
                                                                            <br>
                                                                            Break time #{{$break->id}}: {{$break->end_break_hour}}:{{$break->end_break_minute}} ({{str_pad(strval($break->total_break_hour), 2, "0", STR_PAD_LEFT)}}:{{str_pad(strval($break->total_break_minute), 2, "0", STR_PAD_LEFT)}})
                                                                            <br>
                                                                            Break message #{{$break->id}}: {{$break->time_break_message}}
                                                                            <br>
                                                                            @if($break->document_title)
                                                                                <a href="{{asset(url('uploads/breaks/', [$break->document_title]))}}" class="btn btn-xs btn-white btn-icon-only width-auto" download>
                                                                                    Download evidece of break: <i class="fa fa-download"></i>
                                                                                </a>
                                                                                @endif
                                                                                <a href="/cancel-break?is_canceled=1&break_id= {{$break->id}}&employee_id= {{$break->employee_id}}" class="btn btn-xs btn-danger width-auto">{{__('Cancel break')}}</a>

                                                                                @endif
                                                                                @endforeach
                                                                                @else
                                                                                No break time.
                                                                                @endif
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @can('edit attendance')
                                        <a href="#" data-url="{{ URL::to('attendanceemployee/'.$attendance->id.'/edit') }}" data-size="lg" data-ajax-popup="true" data-title="{{__('Edit Attendance')}}" class="edit-icon" data-toggle="tooltip"
                                          data-original-title="{{__('Edit')}}"><i class="fas fa-pencil-alt"></i></a>
                                        @endcan
                                        @can('delete attendance')
                                        <a href="#" class="delete-icon" data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}"
                                          data-confirm-yes="document.getElementById('delete-form-{{$attendance->id}}').submit();"><i class="fas fa-trash"></i></a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['attendanceemployee.destroy', $attendance->id],'id'=>'delete-form-'.$attendance->id]) !!}
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                    @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script-page')
<script>
    $(document).ready(function() {
        $('.daterangepicker').daterangepicker({
            format: 'yyyy-mm-dd',
            locale: {
                format: 'YYYY-MM-DD'
            },
        });
    });
</script>
@endpush
