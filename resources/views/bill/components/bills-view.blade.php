<div class="table-responsive">
    <table class="table table-striped mb-0 dataTable">
        <thead>
        <tr>
            <th> {{__('Bill')}}</th>
            @if(!\Auth::guard('vender')->check())
                <th> {{__('Vendor')}}</th>
            @endif
            <th> {{__('Bill Date')}}</th>
            <th> {{__('Paid Date')}}</th>
            <th> {{__('Amount')}}</th>
            <th> {{__('Project')}}</th>
            <th> {{__('Task')}}</th>
            <th>{{__('Status')}}</th>
            @if(Gate::check('edit bill') || Gate::check('delete bill') || Gate::check('show bill'))
                <th> {{__('Action')}}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach ($bills as $bill)
            <tr>
                <td class="Id">
                    @if(\Auth::guard('vender')->check())
                        <a href="{{ route('vender.bill.show',\Crypt::encrypt($bill->id)) }}">{{ AUth::user()->billNumberFormat($bill->bill_id) }}</a>
                    @else
                        <a href="{{ route('bill.show',\Crypt::encrypt($bill->id)) }}">{{ AUth::user()->billNumberFormat($bill->bill_id) }}</a>
                    @endif
                </td>
                @if(!\Auth::guard('vender')->check())
                    <td>
                      @if(!empty( $bill->vender))
                        {{ (!empty( $bill->vender)?$bill->vender->name:'') }}
                      @else
                        @if(count($bill->items)>0)
                          {{ $bill->items[0]->description }}
                        @endif
                      @endif
                    </td>
                @endif
                <td data-sort="{{ Carbon\Carbon::createFromFormat('Y-m-d', strval($bill->bill_date))->timestamp }}">{{ Auth::user()->dateFormat($bill->bill_date) }}</td>
                <td data-sort="{{ Carbon\Carbon::createFromFormat('Y-m-d', strval($bill->paid_date))->timestamp }}">{{ Auth::user()->dateFormat($bill->paid_date) }}</td>
                <td data-sort="{{ $bill->getTotal() }}">{{ \Auth::user()->priceFormat($bill->getTotal())}}</td>
                <td>@if($bill->project)
                  <a href="{{ route('projects.show', ['project'=>$bill->project]) }}">{{ $bill->project->project_name }}</a>
                @else
                  -
                @endif</td>
                <td>@if($bill->expense && $bill->expense->task)
                  <a href="{{ url('projects/'.$bill->expense->id.'/task') }}">{{ $bill->expense->task->name }}</a>
                @else
                  -
                @endif</td>
                <td>
                    @if($bill->status == 0)
                        <span class="badge badge-pill badge-primary">{{ __(\App\Models\Invoice::$statues[$bill->status]) }}</span>
                    @elseif($bill->status == 1)
                        <span class="badge badge-pill badge-warning">{{ __(\App\Models\Invoice::$statues[$bill->status]) }}</span>
                    @elseif($bill->status == 2)
                        <span class="badge badge-pill badge-danger">{{ __(\App\Models\Invoice::$statues[$bill->status]) }}</span>
                    @elseif($bill->status == 3)
                        <span class="badge badge-pill badge-info">{{ __(\App\Models\Invoice::$statues[$bill->status]) }}</span>
                    @elseif($bill->status == 4)
                        <span class="badge badge-pill badge-success">{{ __(\App\Models\Invoice::$statues[$bill->status]) }}</span>
                    @endif
                </td>
                @if(Gate::check('edit bill') || Gate::check('delete bill') || Gate::check('show bill'))
                    <td class="Action">
                        <span>@php $billID= Crypt::encrypt($bill->id); @endphp
                            @can('show bill')
                                @if(\Auth::guard('vender')->check())
                                    <a href="{{ route('vender.bill.show',\Crypt::encrypt($bill->id)) }}" class="edit-icon bg-warning" data-toggle="tooltip" data-original-title="{{__('Detail')}}">
                                    <i class="fas fa-eye"></i>
                                </a>
                                @else
                                    <a href="{{ route('bill.show',\Crypt::encrypt($bill->id)) }}" class="edit-icon bg-warning" data-toggle="tooltip" data-original-title="{{__('Detail')}}">
                                    <i class="fas fa-eye"></i>
                                </a>
                                @endif
                            @endcan
                            @can('edit bill')
                            <a href="{{ route('bill.edit',\Crypt::encrypt($bill->id)) }}" class="edit-icon" data-toggle="tooltip" data-original-title="{{__('Edit')}}">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            @endcan
                            @can('delete bill')
                                <a href="#" class="delete-icon " data-toggle="tooltip" data-original-title="{{__('Delete')}}" data-confirm="{{__('Are You Sure?').'|'.__('This action can not be undone. Do you want to continue?')}}" data-confirm-yes="document.getElementById('delete-form-{{$bill->id}}').submit();">
                                <i class="fas fa-trash"></i>
                            </a>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['bill.destroy', $bill->id],'id'=>'delete-form-'.$bill->id]) !!}
                                {!! Form::close() !!}
                            @endcan
                        </span>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
