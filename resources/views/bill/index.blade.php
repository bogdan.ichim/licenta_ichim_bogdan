@extends('layouts.admin')
@section('page-title')
    {{__('Manage Bills')}}
@endsection
@push('script-page')
    <script>

        $('.copy_link').click(function (e) {
            e.preventDefault();
            var copyText = $(this).attr('href');

            document.addEventListener('copy', function (e) {
                e.clipboardData.setData('text/plain', copyText);
                e.preventDefault();
            }, true);

            document.execCommand('copy');
            show_toastr('Success', 'Url copied to clipboard', 'success');
        });

        document.getElementById('file').addEventListener('change', function(e) {
        var fileName = e.target.files[0].name;
        var label = document.querySelector('.custom-file-label');
        
        if (fileName) {
            label.innerHTML = fileName;
            label.classList.add('file-uploaded');
            label.setAttribute('data-file', fileName);
        } else {
            label.innerHTML = 'Choose a file';
            label.classList.remove('file-uploaded');
            label.removeAttribute('data-file');
        }
    });
    </script>

    <style>
       .select2-container {
           z-index: 999 !important;
       }

       .custom-file-label {
        cursor: pointer;
        display: inline-block;
        padding: 12px 12px;
        width: auto;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        background-color: #f8f9fa;
        border: 1px solid #ced4da;
        border-radius: 10px;
        margin-top: 80px;
    }

    </style>
@endpush

@section('action-button')
  <div class="row d-flex justify-content-end">
    <div class="col-0">
      @if(!\Auth::guard('vender')->check())
          {{ Form::open(array('route' => array('bill.index'),'method' => 'GET','id'=>'frm_submit')) }}
      @else
          {{ Form::open(array('route' => array('vender.bill'),'method' => 'GET','id'=>'frm_submit')) }}
      @endif
    </div>
    <div class="col-2">
      <div class="all-select-box">
          <div class="btn-box">
              {{ Form::label('bill_date', __('Date'),['class'=>'text-type']) }}
              {{ Form::text('bill_date', isset($_GET['bill_date'])?$_GET['bill_date']:null, array('class' => 'month-btn form-control datepicker-range')) }}
          </div>
      </div>
    </div>
    @if(!\Auth::guard('vender')->check())
    <div class="col-auto">
      <div class="all-select-box">
          <div class="btn-box">
              {{ Form::label('vender', __('Vender'),['class'=>'text-type']) }}
              {{ Form::select('vender',$vender,isset($_GET['vender'])?$_GET['vender']:'', array('class' => 'form-control select2')) }}
          </div>
      </div>
    </div>
    @endif
    <div class="col-2">
        <div class="all-select-box">
            <div class="btn-box">
                {{ Form::label('status', __('Status'),['class'=>'text-type']) }}
                {{ Form::select('status', [''=>'All'] + $status,isset($_GET['status'])?$_GET['status']:'', array('class' => 'form-control select2')) }}
            </div>
        </div>
    </div>
    <div class="col-auto my-custom">
        <a href="#" class="apply-btn" onclick="document.getElementById('frm_submit').submit(); return false;" data-toggle="tooltip" data-original-title="{{__('apply')}}">
            <span class="btn-inner--icon"><i class="fas fa-search"></i></span>
        </a>
        @if(!\Auth::guard('vender')->check())
            <a href="{{route('bill.index')}}" class="reset-btn" data-toggle="tooltip" data-original-title="{{__('Reset')}}">
                <span class="btn-inner--icon"><i class="fas fa-trash-restore-alt"></i></span>
            </a>
        @else
            <a href="{{route('vender.index')}}" class="reset-btn" data-toggle="tooltip" data-original-title="{{__('Reset')}}">
                <span class="btn-inner--icon"><i class="fas fa-trash-restore-alt"></i></span>
            </a>
        @endif
    </div>
    <div class="col-0">
      {{ Form::close() }}
    </div>
    @can('create bill')
        <div class="col-2 my-custom-btn">
            <div class="all-button-box">
                <a href="{{ route('bill.create',0) }}" class="btn btn-xs btn-white btn-icon-only width-auto">
                    <i class="fas fa-plus"></i> {{__('Create')}}
                </a>
            </div>
        </div>
    @endcan


  <div class="modal" id="importExcel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width: 50%; display: inline;">{{__("Import throught Excel")}}</h4>
        <button type="button" class="btn btn-danger float-right" data-dismiss="modal">{{__("Close")}}</button>
      </div>

      <div class="modal-body">
         <form action="{{ route('bill.upload') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <a href="{{ route('bill.download.example') }}" class="btn btn-xs btn-white btn-icon-only width-auto" style="margin-bottom: 80px;">
                {{__("Download example EXCEL file:")}} <i class="fa fa-download"></i>
             </a>
            <div class="form-group">
                    <label for="file" class="custom-file-label">{{__("Choose a file")}}</label>
                        <input type="file" name="file" id="file" class="form-control-file">
            </div>

                <button type="submit" class="btn btn-primary">{{__("Import")}}</button>
        </form> 
      </div>

    </div>
  </div>
</div>
@endsection
@section('content')
    <div class="">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body py-0 mt-2">
                    @include('bill.components.bills-view')
                </div>
            </div>
        </div>
    </div>
@endsection
