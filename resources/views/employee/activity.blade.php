@extends('layouts.admin')
@section('page-title')
  {{__('Employee Activity')}}
@endsection
@section('content')
  <style media="screen">
    .table-fixed-container{
      height: 300px;
      overflow: auto;
    }

    .table-fixed tr.collapse.show{
      display: table-row;
    }
  </style>
  <div class="row mt-3">
    <div class="col-md-4 col-12">
      <h4>Daily activity</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($daily_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($daily_activity[$i]->date != $date)

                  @php
                    $date = $daily_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($daily_activity) && $daily_activity[$j]->date == $date)
                    @php
                      $hours += $daily_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $daily_activity[$i]->date }}</td>
                    <td>{{ $hours }}</td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".activity-{{ $daily_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($daily_activity) && $daily_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse activity-{{ $daily_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $daily_activity[$j]->hours_worked }}</td>
                      <td>{{ $daily_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-12">
      <h4>Monthly activity</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($monthly_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($monthly_activity[$i]->date != $date)

                  @php
                    $date = $monthly_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($monthly_activity) && $monthly_activity[$j]->date == $date)
                    @php
                      $hours += $monthly_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $monthly_activity[$i]->date }}</td>
                    <td>{{ $hours }}</td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".activity-{{ $monthly_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($monthly_activity) && $monthly_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse activity-{{ $monthly_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $monthly_activity[$j]->hours_worked }}</td>
                      <td>{{ $monthly_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-12">
      <h4>Yearly activity</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($yearly_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($yearly_activity[$i]->date != $date)

                  @php
                    $date = $yearly_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($yearly_activity) && $yearly_activity[$j]->date == $date)
                    @php
                      $hours += $yearly_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $yearly_activity[$i]->date }}</td>
                    <td>{{ $hours }}</td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".activity-{{ $yearly_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($yearly_activity) && $yearly_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse activity-{{ $yearly_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $yearly_activity[$j]->hours_worked }}</td>
                      <td>{{ $yearly_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row mt-3">
    @php
      $hourly_cost = \App\Http\Controllers\AdminCostsController::calculate_hourly_salary($employee->id);
    @endphp

    <div class="col-md-4 col-12">
      <h4>Daily cost</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours - Cost</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($daily_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($daily_activity[$i]->date != $date)

                  @php
                    $date = $daily_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($daily_activity) && $daily_activity[$j]->date == $date)
                    @php
                      $hours += $daily_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $daily_activity[$i]->date }}</td>
                    <td>{{ $hours }} - <b>{{ $hours * $hourly_cost }}</b></td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".cost-{{ $daily_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($daily_activity) && $daily_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse cost-{{ $daily_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $daily_activity[$j]->hours_worked }} - <b>{{ $daily_activity[$j]->hours_worked * $hourly_cost }}</b></td>
                      <td>{{ $daily_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-12">
      <h4>Monthly cost</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours - Cost</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($monthly_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($monthly_activity[$i]->date != $date)

                  @php
                    $date = $monthly_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($monthly_activity) && $monthly_activity[$j]->date == $date)
                    @php
                      $hours += $monthly_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $monthly_activity[$i]->date }}</td>
                    <td>{{ $hours }} - <b>{{ $hours * $hourly_cost }}</b></td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".cost-{{ $monthly_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($monthly_activity) && $monthly_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse cost-{{ $monthly_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $monthly_activity[$j]->hours_worked }} - <b>{{ $monthly_activity[$j]->hours_worked * $hourly_cost }}</b></td>
                      <td>{{ $monthly_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-12">
      <h4>Yearly cost</h4>
      <div class="table-fixed-container">
        <div class="table-responsive">
          <table class="table table-striped table-fixed">
            <thead>
              <tr>
                <th scope="col">Date</th>
                <th scope="col">Hours - Cost</th>
                <th scope="col">Project</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              @php
                $date = null;
              @endphp
              @for ($i = 0; $i < count($yearly_activity); $i++)
                @php
                  $hours = 0;
                @endphp
                @if($yearly_activity[$i]->date != $date)

                  @php
                    $date = $yearly_activity[$i]->date;
                    $j = $i;
                  @endphp

                  @while ($j<count($yearly_activity) && $yearly_activity[$j]->date == $date)
                    @php
                      $hours += $yearly_activity[$j]->hours_worked;
                      $j++;
                    @endphp
                  @endwhile

                  <tr>
                    <td>{{ $yearly_activity[$i]->date }}</td>
                    <td>{{ $hours }} - <b>{{ $hours * $hourly_cost }}</b></td>
                    <td> - </td>
                    <td> <button type="button" class="btn btn-outline-success" data-toggle="collapse" data-target=".cost-{{ $yearly_activity[$i]->date }}" name="button">Show all</button> </td>
                  </tr>

                  @php
                    $j = $i;
                  @endphp

                  @while ($j<count($yearly_activity) && $yearly_activity[$j]->date == $date)
                    <tr class="bg-secondary collapse cost-{{ $yearly_activity[$j]->date }}">
                      <td>&#8593;</td>
                      <td>{{ $yearly_activity[$j]->hours_worked }} - <b>{{ $yearly_activity[$j]->hours_worked * $hourly_cost }}</b></td>
                      <td>{{ $yearly_activity[$j]->project_name }}</td>
                      <td> - </td>
                    </tr>
                    @php
                      $j++;
                    @endphp
                  @endwhile
                @endif
              @endfor
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>

@endsection
