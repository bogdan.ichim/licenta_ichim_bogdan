@extends('layouts.admin')
@section('page-title')
  {{__('Cronometrarea angajaților')}}
@endsection
@section('content')
  <style media="screen">
    .card form input{
      height: 15px;
      line-height: 15px;
    }
    
    .table td, .table th {
      padding: 5px !important;
    }

    .scrollable-y{
      overflow-y: scroll;
      height: 200px;
    }
    .scrollable-y-long {
      overflow-y: scroll;
      height: 300px;
    }
    .scrollable-x{
      overflow-x: scroll;
    }

    .scrollable{
      overflow: scroll;
      height: 300px;
    }

    .datepicker-range {
      text-align: center;
    }

    .form-dater {
        text-align: left; !important
    }

    .common-size-button {
      width: 100px;
      padding: 5px 0px;
    }

    .form-group-bidirectional{
      display: flex;
      gap: 30px;
      justify-content: center;
      align-items: center;
    }

    
    /* .form-i-bidirectional:hover + .project-group-infomation-schema{
      opacity: 1;
    } */
    
    .project-group-infomation-schema {
      width: 300px;
      height: auto;
      padding: 10px;
      background-color: #EFF2F7;
      position: absolute;
      left: 100%;
      top: 5%;
      transform: translateX(20px);
      opacity: 0;
      transition: 1s ease-in-out;
    }
    

    
    @media only screen and (max-width: 900px) {
        .card-title-and-time-picker {
          flex-direction: column;
        }

        .card-title-and-time-picker.align-items-center {
          align-items: flex-start !important;
        }
        .calendar-width {
          max-width: 100%;
        }
      }

      .export-button-custom {
        margin-top: 10px;
      }

      #week-dropdown {
        border: none;
      }

      #month-year-dropdown {
        border: none;
      }
  </style>




  <script type="text/javascript">

    function get_project_by_group(){
      $('#project-text').empty();

      var group_id = $('#project-code').val();
      @if(\Auth::user()->can('show all users wta') || !\Auth::user()->employee)
      $('#employee-id').val($('input[name="employee_name"]').val());
      @else
      @if(\Auth::user()->employee)
        $('#employee-id').val({{\Auth::user()->employee->employee_id}});
      @else
        $('#employee-id').val(0);
      @endif
      @endif
      $.ajax({
        url: "/clocking/projects",
        dataType: "json",
        data: {
          group_id:group_id,
        },
        success: function(data){
          if(data.project){
            $('#project-text').val(`${data.project.project_name}`);
          }
        },
        error: function(){

        }
      });

    }

    function getBillableOrNonbillableByProjectGroup() {
      var group_id = $('#project-code').val();
      $.ajax({
        url: "/project-groups/get-billables/" + group_id,
        type: "GET",
        dataType: 'json',
        data: {
          
        },
        success: function(response){       
          
        }
      });
    }



function getMilestonesForProjectByProjectGroup(projectId) {
    $.ajax({
        url: '/get-milestones-by-project-group/' + projectId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {

          $('#project-milestone').empty();

            $.each(response, function(key, value) {
                $('#project-milestone')
                    .append($('<option></option>')
                        .attr('value', value)
                        .text(value));
            });

            getProjectPhaseByProjectGroup(projectId);
        }
    });
}

function getAllProjectsTotal() {
    $.ajax({
        url: '/clocking/get-all-projects',
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          $('.project-group-infomation-schema').empty();
            // Iterăm prin fiecare răspuns din array-ul JSON
            $.each(response.projects, function(index, project) {
                // Creăm un element <p> pentru fiecare proiect și adăugăm conținutul
                var paragraph = $('<p>').text(project.name);

                // Adăugăm elementul <p> în div-ul cu clasa "project-group-infomation-schema"
                $('.project-group-infomation-schema').append(paragraph);
            });
        }
    });
}


function getEmployeeInternalId(employee_id, callback) {
    $.ajax({
        url: '/clocking/get-employee-real-id/' + employee_id,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
            if (callback) {
                callback(response);
            }
        }
    });
}


function getProjectPhaseByProjectGroup(projectId) {
  var projectName = $('#project-milestone').val();
    $.ajax({
        url: '/get-phases-by-project-group/' + projectId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          $('#project-phase').empty();
          for(let i=0;i<response.length;i++) {
            if(response[i].milestone_title === projectName) {
                $.each(response[i]['project_tasks'], function(key, value) {
                $('#project-phase')
                    .append($('<option></option>')
                        .attr('value', value)
                        .text(value));
              });
            }
          }

            
        }
    });
}

function get_project_by_milestone() {
    var projectId = $('#project-code').val();

    getMilestonesForProjectByProjectGroup(projectId);
}

function get_phase_by_milestone() {
  var projectId = $('#project-code').val();
  $('#project-milestone').change(function() {
      getProjectPhaseByProjectGroup(projectId);
});
}



    function groupByWeekMonthYear(arr) {
      var groups = {};
      arr.forEach(function(item) {
        // Get the week, month, and year for the date
        var week = Math.floor((item.work_date.getDate() - 1) / 7) + 1;
        var month = item.work_date.getMonth();
        var year = item.work_date.getFullYear();
        // Create a key for this week, month, and year
        var key = 'Week ' + week + ' ' + month + '/' + year;
        // Initialize the group for this key if it doesn't exist
        if (!groups[key]) {
          groups[key] = [];
        }
        // Add the item to the group for this key
        groups[key].push(item);
      });
      return groups;
    }

    function updateEmployeeId(employeeId, formId) {
    var form = document.getElementById(formId);
    form.setAttribute('data-employee-id', employeeId);
  }

  
  function get_new_attendance_employee_id(employee_id){
    $.ajax({
      url: "/clocking/get-employee-attendance",
      dataType: "json",
      data: {
        employee_id:employee_id,
      },
      success: function(data){
        console.log(employee_id);
        if(data.attendance.length === 0 || data.attendance === 0) {
          return;
        } else {
        handle_attendance(data.attendance, data.employee);
        updateEmployeeId(employee_id, 'dateRange');
        }
      },
      error: function(){
      }
    });

    updateExportExcel(employee_id, 'exportExcel')
  }
  function extractWeekMonthYear(str) {
    // Extract the week
    var weekMatch = str.match(/Week (\d+)/);
    var week = weekMatch ? weekMatch[1] : null;
    
    // Extract the month and year
    var monthYearMatch = str.match(/(\w+)\/(\d+)/);
    var month = monthYearMatch ? monthYearMatch[1] : null;
    var year = monthYearMatch ? monthYearMatch[2] : null;
    
    return {
      week: week,
      month: month,
      year: year
    };
  }
  function extractWeekMonthYearNormalDate(str) {
    const parts = str.split('.');
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10);
    const year = parseInt(parts[2], 10);
    
    const date = new Date(year, month - 1, day);
    const week = Math.ceil((day + 6 - date.getDay()) / 7);
    
    return { week: String(week), month: String(month), year: String(year) };
  }
  function getCalendarSelectable() {
    var dateRange = $('#dateRange').val();
    
    var startDate = dateRange.split(" - ")[0];
    startDate = startDate.split("-");
    startDate =  startDate[2] + '.' + startDate[1] + '.' + startDate[0];
    var endDate = dateRange.split(" - ")[1];
    endDate = endDate.split("-");
    endDate = endDate[2] + '.' + endDate[1] + '.' + endDate[0];
    
    
    return {
      startDate: startDate,
      endDate: endDate
    };
  }
  function getCalendarSelectableForExcelExport() {
    var dateRange = $('#dateRange').val();
    if(dateRange !== '') {
    var dataRangeSplited = dateRange.split(" - ");
    var startDate = dataRangeSplited[0];
    startDate = startDate.split("-");
    startDate =  startDate[0] + '-' + startDate[1] + '-' + startDate[2];
    var endDate = dataRangeSplited[1];
    endDate = endDate.split("-");
    endDate = endDate[0] + '-' + endDate[1] + '-' + endDate[2];
  }
    
    return {
      startDate: startDate,
      endDate: endDate
    };
  }


  function updateExportExcel(employeeId, elementId) {
    var elementId = document.getElementById(elementId);
    var selectedDates = getCalendarSelectableForExcelExport();
    var exportUrl = "{{ route('clocking-employee.export', ['id' => ':employeeId', 'start_date' => ':startDate', 'end_date' => ':endDate']) }}";
    exportUrl = exportUrl.replace(':employeeId', employeeId);
    exportUrl = exportUrl.replace(':startDate', selectedDates['startDate']);
    exportUrl = exportUrl.replace(':endDate', selectedDates['endDate']);
    $('#exportExcel').attr('href', exportUrl);
  }

    function compareDates(dateString1, dateString2, dateString3) {
      const parts1 = dateString1.split(".");
      const parts2 = dateString2.split(".");
      const parts3 = dateString3.split(".");

      const date1 = new Date(parts1[2], parts1[1] - 1, parts1[0]);
      const date2 = new Date(parts2[2], parts2[1] - 1, parts2[0]);
      const date3 = new Date(parts3[2], parts3[1] - 1, parts3[0]);

        if(date2 >= date1 && date2 <=date3) {
          return 1;
        }
      
        return 0;
    }


    function getEditableProjectPhaseByProjectGroup(projectId) {
  var projectName = $('#project-milestone-editable').val();
    $.ajax({
        url: '/get-phases-by-project-group/' + projectId,
        type: 'GET',
        dataType: 'json',
        success: function(response) {
          $('#project-phase-editable').empty();
          for(let i=0;i<response.length;i++) {
            if(response[i].milestone_title === projectName) {
                $.each(response[i]['project_tasks'], function(key, value) {
                $('#project-phase-editable')
                    .append($('<option></option>')
                        .attr('value', value)
                        .text(value));
              });
            }
          }

            
        }
    });
}


function hours_status(employee_id, selected_date) {
  $.ajax({
    url: '/clocking/verify-clocking-hours/' + employee_id + '/' + selected_date,
    type: 'GET',
    dataType: 'json',
    success: function(response) {
      $('#hours-status').empty();
      var inputHours = parseFloat(document.getElementById("workedHours").value); 
  
      if (response.length === 0) {
        var pTag = $('<p>').text("Nu există ore înregistrate pentru data selectată.");
        pTag.css('color', 'grey');
        $('#hours-status').append(pTag);
      }
      
      response.forEach(function(entry) {
        var date = entry.date;
        var totalHours = parseFloat(entry.total_hours);
        var addedHours = totalHours + inputHours;
        var differenceHours = 8 - addedHours;
        
        if (date === selected_date) {
          if(addedHours < 8) {
            var pTag = $('<p>').text("În data de: " + date  + ", orele totale sunt: " + addedHours + "(orele de pana acum + orele introduse), mai aveți nevoie de " + differenceHours + " ore pentru a ajunge la suma de 8 ore/zi.");
            pTag.css('color', 'red');
          } else if (addedHours > 8){
            var pTag = $('<p>').text("În data de: " + date + ", orele totale sunt: " + addedHours + "(orele de pana acum + orele introduse), depașiți suma de 8 ore/zi cu " + (-differenceHours) + " ore.")
            pTag.css('color', 'green');
          } else if (addedHours == 8) {
            var pTag = $('<p>').text("În data de: " + date + ", orele totale sunt: " + addedHours + "(orele de pana acum + orele introduse), nu mai aveți nevoie de ore in plus pentru a ajunge la suma de 8ore/zi. ");
            pTag.css('color', 'green');
          } 
        }
        $('#hours-status').append(pTag);
      });
    }
  });
}



    
    function getEditableProjectWorkAttendace(project_id) {
      $.ajax({
        url: "/clocking/get-employee-attendance-project-group/" + project_id,
        type: "GET",
        dataType: 'json',
        data: {
          
        },
        success: function(response){  
          $('#project-code-editable-fictional').val(`${response.attendace.id}`)     
          $('#project-code-editable').val(`${response.attendace.project_group}`);
          $('#project-text-editable').val(`${response.attendace.project_name}`);
          $('#task-description-editable').val(`${response.attendace.task_description}`);
          $('#work-date-editable').val(`${response.attendace.work_date.split(' ')[0]}`);
          $('#workedHours-editable').val(`${response.attendace.hours}`);
          $('#project-milestone-editable').empty();
          //MILESTONES LIST
          var default_milestone = response.attendace.milestone;
          var default_milestones = $('<option></option>')
              .attr('value', default_milestone)
              .text(default_milestone)
              .attr('selected', 'selected'); 
          $('#project-milestone-editable').append(default_milestones);
          $.each(response.milestones_available, function(key, value) {
              if (value === default_milestone) {
                  return;
              }
              var option = $('<option></option>')
                  .attr('value', value)
                  .text(value);
              $('#project-milestone-editable').append(option);
          });
          //MILESTONES LIST END
          $('#project-milestone-editable').on('change', function() {
            getEditableProjectPhaseByProjectGroup(response.attendace.project_group);
          });

          if ($('#project-milestone-editable').val() !== '') {
            getEditableProjectPhaseByProjectGroup(response.attendace.project_group);
          }
          
        }
      });


    }    

    function handle_attendance(attendance, employee){

      if (attendance.length === 0) {
          return;
      }

      let rangeDates = getCalendarSelectable();
      let memoryRangeDate = getCalendarSelectableForExcelExport();
      let weekSelected = document.getElementById("week-dropdown").value;
      for (let i = 0; i < attendance.length; i++) {
        attendance[i].work_date = new Date(Date.parse(attendance[i].work_date));
      }
      
      
      $('#attendance-table-body').empty();
      for (let j = 0; j < attendance.length; j++) {
        $('#summary-per-weeks').empty();


        let approveButton = '';
        let editButton = '';
        let deleteButton = '';
        let reverseApproveButton = '';

        @if(\Auth::user()->type=='super admin' || \Auth::user()->type=='company' || \Auth::user()->type=='Admin')
        const approveDateFormated = attendance[j].work_date.toLocaleDateString("ro-RO");
        if(attendance[j].validated_by_admin == 0) {
          approveButton = `<td><a class="btn btn-success common-size-button" style="font-size: 12px;" href="/clocking/approve_attendance_employee/${attendance[j].id}/${approveDateFormated}?selected_id=${employee.id}&start_date=${memoryRangeDate["startDate"]}&end_date=${memoryRangeDate["endDate"]}&week_selected=${weekSelected}">{{__('Approve')}}</a></td>`;
          editButton = `<td><a class="btn btn-warning common-size-button" style="font-size: 12px;" href="" onClick="getEditableProjectWorkAttendace(${attendance[j].id})" data-toggle="modal" data-target="#editWorkModal">{{__('Edit')}}</a></td>`;
          deleteButton = `<td><a class="btn btn-danger common-size-button" style="font-size: 12px;" href="/clocking/delete_attendace_employee/${attendance[j].id}?selected_id=${employee.id}&start_date=${memoryRangeDate["startDate"]}&end_date=${memoryRangeDate["endDate"]}&week_selected=${weekSelected}">{{__('Delete')}}</a></td>`;
          reverseApproveButton = `<td></td>`;
        }else {
          approveButton = ``;
          reverseApproveButton =  `<td><a class="btn btn-danger common-size-button" style="font-size: 12px;" href="/clocking/reverse_approve_attendance_employee/${attendance[j].id}/${approveDateFormated}?selected_id=${employee.id}&start_date=${memoryRangeDate["startDate"]}&end_date=${memoryRangeDate['endDate']}&week_selected=${weekSelected}">{{__('Undo approve')}}</a></td>`;
        }
        @elseif(\Auth::user()->type != 'super admin' && \Auth::user()->type != 'company' && \Auth::user()->type != 'Admin')
          if(attendance[j].validated_by_admin == 0) {
                approveButton = `<td>{{__('Waiting for approval')}}</td>`;
          }else if(attendance[j].validated_by_admin == 1) {
            approveButton = `<td>{{__('Approved')}}</td>`;
          }
        @endif

       @if(\Auth::user()->type != 'super admin' && \Auth::user()->type != 'company' && \Auth::user()->type != 'Admin')
        if(attendance[j].validated_by_admin == 0) {
          editButton = `<td><a class="btn btn-warning common-size-button" style="font-size: 12px;" href="" onClick="getEditableProjectWorkAttendace(${attendance[j].id})" data-toggle="modal" data-target="#editWorkModal">{{__('Edit')}}</a></td>`;
          deleteButton = `<td><a class="btn btn-danger common-size-button" style="font-size: 12px;" href="/clocking/delete_attendace_employee/${attendance[j].id}">{{__('Delete')}}</a></td>`;
        }else if(attendance[j].validated_by_admin == 1) {
          editButton = `<td></td>`;
          deleteButton = `<td></td>`;
        }
        @endif

        if(compareDates(rangeDates["startDate"], attendance[j].work_date.toLocaleDateString("ro-RO"), rangeDates["endDate"]) == 1) {
          $('#attendance-table-body').append(`
              <tr>
                  <td>${attendance[j].project_group}</td>
                  <td>${attendance[j].project_name}</td>
                  <td>${attendance[j].hours}</td>
                  <td>${attendance[j].non_billable_hours}</td>
                  <td>${attendance[j].work_date.toLocaleDateString("ro-RO")}</td>
                  <td>${attendance[j].milestone}</td>
                  <td>${attendance[j].phase}</td>
                  <td>${attendance[j].task_description}</td>
                  ${approveButton}
                  ${reverseApproveButton}
                  ${editButton}
                  ${deleteButton}
              </tr>
          `);
        }

        
    }

    
    let grouped = groupByWeekMonthYear(attendance);

    

      let billable = 0;
      let non_billable = 0;
      let vacation = 0;
      let unpaid_leave = 0;
      let sick_leave = 0;
      let overtime = 0;

      var currentYear = new Date().getFullYear();

      for (const [key, value] of Object.entries(grouped)) {

        let billable_local = 0;
        let non_billable_local = 0;
        let vacation_local = 0;
        let unpaid_leave_local = 0;
        let sick_leave_local = 0;
        
        for (let i = 0; i < value.length; i++) {
          if(value[i].billable==1){
            billable_local+=value[i].hours;
            non_billable_local+=value[i].non_billable_hours;
          }else if(value[i].vacation==1 || value[i].unpaid_leave==1 || value[i].sick_leave==1 || value[i].non_billable==1){
            non_billable_local+=value[i].non_billable_hours;
          }
        }

        let dateKeyObj = extractWeekMonthYear(key);

        if(currentYear == dateKeyObj.year){
          billable+=billable_local;
          vacation+=vacation_local;
          non_billable+= non_billable_local;
          unpaid_leave+=unpaid_leave_local;
          sick_leave+=sick_leave_local;
        }

        let employee_working_hours = employee.working_hours * 5;

        let overtime_local = (billable+non_billable)-employee_working_hours;
        

        let color = '';

        if(overtime_local<0){
          color='#dc3545';
        }else if(overtime_local>0){
          color='#28a745';
        }

          overtime += overtime_local;

        var total_time = billable_local + non_billable_local;

        var billable_percent = String((billable_local/total_time*100).toFixed(2))+'%';
        var non_billable_percent = String((non_billable_local/total_time*100).toFixed(2))+'%';

        
        

          var weeks_select = document.getElementById("week-dropdown");
          var week_selected = parseInt(weeks_select.value);

          var months_and_years_select = document.getElementById("month-year-dropdown");
          var month_and_year_selected = months_and_years_select.value;
          var month_and_year_splited = month_and_year_selected.split('-');
          var month_selected = parseInt(month_and_year_splited[0]);
          var year_selected = parseInt(month_and_year_splited[1]);


          if(week_selected == dateKeyObj.week && month_selected == parseInt(dateKeyObj.month)+1 && year_selected == dateKeyObj.year) {
        $('#summary-per-weeks').append(`
          <tr style="color:${color};">
            <td class="text-center">${dateKeyObj.week}</td>
            <td class="text-center">${parseInt(dateKeyObj.month)+1}-${dateKeyObj.year}</td>
            <td class="text-center">${billable_local} - ${billable_percent}</td>
            <td class="text-center">${non_billable} - ${non_billable_percent}</td>
            <td class="text-center">${billable_local + non_billable}</td>
          </tr>
          `);
          }



      }

      let took_vacation_days = parseInt(vacation/employee.working_hours);
      $('#took-vacation-days').html(String(took_vacation_days));
      $('#overtime-hours').html(String(overtime));

    }

    document.addEventListener("DOMContentLoaded", function() {
      @if(\Auth::user()->can('show all users wta') || !\Auth::user()->employee)
      var current_employee_selected = {{ count($employees)>0?$employees[0]->employee_id:0 }};
      @else
      var current_employee_selected = {{ \Auth::user()->employee->employee_id }};
      @endif
      if(current_employee_selected!=0){
        get_new_attendance_employee_id(current_employee_selected);
      }
    });

  function defaultValuesZero() {
    var workedHours = document.getElementById("workedHours").value;
    if (workedHours === "") {
      document.getElementById("workedHours").value = 0;
    }
}


  function checkValueWithinMinAndMax(input) {
    var workedHours = document.getElementById("workedHours").value;
    var inputValue = parseInt(input.value);
    var maxValue = parseInt(input.max);
    var minValue = parseInt(input.min);
    document.querySelector("#workedHours").max = 8;
    document.querySelector("#workedHours").min = 0;
    if (inputValue < minValue) {
      input.value = 0;
    }else if(inputValue >= maxValue) {
    input.value = input.max;
    } 
    
    if (workedHours === "") {
      document.getElementById("workedHours").value = 0;
    }
  }


  function combinedOnChangeProjectCode () {
    get_project_by_group();
    get_project_by_milestone();
    getBillableOrNonbillableByProjectGroup();
    get_phase_by_milestone();
  }

  
  document.addEventListener("DOMContentLoaded", function() {
    var currentDate = new Date();
    var firstDayOfWeek = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay()));
    var lastDayOfWeek = new Date(currentDate.setDate(currentDate.getDate() + 6));
    var formattedStartDate = formatDateRangePicker(firstDayOfWeek);
    var formattedEndDate = formatDateRangePicker(lastDayOfWeek);

    
    var defaultDateRange = formattedStartDate + ' - ' + formattedEndDate;
    document.querySelector("#project-code").addEventListener('change', ()=>{
      combinedOnChangeProjectCode();
    });

    document.getElementById("workedHours").addEventListener("change", function(){
      @if(\Auth::user()->employee)
      var employee_id = $('#employee-id').val({{\Auth::user()->employee->employee_id}});
      @endif
      var selected_date = document.getElementById("work-date").value;
        checkValueWithinMinAndMax(this);
        hours_status(employee_id[0].defaultValue, selected_date);
    });
    
    
    defaultValuesZero();
  });
  



function formatDateRangePicker(date) {
  var day = date.getDate();
  var month = date.getMonth() + 1;
  var year = date.getFullYear();
  if (day < 10) {
    day = '0' + day;
  }
  if (month < 10) {
    month = '0' + month;
  }

  return year + '-' + month + '-' + day;
}

function scrollToCheckedInput(inputId, offsetPixels) {
  var inputElement = document.getElementById(inputId);
  if (inputElement) {
    var container = inputElement.closest('.form-check');
    if (container) {
      container.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    }
  }
}


  </script>

<div class="row">

  @if(\Auth::user()->can('show all users wta') || !\Auth::user()->employee)
    <div class="col-12 col-md-4 p-2">
      <div class="card">
        <div class="card-header">
          <h3>{{__("Employee Details")}}</h3>
        </div>
        <div class="card-body">
          <form action="#" class="scrollable-y" method="post">

          <?php
            $employeesArray = json_decode(json_encode($employees), true);
            usort($employeesArray, function ($a, $b) {
              return strcasecmp($a['name'], $b['name']);
          });
          ?>
            @foreach ($employeesArray as $employee)
              <div class="form-check">
                <input type="radio" class="form-check-input" id="{{ $employee['id'] }}" name="employee_name" value="{{ $employee['id'] }}"
                  @if(isset($_GET['selected_id']) && $_GET['selected_id'] == $employee['id'])
                    checked
                  @elseif(!isset($_GET['selected_id']) && $employee['id'] == $employeesArray[0]['id'])
                    checked
                  @endif
                  onchange="get_new_attendance_employee_id({{$employee['employee_id']}}); updateEmployeeId({{$employee['employee_id']}}, 'dateRange');">
                <label class="form-check-label" for="{{ $employee['id'] }}">{{ $employee['name'] }}</label>
              </div>
            @endforeach
          </form>
        </div>
      </div>
    </div>
  @endif

  <div class="col-12
  @if(\Auth::user()->can('show all users wta') || !\Auth::user()->employee)
    col-md-8
  @else
    col-md-6
  @endif
    p-2">
    <div class="card">
      <div class="card-header">
        <h3>{{__("Hours")}}</h3>
      </div>
      <div class="card-body scrollable">
        <table class="table">
          <thead>
          <tr>
      <th>
      {{__("Week")}}
        <select id="week-dropdown">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>
      </th>
      <th>{{__("Month-Year")}}
        <select id="month-year-dropdown"></select>
      </th>
      <th>{{__("Hours")}}</th>
    </tr>
          </thead>
          <tbody id="summary-per-weeks">
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="submitWorkModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width: 50%; display: inline;">{{__("Submit Work")}}</h4>
        <button type="button" class="btn btn-danger float-right" data-dismiss="modal">{{__("Close")}}</button>
      </div>

      <div class="modal-body">
        <form action="/clocking/submit" method="post" id="submitWork">
          @csrf

          <input type="hidden" name="employee-id" id="employee-id" value="">

          <div class="form-group form-group-bidirectional">
            <input type="text" class="form-control" name="project-group" id="project-code" placeholder="{{__("Project code")}}" value="">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle form-i-bidirectional" viewBox="0 0 16 16" onClick="getAllProjectsTotal();">
              <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
              <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
            </svg>
            <div class="project-group-infomation-schema"></div>
          </div>

          <div class="form-group">
            <label for="project-text">{{__("Project list:")}}</label>
            <input type="text" name="project-name" class="form-control" id="project-text" readonly value="">
          </div>
          
          <div class="form-group">
            <label for="project-text">{{__("Today date:")}}</label>
            <input type="text" class="form-control datepicker-range form-dater" name="work-date" id="work-date" placeholder="Work date" value="">
        </div>
          <div class="form-group">
            <label for="project-text">{{__("Hours worked:")}}</label>
            <input type="number" id="workedHours" class="form-control" name="hours" placeholder="Hours worked" value="" min="0" max="8">
          </div>

          <div class="form-group">
            <p name="hours-status" id="hours-status"></p>
          </div>

          <div class="form-group">
          <label for="project-text">{{__("Milestone:")}}</label>
          {!! Form::select('project-milestone', [], null, array('class' => 'form-control select2', 'required' => 'required', 'id' => 'project-milestone')) !!}
          </div>

          <div class="form-group">
          <label for="project-phase">{{__("Project phase:")}}</label>
          {!! Form::select('project-phase', [], null, array('class' => 'form-control select2', 'required' => 'required', 'id' => 'project-phase')) !!}  
          </div>

          <div class="form-group">
            <input type="text" class="form-control" name="task-description" placeholder="{{__("Task Description")}}" value="">
          </div>

          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" name="overtime-balance" class="form-check-input" value="0" checked>{{__("Normal hours")}}
            </label>
          </div>

          <input type="submit" class="btn btn-primary" name="submit" value="{{__("Submit")}}">

        </form>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="editWorkModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="width: 50%; display: inline;">{{__("Edit Work")}}</h4>
        <button type="button" class="btn btn-danger float-right" data-dismiss="modal">{{__("Close")}}</button>
      </div>

      <div class="modal-body">
        <form action="/clocking/submit_edit" method="post" id="submitWork">
          @csrf

          <input type="hidden" name="employee-id" id="employee-id" value="">
          <input type="hidden" name="project-code-editable-fictional" id="project-code-editable-fictional" value="">

          <div class="form-group">
            <input type="text" class="form-control" name="project-group-editable" id="project-code-editable" readonly placeholder="{{__("Project code")}}" value="">
          </div>

          <div class="form-group">
            <label for="project-text">{{__("Project list:")}}</label>
            <input type="text" name="project-text-editable" class="form-control" id="project-text-editable" readonly value="">
          </div>

          <div class="form-group">
            <label for="project-text">{{__("Today date:")}}</label>
            <input type="text" class="form-control datepicker-range form-dater" name="work-date-editable" id="work-date-editable" placeholder="Work date" value="">
          </div>

          <div class="form-group">
            <label for="project-text">{{__("Hours worked:")}}</label>
            <input type="number" name="workedHours-editable" id="workedHours-editable" class="form-control" name="hours" placeholder="Hours worked" value="" min="0" max="8">
          </div>


          <div class="form-group">
          <label for="project-text">{{__("Milestone:")}}</label>
          {!! Form::select('project-milestone-editable', [], null, array('class' => 'form-control select2', 'required' => 'required', 'id' => 'project-milestone-editable')) !!}
          </div>

          <div class="form-group">
          <label for="project-phase">{{__("Project phase:")}}</label>
          {!! Form::select('project-phase-editable', [], null, array('class' => 'form-control select2', 'required' => 'required', 'id' => 'project-phase-editable')) !!}  
          </div>

          <div class="form-group">
            <input type="text" class="form-control" name="task-description-editable" id="task-description-editable" placeholder="{{__("Task Description")}}" value="">
          </div>

          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" name="overtime-balance" class="form-check-input" value="0" checked>{{__("Normal hours")}}
            </label>
          </div>

          <input type="submit" class="btn btn-primary" name="submit" value="{{__("Submit")}}">

        </form>
      </div>

    </div>
  </div>
</div>

<div class="row">
  <div class="col-12 p-2">
    <div class="card">
      <div class="card-header">
        <div class="card-title-and-time-picker row justify-content-between align-items-center">
        <h3 style="display: inline; width: 50%;">{{__('Clocking Details')}}</h3>
        <div class="col-2 p-2 calendar-width">
          <div class="all-select-box">
          <div class="btn-box">
            {{ Form::label('dateRange', __('Date'), ['class' => 'text-type']) }}
            <?php
            $currentDate = \Carbon\Carbon::now();
            if(isset($_GET['start_date']) && isset($_GET['end_date'])) {
              $startOfWeek = $_GET['start_date'];
              $endOfWeek = $_GET['end_date'];
            }else {
              $startOfWeek = $currentDate->startOfWeek()->format('Y-m-d');
              $endOfWeek = $currentDate->endOfWeek()->format('Y-m-d');
            }

          
            ?> 

              {{ Form::text('dateRange', isset($_GET['dateRange']) ? $_GET['dateRange'] : $startOfWeek.' - '.$endOfWeek, [
                  'class' => 'month-btn form-control datepicker-range',
                  'id' => 'dateRange',
                  'data-employee-id' => '',
                  'onchange' => 'get_new_attendance_employee_id(this.getAttribute("data-employee-id"))'
              ]) }}        
        </div>
          </div>
          <div class="all-button-box export-button-custom">
          <a href="{{ route('clocking-employee.export', ['id' => ':employeeId', 'start_date' => ':startDate', 'end_date' => ':endDate']) }}" id="exportExcel" class="btn btn-xs btn-white btn-icon-only width-auto">
                  <i class="fa fa-file-excel"></i> {{__('Export')}}
              </a>
          </div>
        </div>
    </div>
        @if(\Auth::user()->employee)
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#submitWorkModal">
          {{__('Submit Work')}}
        </button>
        @endif
      </div>
      <div class="card-body">
        <div class="scrollable-y-long scrollable-x">
          <table class="table">
            <thead>
              <tr>
                <th>{{__('Project code')}}</th>
                <th>{{__('Project Name')}}</th>
                <th>{{__('Hours')}}</th>
                <th>{{__('Date')}}</th>
                <th>{{__('Milestone')}}</th>
                <th>{{__('Project pahse')}}</td>
                <th>{{__('Task Description')}}</th>
                @if(\Auth::user()->type != 'super admin' && \Auth::user()->type != 'company' && \Auth::user()->type != 'Admin')
                <th>{{__('Status')}}</th>
                @endif
                <th>{{__('Actions')}}</th>
              </tr>
            </thead>
            <tbody id="attendance-table-body">
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('script-page')
    <script>

      
        $(document).ready(function () {
            $('#work-date').daterangepicker({
              singleDatePicker: true,
                            locale: {
                                format: 'YYYY-MM-DD'
              }
            });

            $('#work-date-editable').daterangepicker({
              singleDatePicker: true,
                            locale: {
                                format: 'YYYY-MM-DD'
              }
            });


                  var select = document.getElementById("month-year-dropdown");
                  var currentDate = new Date();
                  var currentYear = currentDate.getFullYear();
                  var currentMonth = currentDate.getMonth() + 1; 
                
                  for (var i = 0; i < 12; i++) {
                    var month = currentMonth - i;
                    var year = currentYear;
                    if (month <= 0) {
                      month += 12;
                      year -= 1;
                    }
                    var option = document.createElement("option");
                    option.value = month + "-" + year;
                    option.textContent = month + "-" + year;
                    select.appendChild(option);
                  }

                  var select_week = document.getElementById("week-dropdown");
                  
                  var week_input = document.getElementById("week-dropdown");
                  var month_and_year_input = document.getElementById("month-year-dropdown");

                  week_input.onchange = function() {
                      var checkedInput = document.querySelector('input[name="employee_name"]:checked');
                      var employeeId = checkedInput.value;
                      getEmployeeInternalId(employeeId, function(internalId){
                        get_new_attendance_employee_id(internalId);
                      });
                    };

                    month_and_year_input.onchange = function() {
                      var checkedInput = document.querySelector('input[name="employee_name"]:checked');
                      var employeeId = checkedInput.value;
                      getEmployeeInternalId(employeeId, function(internalId) {
                        get_new_attendance_employee_id(internalId);
                      });
                    };

        });

        window.addEventListener('DOMContentLoaded', function() {
    var checkedInput = document.querySelector('input[name="employee_name"]:checked');

 
    if (checkedInput && checkedInput.onchange !== null) {
        var employeeId = checkedInput.value;
        
        
        // Use the callback approach to get the internal ID
        getEmployeeInternalId(employeeId, function(internalId) {
            // Now you have the internalId, you can use it in your other function
            scrollToCheckedInput(checkedInput.id, 300);
            get_new_attendance_employee_id(internalId);
        });
    }
});


$(document).ready(function() {
  $('.form-i-bidirectional').click(function() {
    // Verificăm dacă elementul următor are deja opacitatea setată la 1
    if ($('.project-group-infomation-schema').css('opacity') === '1') {
      // Dacă are opacitatea 1, o reducem la 0 la clic
      $('.project-group-infomation-schema').css('opacity', '0');
    } else {
      // Altfel, o setăm la 1 la clic
      $('.project-group-infomation-schema').css('opacity', '1');
    }
  });
});

document.addEventListener("DOMContentLoaded", function() {
    // Obține elementul dropdown și valoarea din URL
    var dropdown = document.getElementById("week-dropdown");
    var selectedWeek = getUrlParameter("week_selected");

    // Setează valoarea implicită a dropdown-ului la valoarea din URL sau la prima opțiune dacă nu există
    if (selectedWeek) {
        dropdown.value = selectedWeek;
    } else {
        dropdown.value = "1"; // Setează valoarea implicită la "1" dacă nu e specificată în URL
    }

    // Funcție pentru a obține parametrii din URL
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
});

    </script>
@endpush