@php
    $users=\Auth::user();
    $profile=asset(Storage::url('uploads/avatar/'));
    //$currantLang = $users->currentLanguage();
    $languages=\App\Models\Utility::languages();
    $lang = !is_null(\Cookie::get('lang')) ? \Cookie::get('lang') : 'en';

    $unseenCounter=App\Models\ChMessage::where('to_id', Auth::user()->id)->where('seen', 0)->count();
@endphp
<nav class="navbar navbar-main navbar-expand-lg navbar-border n-top-header" id="navbar-main">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main-collapse" aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- User's navbar -->
        <div class="navbar-user d-lg-none ml-auto">
            <ul class="navbar-nav flex-row align-items-center">
                <li class="nav-item">
                    <a href="#" class="nav-link nav-link-icon sidenav-toggler" data-action="sidenav-pin" data-target="#sidenav-main"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item dropdown dropdown-animate">
                    <a class="nav-link pr-lg-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="avatar avatar-sm rounded-circle">
                          <img src="{{(!empty($users->avatar)? $profile.'/'.$users->avatar : $profile.'/avatar.png')}}" class="hweb"/>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right dropdown-menu-arrow">
                        <h6 class="dropdown-header px-0">{{__('Hi')}}, {{\Auth::user()->name}}</h6>
                        @if(\Auth::guard('customer')->check())
                            <a href="{{route('customer.profile')}}" class="dropdown-item">
                            </a>
                        @elseif(\Auth::guard('vender')->check())
                            <a href="{{route('vender.profile')}}" class="dropdown-item">
                            </a>
                        @else
                            <a href="{{route('profile')}}" class="dropdown-item has-icon">
                            </a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="dropdown-item">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>{{__('Logout')}}</span>
                        </a>
                        @if(\Auth::guard('customer')->check())
                            <form id="frm-logout" action="{{ route('customer.logout') }}" method="POST" class="d-none">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <form id="frm-logout" action="{{ route('logout') }}" method="POST" class="d-none">
                                {{ csrf_field() }}
                            </form>
                        @endif
                    </div>
                </li>
            </ul>
        </div>

        <div class="collapse navbar-collapse navbar-collapse-fade" id="navbar-main-collapse">
            <ul class="navbar-nav align-items-center d-none d-lg-flex">
                <li class="nav-item mr-4">
                    <a href="#" class="nav-link nav-link-icon sidenav-toggler" data-action="sidenav-pin" data-target="#sidenav-main"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item dropdown dropdown-animate">
                    <a class="nav-link pr-lg-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media media-pill align-items-center">
                          <span class="avatar rounded-circle">
                            <img src="{{asset('assets/img/avatar/avatar-1.png')}}" class="hweb"/>
                          </span>
                            <div class="ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm font-weight-bold">{{\Auth::user()->name}}</span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right dropdown-menu-arrow">
                        <h6 class="dropdown-header px-0">{{__('Hi')}}, {{\Auth::user()->name}}</h6>
                        @if(\Auth::guard('customer')->check())
                            <a href="{{route('customer.profile')}}" class="dropdown-item">
                            </a>
                        @elseif(\Auth::guard('vender')->check())
                            <a href="{{route('vender.profile')}}" class="dropdown-item">
                            </a>
                        @else
                            <a href="{{route('profile')}}" class="dropdown-item has-icon">
                            </a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                            <span>{{__('Logout')}}</span>
                            @if(\Auth::guard('customer')->check())
                                <form id="frm-logout" action="{{ route('customer.logout') }}" method="POST" class="d-none">
                                    {{ csrf_field() }}
                                </form>
                            @else
                                <form id="frm-logout" action="{{ route('logout') }}" method="POST" class="d-none">
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
