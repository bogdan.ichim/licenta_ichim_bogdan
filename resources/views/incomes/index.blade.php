@extends('layouts.admin')

@section('page-title')
    {{ucwords($project->project_name).__("'s Incomes")}}
@endsection

@section('action-button')
    <div class="col-md-6 d-flex align-items-center justify-content-between justify-content-md-end">
        @can('create expense')
            <!--<a href="#" class="btn btn-xs btn-white btn-icon-only width-auto ml-2" data-url="{{ route('projects.incomes.create',$project->id) }}" data-ajax-popup="true" data-size="lg" data-title="{{__('Create Income')}}">
                <span class="btn-inner--icon"><i class="fas fa-plus"></i>{{__('Create')}}</span>
            </a>-->
        @endcan
        <a href="{{ route('projects.show',$project->id) }}" class="btn btn-xs btn-white btn-icon-only width-auto">
            <span class="btn-inner--icon"><i class="fas fa-arrow-left"></i>{{__('Back')}}</span>
        </a>
        <a href="{{ route('projects.incomes.export',$project->id) }}" class="btn btn-xs btn-white btn-icon-only width-auto">
            <span class="btn-inner--icon"><i class="fa fa-file-excel"></i>{{__('Export')}}</span>
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
              @include('invoice.components.invoices-view')
            </div>
        </div>
    </div>
@endsection
